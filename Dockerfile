# Base Alpine Linux based image with OpenJDK JRE only
FROM openjdk:8-jre-alpine

WORKDIR /usr/metrics-app

# copy application WAR (with libraries inside)
COPY metrics-viewer-web/target/metrics-viewer-web-1.0-SNAPSHOT.war app.war

# specify default command
CMD ["/usr/bin/java", "-jar", "-Dspring.profiles.active=docker", "-Dlog.directory=mounted/metrics-viewer-web-logs", "app.war"]