package com.mateuszprzybyla.uj.masterthesis.runner.command;

import java.util.concurrent.CompletableFuture;

public class RunningProcess {
    private String processId;
    private CompletableFuture<ProcessResult> processResult;
    private CompletableFuture<Long> totalTimeMs;
    private Runnable stopCallback;

    RunningProcess(String processId, CompletableFuture<ProcessResult> processResult,
                          CompletableFuture<Long> totalTimeMs, Runnable stopCallback) {
        this.processId = processId;
        this.processResult = processResult;
        this.totalTimeMs = totalTimeMs;
        this.stopCallback = stopCallback;
    }

    public String getProcessId() {
        return processId;
    }

    public CompletableFuture<ProcessResult> getProcessResult() {
        return processResult;
    }

    public CompletableFuture<Long> getTotalTimeMs() {
        return totalTimeMs;
    }

    public void whenDone(Runnable action) {
        this.getTotalTimeMs().thenRun(action);
    }

    public void stop() {
        stopCallback.run();
    }
}
