package com.mateuszprzybyla.uj.masterthesis.runner.custom;

import com.mateuszprzybyla.uj.masterthesis.metrics.model.input.MetricData;
import com.mateuszprzybyla.uj.masterthesis.metrics.model.runner.ScenarioConfig;
import com.mateuszprzybyla.uj.masterthesis.runner.MetricConverter;
import com.mateuszprzybyla.uj.masterthesis.runner.command.RunningProcess;
import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.disposables.Disposable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

@Component
public class CustomMetricsCollectorScheduler {
    private static final Logger LOGGER = LoggerFactory.getLogger(CustomMetricsCollectorScheduler.class);

    private final Scheduler scheduler;
    private final CustomMetricsCollectorService customMetricsCollectorService;
    private final MetricConverter<ScenarioMetricsSnapshot> metricConverter;

    @Autowired
    public CustomMetricsCollectorScheduler(@Qualifier("customMetricsScheduler") Scheduler scheduler,
                                           CustomMetricsCollectorService customMetricsCollectorService,
                                           @Qualifier("scenarioMetricsSnapshotMetricConverter")
                                                   MetricConverter<ScenarioMetricsSnapshot> metricConverter) {
        this.scheduler = scheduler;
        this.customMetricsCollectorService = customMetricsCollectorService;
        this.metricConverter = metricConverter;
    }

    public CompletableFuture<List<MetricData>> startCollecting(long startTimeMillis, RunningProcess runningProcess,
                                                               ScenarioConfig scenarioConfig, CountDownLatch countDownLatch) {
        List<ScenarioMetricsSnapshot> metricsSnapshots = Collections.synchronizedList(new ArrayList<>());
        String processId = runningProcess.getProcessId();
        CompletableFuture<List<MetricData>> executionScenarioCompletableFuture = new CompletableFuture<>();
        LOGGER.info("Starting collecting custom scenario metrics from process with PID {}", processId);
        Disposable subscribe = Observable.interval(scenarioConfig.getRefreshIntervalMs(), TimeUnit.MILLISECONDS, scheduler)
                .subscribe(i -> {
                    ScenarioMetricsSnapshot metricsSnapshot = customMetricsCollectorService
                            .collectMetrics(startTimeMillis, processId, scenarioConfig.getMetricList());
                    metricsSnapshots.add(metricsSnapshot);
                });
        runningProcess.getProcessResult().handle((output, exception) -> {
            countDownLatch.countDown();
            subscribe.dispose();
            executionScenarioCompletableFuture.complete(metricConverter.createMetrics(metricsSnapshots));
            LOGGER.info("Stopped collecting custom scenario metrics from process with PID {}", processId);
            return output;
        });
        return executionScenarioCompletableFuture;
    }
}
