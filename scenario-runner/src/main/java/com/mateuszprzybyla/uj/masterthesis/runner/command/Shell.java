package com.mateuszprzybyla.uj.masterthesis.runner.command;

import java.io.IOException;

interface Shell {
    Process startProcessWithCommandAndFileOutputRedirection(String command, String fileName) throws IOException;
    Process startProcessWithCommand(String command) throws IOException;
    String getProcessId(Process process);
}
