package com.mateuszprzybyla.uj.masterthesis.runner.custom;

import org.apache.commons.lang3.text.StrSubstitutor;

import java.util.Collections;
import java.util.Map;

class CustomMetricCommand {
    private String commandExpression;

    CustomMetricCommand(String commandExpression) {
        this.commandExpression = commandExpression;
    }

    String getForProcessId(String processId) {
        Map<String, String> valuesMap = Collections.singletonMap("pid", processId);
        StrSubstitutor strSubstitutor = new StrSubstitutor(valuesMap);
        return strSubstitutor.replace(commandExpression);
    }
}
