package com.mateuszprzybyla.uj.masterthesis.runner.command;

import java.util.List;
import java.util.function.Supplier;

abstract class OutputSupplier implements Supplier<List<String>> {
    private List<String> outputCache;

    @Override
    public synchronized List<String> get() {
        if (outputCache == null) {
            outputCache = getOutput();
        }
        return outputCache;
    }

    abstract List<String> getOutput();
}
