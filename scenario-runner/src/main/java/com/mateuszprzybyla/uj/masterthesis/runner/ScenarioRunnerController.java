package com.mateuszprzybyla.uj.masterthesis.runner;

import com.mateuszprzybyla.uj.masterthesis.metrics.model.Metric;
import com.mateuszprzybyla.uj.masterthesis.metrics.model.runner.ScenarioConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.beans.PropertyEditorSupport;

@Controller
@RequestMapping("/")
class ScenarioRunnerController {
    private static final Logger LOGGER = LoggerFactory.getLogger(ScenarioRunnerController.class);

    private final ScenarioRunner scenarioRunner;

    @Autowired
    ScenarioRunnerController(ScenarioRunner scenarioRunner) {
        this.scenarioRunner = scenarioRunner;
    }

    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(Metric.class, new MetricEnumConverter());
    }

    @RequestMapping(value = "/runScenario", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<?> runScenario(@RequestBody @Validated ScenarioConfig scenarioConfig) {
        LOGGER.debug("Received request to /runScenario with body {}", scenarioConfig);

        return scenarioRunner.run(scenarioConfig)
                .map(ResponseEntity::ok)
                .orElse(new ResponseEntity<>(HttpStatus.FORBIDDEN));
    }

    private class MetricEnumConverter extends PropertyEditorSupport {
        @Override
        public void setAsText(String source) throws IllegalArgumentException {
            try {
                setValue(Metric.valueOf(source));
            } catch (IllegalArgumentException e) {
                setValue(null);
            }
        }
    }
}
