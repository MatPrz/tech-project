package com.mateuszprzybyla.uj.masterthesis.runner.custom;

import com.mateuszprzybyla.uj.masterthesis.metrics.model.Metric;
import com.mateuszprzybyla.uj.masterthesis.runner.MetricConverter;
import com.mateuszprzybyla.uj.masterthesis.runner.command.CommandExecutor;
import io.reactivex.Scheduler;
import io.reactivex.internal.schedulers.ExecutorScheduler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.EnumMap;
import java.util.Map;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

@Configuration
class CustomMetricsConfiguration {
    @Bean
    Scheduler customMetricsScheduler() {
        Executor executor = Executors.newFixedThreadPool(10);
        return new ExecutorScheduler(executor);
    }

    @Bean
    @Autowired
    SupportedCustomMetrics supportedCustomMetrics(CommandExecutor commandExecutor) {
        Map<Metric, CustomMetric<?>> customMetricConfigMap = new EnumMap<>(Metric.class);
        customMetricConfigMap.put(Metric.MEMORY_USAGE, memoryUsageConfig(commandExecutor));
        customMetricConfigMap.put(Metric.CPU_USAGE, cpuUsageConfig(commandExecutor));
        customMetricConfigMap.put(Metric.RESIDENT_SET_SIZE, residentSetSizeConfig(commandExecutor));
        customMetricConfigMap.put(Metric.DATA_RESIDENT_SET_SIZE, dataResidentSetSizeConfig(commandExecutor));
        customMetricConfigMap.put(Metric.TEXT_RESIDENT_SET_SIZE, textResidentSetSizeConfig(commandExecutor));
        customMetricConfigMap.put(Metric.VIRTUAL_MEMORY_SIZE, virtualMemorySizeConfig(commandExecutor));
        return new SupportedCustomMetrics(customMetricConfigMap);
    }

    @Bean
    @Autowired
    CustomMetric memoryUsageConfig(CommandExecutor commandExecutor) {
        return new CustomMetric<>(Metric.MEMORY_USAGE,
                psCommandMetricsSourceProvider(commandExecutor),
                new TabSeparatedResultParser(0, 0));
    }

    @Bean
    @Autowired
    CustomMetric cpuUsageConfig(CommandExecutor commandExecutor) {
        return new CustomMetric<>(Metric.CPU_USAGE,
                psCommandMetricsSourceProvider(commandExecutor),
                new TabSeparatedResultParser(0, 1));
    }

    @Bean
    @Autowired
    CustomMetric residentSetSizeConfig(CommandExecutor commandExecutor) {
        return new CustomMetric<>(Metric.RESIDENT_SET_SIZE,
                psCommandMetricsSourceProvider(commandExecutor),
                new TabSeparatedResultParser(0, 2));
    }

    @Bean
    @Autowired
    CustomMetric dataResidentSetSizeConfig(CommandExecutor commandExecutor) {
        return new CustomMetric<>(Metric.DATA_RESIDENT_SET_SIZE,
                psCommandMetricsSourceProvider(commandExecutor),
                new TabSeparatedResultParser(0, 3));
    }

    @Bean
    @Autowired
    CustomMetric textResidentSetSizeConfig(CommandExecutor commandExecutor) {
        return new CustomMetric<>(Metric.TEXT_RESIDENT_SET_SIZE,
                psCommandMetricsSourceProvider(commandExecutor),
                new TabSeparatedResultParser(0, 4));
    }

    @Bean
    @Autowired
    CustomMetric virtualMemorySizeConfig(CommandExecutor commandExecutor) {
        return new CustomMetric<>(Metric.VIRTUAL_MEMORY_SIZE,
                psCommandMetricsSourceProvider(commandExecutor),
                new TabSeparatedResultParser(0, 5));
    }

    @Bean
    @Autowired
    CommandLineMetricsSourceProvider psCommandMetricsSourceProvider(CommandExecutor commandExecutor) {
        return new CommandLineMetricsSourceProvider(commandExecutor,
                new CustomMetricCommand("ps -p ${pid} -ho %mem,%cpu,rss,drs,trs,vsize,maj_flt,min_flt"), "ps command metrics");
    }

    @Bean
    MetricConverter<ScenarioMetricsSnapshot> scenarioMetricsSnapshotMetricConverter() {
        return new MetricConverter<>(
                ScenarioMetricsSnapshot::getTimeFromStart, ScenarioMetricsSnapshot::getMetricsValues);
    }
}
