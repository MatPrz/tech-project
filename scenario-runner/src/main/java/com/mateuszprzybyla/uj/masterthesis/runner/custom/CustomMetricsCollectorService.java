package com.mateuszprzybyla.uj.masterthesis.runner.custom;

import com.mateuszprzybyla.uj.masterthesis.metrics.model.Metric;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.*;

@Service
class CustomMetricsCollectorService {
    private static final Logger LOGGER = LoggerFactory.getLogger(CustomMetricsCollectorService.class);

    private final SupportedCustomMetrics customMetrics;
    private final BinaryOperator<Map<Metric, Double>> mapAccumulator = (mapOne, mapTwo) -> {
        mapOne.putAll(mapTwo);
        return mapOne;
    };

    @Autowired
    CustomMetricsCollectorService(SupportedCustomMetrics customMetrics) {
        this.customMetrics = customMetrics;
    }

    ScenarioMetricsSnapshot collectMetrics(long startTimeMillis, String processId, List<Metric> metricList) {
        int timeAfterStart = (int) (System.currentTimeMillis() - startTimeMillis);
        List<Metric> customMetrics = filterCustomMetrics(metricList);
        LOGGER.info("Collecting custom metrics {} for PID {} and estimated time {}", customMetrics, processId, timeAfterStart);
        List<CustomMetricGroup<?>> customMetricGroups = getCustomMetricGroups(customMetrics);
        LOGGER.debug("Created {} custom metrics groups", customMetricGroups.size());

        Map<Metric, Double> metricsValues = customMetricGroups.stream().parallel()
                .map(group -> this.getMetricsForGroup(group, timeAfterStart, processId))
                .reduce(new HashMap<>(), mapAccumulator);

        return new ScenarioMetricsSnapshot(timeAfterStart, metricsValues);
    }

    private List<Metric> filterCustomMetrics(List<Metric> metricList) {
        return metricList.stream()
                .map(this.customMetrics::getCustomMetricConfig)
                .filter(Objects::nonNull)
                .map(CustomMetric::getMetric)
                .collect(Collectors.toList());
    }

    @SuppressWarnings("unchecked")
    private List<CustomMetricGroup<?>> getCustomMetricGroups(List<Metric> metricList) {
        Map<MetricsSourceProvider<?>, List<CustomMetric<?>>> metricsByProvider = metricList.stream()
                .map(this.customMetrics::getCustomMetricConfig)
                .filter(Objects::nonNull)
                .collect(groupingBy(
                        CustomMetric::getMetricsSourceProvider,
                        mapping(Function.identity(), toList())
                ));

        List<CustomMetricGroup<?>> customMetricGroups = new LinkedList<>();
        for (Map.Entry<MetricsSourceProvider<?>, List<CustomMetric<?>>> entry : metricsByProvider.entrySet()) {
            customMetricGroups.add(new CustomMetricGroup(entry.getKey(), entry.getValue()));
        }
        return customMetricGroups;
    }

    private <T> Map<Metric, Double> getMetricsForGroup(CustomMetricGroup<T> group, long timeAfterStart, String processId) {
        T metricsSource = null;
        try {
            metricsSource = group.provider.getMetricsSourceForProcess(processId);
        } catch (Exception e) {
            LOGGER.warn("Could not get custom metrics source for metricSourceProvider {}, pid {} and timeAfterStart {}, " +
                    "exception message: {}", group.provider.getName(), processId, timeAfterStart, e.getMessage());
        }
        return parseMetricsForGroup(group, metricsSource);
    }

    private <T> Map<Metric, Double> parseMetricsForGroup(CustomMetricGroup<T> group, final T metricsSource) {
        return group.customMetrics.stream()
                .collect(Collectors.toMap(
                        CustomMetric::getMetric,
                        metric -> this.parseSingleMetricForMetricsSource(metric, metricsSource)
                ));
    }

    private <T> double parseSingleMetricForMetricsSource(CustomMetric<T> metric, final T metricsSource) {
        if (metricsSource == null) {
            return 0;
        }
        try {
            Double metricValue = metric.getMetricsParser().getMetricValue(metricsSource);
            LOGGER.info("Parsed a metric source for metric {} with value {}", metric.getMetric(), metricValue);
            return metricValue;
        } catch (RuntimeException e) {
            LOGGER.warn("Could not parse metric source for metric {}, exception message: {}",
                    metric.getMetric(), e.getMessage());
            return 0.;
        }
    }

    private static class CustomMetricGroup<T> {
        private MetricsSourceProvider<T> provider;
        private List<CustomMetric<T>> customMetrics;

        private CustomMetricGroup(MetricsSourceProvider<T> provider, List<CustomMetric<T>> customMetrics) {
            this.provider = provider;
            this.customMetrics = customMetrics;
        }
    }
}
