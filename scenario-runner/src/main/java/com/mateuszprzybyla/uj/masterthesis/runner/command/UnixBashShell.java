package com.mateuszprzybyla.uj.masterthesis.runner.command;

import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.file.Paths;

class UnixBashShell implements Shell {
    private static final String UNIX_PROCESS_CLASS_NAME = "java.lang.UNIXProcess";
    private final String bashPath;

    UnixBashShell(String bashPath) {
        this.bashPath = bashPath;
    }

    @Override
    public Process startProcessWithCommand(String command) throws IOException {
        ProcessBuilder builder = new ProcessBuilder(bashPath, "-c", command);
        builder.redirectErrorStream(true);
        return builder.start();
    }

    @Override
    public Process startProcessWithCommandAndFileOutputRedirection(String command, String fileName) throws IOException {
        ProcessBuilder builder = new ProcessBuilder(bashPath, "-c", command);
        builder.redirectErrorStream(true);
        builder.redirectOutput(Paths.get(fileName).toFile());
        return builder.start();
    }

    @Override
    public String getProcessId(Process process) {
        if (process.getClass().getName().equals(UNIX_PROCESS_CLASS_NAME)) {
            try {
                Field f = process.getClass().getDeclaredField("pid");
                f.setAccessible(true);
                return String.valueOf(f.getInt(process));
            } catch (Exception e) {
                throw new UnsupportedOperationException("Could not retrieve process ID for UNIXProcess", e);
            }
        }
        throw new UnsupportedOperationException("Unable to retrieve process ID. Process class is not java.lang.UNIXProcess");
    }


}
