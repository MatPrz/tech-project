package com.mateuszprzybyla.uj.masterthesis.runner.command;

import java.util.Collections;
import java.util.List;

public class ProcessResult {
    private List<String> output;
    private Integer exitCode;

    ProcessResult(List<String> output, Integer exitCode) {
        this.output = Collections.unmodifiableList(output);
        this.exitCode = exitCode;
    }

    ProcessResult(List<String> output) {
        this(output, null);
    }

    public List<String> getOutput() {
        return output;
    }

    public Integer getExitCode() {
        return exitCode;
    }
}
