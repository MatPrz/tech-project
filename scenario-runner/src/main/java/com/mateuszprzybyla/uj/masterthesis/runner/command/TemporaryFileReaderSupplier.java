package com.mateuszprzybyla.uj.masterthesis.runner.command;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.Reader;
import java.io.StringReader;
import java.nio.file.Paths;

class TemporaryFileReaderSupplier implements ReaderSupplier {
    private static final Logger LOGGER = LoggerFactory.getLogger(TemporaryFileReaderSupplier.class);
    private String fileName;

    TemporaryFileReaderSupplier(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public Reader get() {
        try {
            return new FileReader(fileName);
        } catch (FileNotFoundException e) {
            LOGGER.warn("Could not create a FileReader for fileName " + fileName, e);
        }
        return new StringReader(StringUtils.EMPTY);
    }

    @Override
    public void close() {
        LOGGER.debug("Removing a temporary file with fileName {}", fileName);
        boolean result = Paths.get(fileName).toFile().delete();
        if (result) {
            LOGGER.debug("Successfully removed {}", fileName);
        } else {
            LOGGER.warn("Could not remove a file {}", fileName);
        }
    }
}
