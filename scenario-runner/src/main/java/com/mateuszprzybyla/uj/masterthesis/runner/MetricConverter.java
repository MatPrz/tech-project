package com.mateuszprzybyla.uj.masterthesis.runner;

import com.mateuszprzybyla.uj.masterthesis.metrics.model.Metric;
import com.mateuszprzybyla.uj.masterthesis.metrics.model.input.MetricData;
import com.mateuszprzybyla.uj.masterthesis.metrics.model.input.MetricSnapshot;

import java.util.*;
import java.util.function.Function;

public class MetricConverter<T> {
    private final Function<T, Integer> timeFunction;
    private final Function<T, Map<Metric, Double>> valuesByMetricKeyFunction;

    public MetricConverter(Function<T, Integer> timeFunction,
                           Function<T, Map<Metric, Double>> valuesByMetricKeyFunction) {
        this.timeFunction = timeFunction;
        this.valuesByMetricKeyFunction = valuesByMetricKeyFunction;
    }

    public List<MetricData> createMetrics(List<T> sourceList) {
        List<T> sourceListCopy = new ArrayList<>(sourceList);
        Collections.sort(sourceListCopy, Comparator.comparing(timeFunction));
        Map<Metric, MetricData> resultMetricsByKey = new HashMap<>();
        for (T sourceItem : sourceListCopy) {
            int timeFromStart = timeFunction.apply(sourceItem);
            for (Map.Entry<Metric, Double> valueEntry : valuesByMetricKeyFunction.apply(sourceItem).entrySet()) {
                Metric metric = valueEntry.getKey();
                Double metricValue = valueEntry.getValue();
                if (!resultMetricsByKey.containsKey(metric)) {
                    MetricData metricData = new MetricData();
                    metricData.setKey(metric);
                    metricData.setData(new ArrayList<>());
                    resultMetricsByKey.put(metric, metricData);
                }
                resultMetricsByKey.get(metric).getData().add(new MetricSnapshot(timeFromStart, metricValue));
            }
        }
        return new LinkedList<>(resultMetricsByKey.values());
    }
}
