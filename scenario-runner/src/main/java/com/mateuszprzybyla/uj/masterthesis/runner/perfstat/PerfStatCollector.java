package com.mateuszprzybyla.uj.masterthesis.runner.perfstat;

import com.mateuszprzybyla.uj.masterthesis.metrics.model.input.MetricData;
import com.mateuszprzybyla.uj.masterthesis.metrics.model.runner.ScenarioConfig;
import com.mateuszprzybyla.uj.masterthesis.runner.MetricConverter;
import com.mateuszprzybyla.uj.masterthesis.runner.command.CommandExecutor;
import com.mateuszprzybyla.uj.masterthesis.runner.command.ProcessResult;
import com.mateuszprzybyla.uj.masterthesis.runner.command.RunningProcess;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CountDownLatch;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Component
public class PerfStatCollector {
    private static final Logger LOGGER = LoggerFactory.getLogger(PerfStatCollector.class);
    private static final Pattern PERF_STAT_VERSION_PATTERN = Pattern.compile("perf version ([^\\s]+)");
    private static final String PERF_VERSION_CHECK_COMMAND = "perf -v";
    private static final String PERF_STAT_COMMAND = "perf stat -p %s -e %s -x%s -I %d";

    private final CommandExecutor commandExecutor;
    private final PerfStatMetrics perfStatMetrics;
    private final String perfStatSeparator;
    private final MetricConverter<String> metricConverter;

    @Autowired
    PerfStatCollector(CommandExecutor commandExecutor, PerfStatMetrics perfStatMetrics,
                      @Qualifier("perfStatOutputMetricConverter") MetricConverter<String> metricConverter,
                      @Value("${scenario.runner.perf.stat.separator}") String perfStatSeparator) {
        this.commandExecutor = commandExecutor;
        this.perfStatMetrics = perfStatMetrics;
        this.perfStatSeparator = perfStatSeparator;
        this.metricConverter = metricConverter;
    }

    public CompletableFuture<List<MetricData>> collectPerfStatMetrics(ScenarioConfig scenarioConfig,
                                                                      RunningProcess mainProcess, CountDownLatch countDownLatch) {
        Optional<String> perfStatVersion = verifyPerfStatExistence();
        if (perfStatVersion.isPresent()) {
            LOGGER.info("Perf stat exists with version {}", perfStatVersion.get());
        } else {
            LOGGER.warn("Perf stat does not exists, returning no metric data");
            return getEmptyCompletedFuture(countDownLatch);
        }
        Optional<String> perfStatCommand = createPerfStatCommand(scenarioConfig, mainProcess.getProcessId());
        Optional<RunningProcess> optionalProcess = perfStatCommand.map(this::executeAndGetOutput);
        if (optionalProcess.isPresent()) {
            RunningProcess perfStatProcess = optionalProcess.get();
            LOGGER.info("Perf stat process started with pid {}", perfStatProcess.getProcessId());
            mainProcess.whenDone(perfStatProcess::stop);
            CompletableFuture<List<MetricData>> futureMetricData = perfStatProcess.getProcessResult()
                    .thenApply(ProcessResult::getOutput)
                    .thenApply(metricConverter::createMetrics);
            futureMetricData.handle((result, ex) -> {
                countDownLatch.countDown();
                return result;
            });
            return futureMetricData;
        }
        return getEmptyCompletedFuture(countDownLatch);
    }

    private Optional<String> verifyPerfStatExistence() {
        try {
            // TODO - check exit Code
            List<String> outputList = commandExecutor.executeAndGetOutput(PERF_VERSION_CHECK_COMMAND);
            Optional<String> output = outputList.stream().findFirst();
            return output.map(this::getPerfStatVersion);
        } catch (IOException e) {
            LOGGER.warn("Could not verify perf stat existence, assuming it's not installed...", e);
            return Optional.empty();
        }
    }

    private String getPerfStatVersion(String output) {
        Matcher matcher = PERF_STAT_VERSION_PATTERN.matcher(output);
        if (matcher.matches()) {
            return matcher.group(1);
        }
        LOGGER.warn("Perf stat version output: \"{}\" does not match expected pattern: \"{}\"",
                output, PERF_STAT_VERSION_PATTERN.toString());
        return null;
    }

    private Optional<String> createPerfStatCommand(ScenarioConfig scenarioConfig, String processId) {
        String eventsParameter = createPerfStatEventsParameter(scenarioConfig);
        if (eventsParameter.isEmpty()) {
            LOGGER.info("No perf stat events. Not starting perf stat collection...");
            return Optional.empty();
        } else {
            String perfStatCommand = String.format(PERF_STAT_COMMAND,
                    processId,
                    eventsParameter,
                    perfStatSeparator,
                    scenarioConfig.getRefreshIntervalMs());
            LOGGER.info("Perf stat events detected. Running perf stat collection command: \"{}\"", perfStatCommand);
            return Optional.of(perfStatCommand);
        }
    }

    private String createPerfStatEventsParameter(ScenarioConfig scenarioConfig) {
        return scenarioConfig.getMetricList().stream()
                .map(perfStatMetrics::getPerfStatKey)
                .filter(Objects::nonNull)
                .collect(Collectors.joining(","));
    }

    private RunningProcess executeAndGetOutput(String perfStatCommand) {
        try {
            return commandExecutor.runInTheBackground(perfStatCommand, "/tmp/metrics_output_" + System.currentTimeMillis() + ".out");
        } catch (IOException e) {
            LOGGER.warn("Error occurred during running perf stat command", e);
            return null;
        }
    }

    private CompletableFuture<List<MetricData>> getEmptyCompletedFuture(CountDownLatch countDownLatch) {
        CompletableFuture<List<MetricData>> completedFuture = CompletableFuture.completedFuture(Collections.emptyList());
        countDownLatch.countDown();
        return completedFuture;
    }
}
