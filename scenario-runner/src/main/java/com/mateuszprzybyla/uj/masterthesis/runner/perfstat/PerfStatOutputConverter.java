package com.mateuszprzybyla.uj.masterthesis.runner.perfstat;

import com.mateuszprzybyla.uj.masterthesis.metrics.model.Metric;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
class PerfStatOutputConverter {
    private static final Logger LOGGER = LoggerFactory.getLogger(PerfStatOutputConverter.class);

    private final PerfStatMetrics perfStatMetrics;
    private final Pattern perfStatPattern;

    @Autowired
    PerfStatOutputConverter(@Value("${scenario.runner.perf.stat.separator}") String perfStatSeparator,
                            PerfStatMetrics perfStatMetrics) {
        this.perfStatMetrics = perfStatMetrics;
        this.perfStatPattern = Pattern.compile("\\s*" +
                "(?<timestamp>.*)" + perfStatSeparator +
                "(?<value>.*)" + perfStatSeparator +
                "(?<unit>.*)" + perfStatSeparator +
                "(?<key>.*)" + perfStatSeparator +
                "(.*)" + perfStatSeparator +
                "(.*)");
    }


    Integer getTime(String line) {
        Matcher matcher = perfStatPattern.matcher(line);
        if (matcher.matches()) {
            String timestampStr = matcher.group("timestamp");
            Double timeStampMs = Double.valueOf(timestampStr) * 1000;
            return Math.toIntExact(Math.round(timeStampMs));
        }
        throw new IllegalArgumentException(String.format(
                "Provided line (%s) does not match format: %s", line, perfStatPattern));
    }

    Map<Metric, Double> getKeyValueItem(String line) {
        Matcher matcher = perfStatPattern.matcher(line);
        if (matcher.matches()) {
            String key = matcher.group("key");
            Metric perfStatMetricByKey = perfStatMetrics.getPerfStatMetricByKey(key);
            if (perfStatMetricByKey == null) {
                LOGGER.warn("Could not get perf stat metric for key {}", key);
                return Collections.emptyMap();
            }
            String valueStr = matcher.group("value").replace(",", ".");
            Double value;
            try {
                value = Double.valueOf(valueStr);
            } catch (NumberFormatException nfe) {
                LOGGER.warn("Unable to convert " + valueStr + " to double value for key " + key +
                        ". Setting 0 as a value");
                value = 0.;
            }
            return Collections.singletonMap(perfStatMetricByKey, value);
        }
        throw new IllegalArgumentException(String.format(
                "Provided line (%s) does not match format: %s", line, perfStatPattern));
    }
}
