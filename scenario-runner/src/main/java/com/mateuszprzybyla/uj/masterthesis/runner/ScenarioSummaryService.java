package com.mateuszprzybyla.uj.masterthesis.runner;

import com.mateuszprzybyla.uj.masterthesis.metrics.model.Metric;
import com.mateuszprzybyla.uj.masterthesis.metrics.model.input.MetricData;
import com.mateuszprzybyla.uj.masterthesis.metrics.model.input.MetricSnapshot;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
class ScenarioSummaryService {
    private static final Logger LOGGER = LoggerFactory.getLogger(ScenarioRunner.class);

    Map<Metric, Double> aggregateSum(List<MetricData> collectedMetricDatas) {
        Map<Metric, Double> aggregatedMetrics = collectedMetricDatas.stream().filter(this::isAggregable).collect(
                Collectors.toMap(MetricData::getKey, this::aggregateMetricsData));
        LOGGER.info("Creating sum aggregated summary for {} metrics", aggregatedMetrics.size());
        return aggregatedMetrics;
    }

    private boolean isAggregable(MetricData metricData) {
        return metricData.getKey().isAggregable();
    }

    private double aggregateMetricsData(MetricData metricData) {
        return metricData.getData().stream().mapToDouble(MetricSnapshot::getValue).sum();
    }
}
