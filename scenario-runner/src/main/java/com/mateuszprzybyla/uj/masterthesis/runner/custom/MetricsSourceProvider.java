package com.mateuszprzybyla.uj.masterthesis.runner.custom;

abstract class MetricsSourceProvider<T> {
    private final String name;

    protected MetricsSourceProvider(String name) {
        this.name = name;
    }

    abstract T getMetricsSourceForProcess(String processId) throws Exception;

    String getName() {
        return name;
    }
}
