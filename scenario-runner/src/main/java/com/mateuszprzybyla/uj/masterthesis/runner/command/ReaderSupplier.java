package com.mateuszprzybyla.uj.masterthesis.runner.command;

import java.io.Reader;
import java.util.function.Supplier;

interface ReaderSupplier extends Supplier<Reader>, AutoCloseable {
    @Override
    default void close() {
    }
}