package com.mateuszprzybyla.uj.masterthesis.runner.custom;

import com.mateuszprzybyla.uj.masterthesis.metrics.model.Metric;

import java.util.Map;

class ScenarioMetricsSnapshot {
    private int timeFromStart;
    private Map<Metric, Double> metricsValues;

    ScenarioMetricsSnapshot(int timeFromStart, Map<Metric, Double> metricsValues) {
        this.timeFromStart = timeFromStart;
        this.metricsValues = metricsValues;
    }

    int getTimeFromStart() {
        return timeFromStart;
    }

    Map<Metric, Double> getMetricsValues() {
        return metricsValues;
    }
}
