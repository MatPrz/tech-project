package com.mateuszprzybyla.uj.masterthesis.runner.custom;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

class TabSeparatedResultParser implements MetricsParser<List<String>> {
    private static final Logger LOGGER = LoggerFactory.getLogger(TabSeparatedResultParser.class);

    private final int rowIndex;
    private final int columnIndex;

    TabSeparatedResultParser(int rowIndex, int columnIndex) {
        this.rowIndex = rowIndex;
        this.columnIndex = columnIndex;
    }

    @Override
    public Double getMetricValue(List<String> result) {
        LOGGER.debug("Parsing metric value from tab separated result");
        if (CollectionUtils.isEmpty(result)) {
            throw new IllegalArgumentException("Input result is empty");
        }
        if (result.size() <= rowIndex) {
            throw new IllegalArgumentException(String.format(
                    "Number of rows (%d) is less than expected row index (%d)", result.size(), rowIndex));
        }
        String[] splitted = result.get(rowIndex).split("\\s+");
        if (splitted.length <= columnIndex) {
            throw new IllegalArgumentException(String.format(
                    "Number of columns (%d) is less than expected column index (%d)", splitted.length, columnIndex));
        }
        Double value = Double.valueOf(splitted[columnIndex]);
        LOGGER.debug("Parsed metric value {}", value);
        return value;
    }
}
