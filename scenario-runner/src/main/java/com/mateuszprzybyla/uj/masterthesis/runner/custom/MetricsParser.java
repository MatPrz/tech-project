package com.mateuszprzybyla.uj.masterthesis.runner.custom;

interface MetricsParser<S> {
    Double getMetricValue(S metricsSource);
}
