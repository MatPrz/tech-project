package com.mateuszprzybyla.uj.masterthesis.runner;

import com.mateuszprzybyla.uj.masterthesis.metrics.model.Metric;
import com.mateuszprzybyla.uj.masterthesis.metrics.model.input.MetricData;
import com.mateuszprzybyla.uj.masterthesis.metrics.model.runner.ScenarioConfig;
import com.mateuszprzybyla.uj.masterthesis.metrics.model.runner.ScenarioResult;
import com.mateuszprzybyla.uj.masterthesis.runner.command.CommandExecutor;
import com.mateuszprzybyla.uj.masterthesis.runner.command.ProcessResult;
import com.mateuszprzybyla.uj.masterthesis.runner.command.RunningProcess;
import com.mateuszprzybyla.uj.masterthesis.runner.custom.CustomMetricsCollectorScheduler;
import com.mateuszprzybyla.uj.masterthesis.runner.perfstat.PerfStatCollector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

@Component
class ScenarioRunner {
    private static final Logger LOGGER = LoggerFactory.getLogger(ScenarioRunner.class);

    private final CustomMetricsCollectorScheduler customMetricsCollectorScheduler;
    private final CommandExecutor commandExecutor;
    private final PerfStatCollector perfStatCollector;
    private final ScenarioSummaryService scenarioSummaryService;
    private final int defaultTimeout;

    @Autowired
    ScenarioRunner(PerfStatCollector perfStatCollector, ScenarioSummaryService scenarioSummaryService,
                   CustomMetricsCollectorScheduler customMetricsCollectorScheduler,
                   CommandExecutor commandExecutor,
                   @Value("${scenario.runner.default.timeout.seconds}") int defaultTimeout) {
        this.perfStatCollector = perfStatCollector;
        this.scenarioSummaryService = scenarioSummaryService;
        this.customMetricsCollectorScheduler = customMetricsCollectorScheduler;
        this.commandExecutor = commandExecutor;
        this.defaultTimeout = defaultTimeout;
    }

    /*
    TODO: refactor into short methods ~ steps
     */
    Optional<ScenarioResult> run(ScenarioConfig scenarioConfig) {
        RunningProcess runningProcess;
        try {
            String fileName = "/tmp/metrics_output_" + System.currentTimeMillis() + ".out";
            runningProcess = commandExecutor.runInTheBackground(scenarioConfig.getStartCommand(), fileName);
            LOGGER.info("Monitored application has been started, PID = " + runningProcess.getProcessId());
        } catch (Exception e) {
            LOGGER.error("Error occurred during running the application", e);
            return Optional.empty();
        }
        long startTimeMillis = System.currentTimeMillis();
        CountDownLatch countDownLatch = new CountDownLatch(2);
        CompletableFuture<List<MetricData>> executionScenarioCompletableFuture = customMetricsCollectorScheduler
                .startCollecting(startTimeMillis, runningProcess, scenarioConfig, countDownLatch);
        CompletableFuture<List<MetricData>> futurePerfStatMetrics = perfStatCollector
                .collectPerfStatMetrics(scenarioConfig, runningProcess, countDownLatch);
        List<MetricData> metricDatas, perfStatMetricDatas;
        try {
            boolean hasTimedOut = !countDownLatch.await(getTimeout(scenarioConfig), TimeUnit.SECONDS);
            if (hasTimedOut) {
                LOGGER.warn("Timeout occurred during getting result of the scenario");
                return Optional.empty();
            }
            ProcessResult processResult = runningProcess.getProcessResult().getNow(null);
            if (processResult == null) {
                LOGGER.warn("No process results available, returning no results");
                return Optional.empty();
            }
            int exitCode = processResult.getExitCode();
            if (exitCode != 0) {
                LOGGER.warn("Process exited with exitCode {}, returning no results", exitCode);
                return Optional.empty();
            }
            metricDatas = executionScenarioCompletableFuture.getNow(Collections.emptyList());
            LOGGER.info("Metric are ready: {}", metricDatas);
            perfStatMetricDatas = futurePerfStatMetrics.getNow(Collections.emptyList());
            LOGGER.info("Perf stat metricDatas are ready: {}", perfStatMetricDatas);
        } catch (InterruptedException e) {
            LOGGER.error("Error occurred during getting result of the scenario", e);
            return Optional.empty();
        }
        LOGGER.debug("Merging metricDatas...");
        List<MetricData> collectedMetricDatas = mergeMetrics(metricDatas, perfStatMetricDatas);

        Map<Metric, Double> aggregatedMetrics = scenarioSummaryService.aggregateSum(collectedMetricDatas);
        aggregatedMetrics.put(Metric.TOTAL_RUN_TIME, Double.valueOf(runningProcess.getTotalTimeMs().getNow(0L)));
        ScenarioResult scenarioResult = new ScenarioResult(aggregatedMetrics, collectedMetricDatas);
        scenarioResult.setHostname(resolveHostName());
        return Optional.of(scenarioResult);
    }

    private int getTimeout(ScenarioConfig scenarioConfig) {
        if (scenarioConfig.getTimeout() == null || scenarioConfig.getTimeout() == 0) {
            LOGGER.info("Scenario config timeout not specified, using default: {}", defaultTimeout);
            return defaultTimeout;
        }
        return scenarioConfig.getTimeout();
    }

    private List<MetricData> mergeMetrics(List<MetricData> metricDatas, List<MetricData> perfStatMetricDatas) {
        List<MetricData> mergedMetricDatas = new ArrayList<>(metricDatas.size() + perfStatMetricDatas.size());
        mergedMetricDatas.addAll(metricDatas);
        mergedMetricDatas.addAll(perfStatMetricDatas);
        return mergedMetricDatas;
    }

    private String resolveHostName() {
        try {
            String hostName = InetAddress.getLocalHost().getHostName();
            LOGGER.info("Hostname resolved: " + hostName);
            return hostName;
        } catch (UnknownHostException e) {
            LOGGER.warn("Could not resolve host name of the machine, using 'unknown' as host name", e);
            return "unknown";
        }
    }
}
