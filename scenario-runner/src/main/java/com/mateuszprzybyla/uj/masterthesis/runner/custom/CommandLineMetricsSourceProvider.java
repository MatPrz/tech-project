package com.mateuszprzybyla.uj.masterthesis.runner.custom;

import com.mateuszprzybyla.uj.masterthesis.runner.command.CommandExecutor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

class CommandLineMetricsSourceProvider extends MetricsSourceProvider<List<String>> {
    private static final Logger LOGGER = LoggerFactory.getLogger(CommandLineMetricsSourceProvider.class);

    private final CommandExecutor commandExecutor;
    private final CustomMetricCommand command;

    public CommandLineMetricsSourceProvider(CommandExecutor commandExecutor, CustomMetricCommand command, String name) {
        super(name);
        this.commandExecutor = commandExecutor;
        this.command = command;
    }

    @Override
    public List<String> getMetricsSourceForProcess(String processId) throws Exception {
        try {
            String commandForProcessId = command.getForProcessId(processId);
            LOGGER.info("Creating command line custom metrics source for command: \"{}\" and PID {}", commandForProcessId, processId);
            // TODO - check exit code
            List<String> output = commandExecutor.executeAndGetOutput(commandForProcessId);
            output = output.stream().map(String::trim).collect(Collectors.toList());
            LOGGER.info("Retrieved an output (after trimming): \"{}\"", output);
            return output;
        } catch (IOException e) {
            LOGGER.warn("Could not create command line perfstat source for pid {}", processId);
            throw e;
        }
    }
}
