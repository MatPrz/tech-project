package com.mateuszprzybyla.uj.masterthesis.runner.command;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

class ProcessOutputSupplier extends OutputSupplier {
    private static final Logger LOGGER = LoggerFactory.getLogger(ProcessOutputSupplier.class);
    private String processId;
    private ReaderSupplier readerSupplier;

    ProcessOutputSupplier(String processId, ReaderSupplier readerSupplier) {
        this.processId = processId;
        this.readerSupplier = readerSupplier;
    }

    @Override
    public List<String> getOutput() {
        try (ReaderSupplier readerSupplier = this.readerSupplier) {
            LOGGER.debug("Reading process output for pid {}", processId);
            List<String> output = IOUtils.readLines(readerSupplier.get());
            LOGGER.debug("Successfully read output for pid {}", processId);
            return output;
        } catch (IOException e) {
            LOGGER.warn("Could not read process output for pid " + processId, e);
            return Collections.emptyList();
        }
    }
}