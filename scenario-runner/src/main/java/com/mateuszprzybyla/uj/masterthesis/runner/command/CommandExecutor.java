package com.mateuszprzybyla.uj.masterthesis.runner.command;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.function.Supplier;

@Component
public class CommandExecutor {
    private static final Logger LOGGER = LoggerFactory.getLogger(CommandExecutor.class);

    private final Shell shell;

    @Autowired
    public CommandExecutor(Shell shell) {
        this.shell = shell;
    }

    public RunningProcess runInTheBackground(String command, String fileName) throws IOException {
        Process process = shell.startProcessWithCommandAndFileOutputRedirection(command, fileName);
        long startTime = System.currentTimeMillis();
        String processId = shell.getProcessId(process);
        CompletableFuture<Long> totalTimeFuture = new CompletableFuture<>();
        Supplier<List<String>> outputSupplier = new ProcessOutputSupplier(processId, new TemporaryFileReaderSupplier(fileName));
        CompletableFuture<ProcessResult> processResultFuture = new CompletableFuture<>();
        CompletableFuture.runAsync(() -> {
            LOGGER.debug("Waiting in background for process {} to finish", processId);
            try {
                processResultFuture.complete(waitAndGetOutput(process, processId, outputSupplier));
                LOGGER.info("Completed the running process {} after successful wait", processId);
            } catch (InterruptedException e) {
                processResultFuture.completeExceptionally(e);
                LOGGER.warn("Could not successfully wait for process with pid {} to end, completed exceptionally", processId);
            }
            totalTimeFuture.complete(System.currentTimeMillis() - startTime);
        });
        return new RunningProcess(processId, processResultFuture, totalTimeFuture, () -> {
            LOGGER.debug("Received a request to stop the process {}, getting current output", processId);
            if (!processResultFuture.isDone()) {
                processResultFuture.complete(new ProcessResult(outputSupplier.get()));
                totalTimeFuture.complete(System.currentTimeMillis() - startTime);
                LOGGER.debug("Destroying a process {}", processId);
                process.destroy();
                LOGGER.debug("Process {} has been destroyed. Completing running process", processId);
            } else {
                LOGGER.debug("Process {} has alread been destroyed, no action taken", processId);
            }
        });
    }

    public List<String> executeAndGetOutput(String command) throws IOException {
        Process process = shell.startProcessWithCommand(command);
        String processId = "unknown";
        try {
            return waitAndGetOutput(process, processId, new ProcessOutputSupplier(processId,
                    () -> new InputStreamReader(process.getInputStream())))
                    .getOutput();
        } catch (InterruptedException e) {
            LOGGER.warn("Could not successfully wait for process with pid {} to end", processId);
            throw new IOException(e);
        }
    }

    private ProcessResult waitAndGetOutput(Process process, String processId,
                                           Supplier<List<String>> outputProducer) throws InterruptedException {
        int exitCode = process.waitFor();
        LOGGER.info("Waiting for process {} ended with success, exit code: {} reading the output...", processId, exitCode);
        return new ProcessResult(outputProducer.get(), exitCode);
    }
}
