package com.mateuszprzybyla.uj.masterthesis.runner.custom;

import com.mateuszprzybyla.uj.masterthesis.metrics.model.Metric;

class CustomMetric<T> {
    private final Metric metric;
    private final MetricsSourceProvider<T> metricsSourceProvider;
    private final MetricsParser<T> metricsParser;

    CustomMetric(Metric metric, MetricsSourceProvider<T> metricsSourceProvider,
                 MetricsParser<T> metricsParser) {
        this.metric = metric;
        this.metricsSourceProvider = metricsSourceProvider;
        this.metricsParser = metricsParser;
    }

    Metric getMetric() {
        return metric;
    }

    MetricsSourceProvider<T> getMetricsSourceProvider() {
        return metricsSourceProvider;
    }

    MetricsParser<T> getMetricsParser() {
        return metricsParser;
    }
}
