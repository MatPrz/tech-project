package com.mateuszprzybyla.uj.masterthesis.runner.custom;

import com.mateuszprzybyla.uj.masterthesis.metrics.model.Metric;

import java.util.Map;

class SupportedCustomMetrics {
    private final Map<Metric, CustomMetric<?>> customMetricConfigMap;

    public SupportedCustomMetrics(Map<Metric, CustomMetric<?>> customMetricConfigMap) {
        this.customMetricConfigMap = customMetricConfigMap;
    }

    CustomMetric<?> getCustomMetricConfig(Metric metric) {
        return customMetricConfigMap.get(metric);
    }
}
