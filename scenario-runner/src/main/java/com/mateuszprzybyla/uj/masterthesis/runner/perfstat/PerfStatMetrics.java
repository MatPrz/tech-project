package com.mateuszprzybyla.uj.masterthesis.runner.perfstat;

import com.mateuszprzybyla.uj.masterthesis.metrics.model.Metric;

import java.util.Map;
import java.util.stream.Collectors;

class PerfStatMetrics {
    private final Map<Metric, String> perfStatKeyMap;
    private final Map<String, Metric> perfMetricsByKey;

    PerfStatMetrics(Map<Metric, String> perfStatKeyMap) {
        this.perfStatKeyMap = perfStatKeyMap;
        this.perfMetricsByKey = perfStatKeyMap.entrySet().stream().collect(
                Collectors.toMap(Map.Entry::getValue, Map.Entry::getKey));
    }

    String getPerfStatKey(Metric metric) {
        return perfStatKeyMap.get(metric);
    }

    Metric getPerfStatMetricByKey(String key) {
        return perfMetricsByKey.get(key);
    }
}
