package com.mateuszprzybyla.uj.masterthesis.runner.command;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Component
class ShellFactoryBean implements FactoryBean<Shell> {
    private final Environment environment;

    @Autowired
    ShellFactoryBean(Environment environment) {
        this.environment = environment;
    }

    @Override
    public Shell getObject() {
        String osName = getOperatingSystem();
        if (StringUtils.equals(osName, "Linux")) {
            String binBashPath = environment.getRequiredProperty("scenario.runner.bash.path");
            return new UnixBashShell(binBashPath);
        }
        throw new IllegalStateException(osName + " is not supported operating system");
    }

    @Override
    public Class<?> getObjectType() {
        String osName = getOperatingSystem();
        if (StringUtils.equals(osName, "Linux")) {
            return UnixBashShell.class;
        }
        throw new IllegalStateException(osName + " is not supported operating system");
    }

    @Override
    public boolean isSingleton() {
        return true;
    }

    private String getOperatingSystem() {
        String osName = environment.getProperty("os.name");
        if (StringUtils.contains(osName, "Linux")) {
            return "Linux";
        }
        return osName;
    }
}
