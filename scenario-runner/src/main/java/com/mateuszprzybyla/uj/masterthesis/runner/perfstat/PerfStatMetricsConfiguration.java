package com.mateuszprzybyla.uj.masterthesis.runner.perfstat;

import com.mateuszprzybyla.uj.masterthesis.metrics.model.Metric;
import com.mateuszprzybyla.uj.masterthesis.runner.MetricConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.EnumMap;
import java.util.Map;

@Configuration
class PerfStatMetricsConfiguration {
    @Bean
    @Autowired
    MetricConverter<String> perfStatOutputMetricConverter(PerfStatOutputConverter perfStatOutputConverter) {
        return new MetricConverter<>(perfStatOutputConverter::getTime, perfStatOutputConverter::getKeyValueItem);
    }
    
    @Bean
    PerfStatMetrics perfStatMetrics() {
        Map<Metric, String> perfStatKeyMap = new EnumMap<>(Metric.class);
        // hardware events
        perfStatKeyMap.put(Metric.BRANCHES, "branches");
        perfStatKeyMap.put(Metric.BRANCH_MISSES, "branch-misses");
        perfStatKeyMap.put(Metric.BUS_CYCLES, "bus-cycles");
        perfStatKeyMap.put(Metric.CPU_CYCLES, "cycles");
        perfStatKeyMap.put(Metric.INSTRUCTIONS, "instructions");
        perfStatKeyMap.put(Metric.REF_CYCLES, "ref-cycles");
        perfStatKeyMap.put(Metric.IDLE_CYCLES_FRONTEND, "idle-cycles-frontend");
        perfStatKeyMap.put(Metric.IDLE_CYCLES_BACKEND, "idle-cycles-backend");
        // software events
        perfStatKeyMap.put(Metric.ALIGNMENT_FAULTS, "alignment-faults");
        perfStatKeyMap.put(Metric.CONTEXT_SWITCHES, "context-switches");
        perfStatKeyMap.put(Metric.CPU_CLOCK, "cpu-clock");
        perfStatKeyMap.put(Metric.CPU_MIGRATIONS, "cpu-migrations");
        perfStatKeyMap.put(Metric.EMULATION_FAULTS, "emulation-faults");
        perfStatKeyMap.put(Metric.MAJOR_FAULTS, "major-faults");
        perfStatKeyMap.put(Metric.MINOR_FAULTS, "minor-faults");
        perfStatKeyMap.put(Metric.PAGE_FAULTS, "page-faults");
        perfStatKeyMap.put(Metric.TASK_CLOCK, "task-clock");
        // cache events
        perfStatKeyMap.put(Metric.L1_DCACHE_LOAD_MISSES, "L1-dcache-load-misses");
        perfStatKeyMap.put(Metric.L1_DCACHE_LOADS, "L1-dcache-loads");
        perfStatKeyMap.put(Metric.L1_DCACHE_PREFETCH_MISSES, "L1-dcache-prefetch-misses");
        perfStatKeyMap.put(Metric.L1_DCACHE_PREFETCHES, "L1-dcache-prefetches");
        perfStatKeyMap.put(Metric.L1_DCACHE_STORE_MISSES, "L1-dcache-store-misses");
        perfStatKeyMap.put(Metric.L1_DCACHE_STORES, "L1-dcache-stores");
        perfStatKeyMap.put(Metric.L1_ICACHE_LOAD_MISSES, "L1-icache-load-misses");
        perfStatKeyMap.put(Metric.L1_ICACHE_LOADS, "L1-icache-loads");
        perfStatKeyMap.put(Metric.L1_ICACHE_PREFETCH_MISSES, "L1-icache-prefetch-misses");
        perfStatKeyMap.put(Metric.L1_ICACHE_PREFETCHES, "L1-icache-prefetches");
        perfStatKeyMap.put(Metric.LLC_LOAD_MISSES, "LLC-load-misses");
        perfStatKeyMap.put(Metric.LLC_LOADS, "LLC-loads");
        perfStatKeyMap.put(Metric.LLC_PREFETCH_MISSES, "LLC-prefetch-misses");
        perfStatKeyMap.put(Metric.LLC_PREFETCHES, "LLC-prefetches");
        perfStatKeyMap.put(Metric.LLC_STORE_MISSES, "LLC-store-misses");
        perfStatKeyMap.put(Metric.LLC_STORES, "LLC-stores");
        perfStatKeyMap.put(Metric.DTLB_LOAD_MISSES, "dTLB-load-misses");
        perfStatKeyMap.put(Metric.DTLB_LOADS, "dTLB-loads");
        perfStatKeyMap.put(Metric.DTLB_PREFETCH_MISSES, "dTLB-prefetch-misses");
        perfStatKeyMap.put(Metric.DTLB_PREFETCHES, "dTLB-prefetches");
        perfStatKeyMap.put(Metric.DTLB_STORE_MISSES, "dTLB-store-misses");
        perfStatKeyMap.put(Metric.DTLB_STORES, "dTLB-load-stores");
        perfStatKeyMap.put(Metric.ITLB_LOAD_MISSES, "iTLB-load-misses");
        perfStatKeyMap.put(Metric.ITLB_LOADS, "iTLB-loads");

        return new PerfStatMetrics(perfStatKeyMap);
    }

}
