package com.mateuszprzybyla.uj.masterthesis.runner;

import com.mateuszprzybyla.uj.masterthesis.metrics.model.Metric;
import com.mateuszprzybyla.uj.masterthesis.metrics.model.input.MetricData;
import com.mateuszprzybyla.uj.masterthesis.metrics.model.input.MetricSnapshot;
import com.mateuszprzybyla.uj.masterthesis.metrics.model.runner.ScenarioConfig;
import com.mateuszprzybyla.uj.masterthesis.runner.command.ProcessResult;

import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class MockData {
    public static final double DEFAULT_VALUE = 0.;
    public static final long MONITORED_APP_TOTAL_TIME = 1234L;
    public static final String MONITORED_APP_PID = "123";
    public static final int REFRESH_INTERVAL_MS = 250;
    public static final String VALID_PERF_STAT_COMMAND = "perf stat -p 123 -e branches,cycles -x: -I 250";
    public static final String PERF_SEPARATOR = ":";
    public static final int DEFAULT_TIMEOUT = 1;
    public static final String COMMAND = "command";

    public static ScenarioConfig mockScenarioConfigForMetrics(Metric... metrics) {
        ScenarioConfig scenarioConfig = new ScenarioConfig();
        scenarioConfig.setMetricList(Arrays.asList(metrics));
        scenarioConfig.setRefreshIntervalMs(REFRESH_INTERVAL_MS);
        return scenarioConfig;
    }

    public static ScenarioConfig mockScenarioConfigForDefaultCommand() {
        ScenarioConfig scenarioConfig = new ScenarioConfig();
        scenarioConfig.setStartCommand(COMMAND);
        return scenarioConfig;
    }

    public static ScenarioConfig mockScenarioConfigForTimeoutMs(int timeoutMs) {
        ScenarioConfig scenarioConfig = new ScenarioConfig();
        scenarioConfig.setStartCommand(COMMAND);
        scenarioConfig.setTimeout((int) Math.round(timeoutMs / 1000.));
        return scenarioConfig;
    }

    public static ScenarioConfig mockScenarioConfigForRefreshIntervalAndMetrics(int refreshInterval, List<Metric> metrics) {
        ScenarioConfig scenarioConfig = new ScenarioConfig();
        scenarioConfig.setStartCommand(COMMAND);
        scenarioConfig.setMetricList(metrics);
        scenarioConfig.setRefreshIntervalMs(refreshInterval);
        return scenarioConfig;
    }

    public static CompletableFuture<ProcessResult> mockProcessResult(int exitCode, List<String> output) {
        ProcessResult processResult = mock(ProcessResult.class);
        when(processResult.getExitCode()).thenReturn(exitCode);
        when(processResult.getOutput()).thenReturn(output);
        return CompletableFuture.completedFuture(processResult);
    }

    public static MetricData mockMetricData(Metric key, Object... data) {
        MetricData metricData = new MetricData();
        metricData.setKey(key);
        List<MetricSnapshot> snapshots = new ArrayList<>(data.length / 2);
        for (int i = 0; i < data.length / 2; i++) {
            snapshots.add(new MetricSnapshot((int) data[2 * i], (double) data[2 * i + 1]));
        }
        metricData.setData(snapshots);
        return metricData;
    }

    public static CompletableFuture<List<MetricData>> mockFutureMetricData(int delayMs, Object... data) {
        List<MetricData> metricDataList = new ArrayList<>();
        for (int i = 0; i < data.length / 3; i++) {
            metricDataList.add(mockMetricData((Metric) data[3 * i], data[3 * i + 1], data[3 * i + 2]));
        }
        return CompletableFuture.supplyAsync(() -> {
            try {
                TimeUnit.MILLISECONDS.sleep(delayMs);
            } catch (InterruptedException ignored) {
            }
            return metricDataList;
        });
    }

    public static class ConverterSource {
        private int time;
        private Map<Metric, Double> valuesByMetricKey = new HashMap<>();

        public ConverterSource(int time, Object... data) {
            this.time = time;
            for (int i = 0; i < data.length / 2; i++) {
                this.valuesByMetricKey.put((Metric) data[2 * i], (double) data[2 * i + 1]);
            }
        }

        public Integer getTime() {
            return time;
        }

        public Map<Metric, Double> getValuesByMetricKey() {
            return valuesByMetricKey;
        }
    }
}
