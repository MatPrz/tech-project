package com.mateuszprzybyla.uj.masterthesis.runner.command;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static com.mateuszprzybyla.uj.masterthesis.runner.MockData.COMMAND;
import static com.mateuszprzybyla.uj.masterthesis.runner.MockData.MONITORED_APP_PID;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class CommandExecutorTest {
    private static final Path FILE_PATH = Paths.get("/tmp", "mockFileName.txt");
    private static final String FILE_PATH_STR = Paths.get("/tmp", "mockFileName.txt").toString();
    private static final List<String> PROCESS_OUTPUT = Arrays.asList("process output", "line 2");
    private static final String PROCESS_OUTPUT_STR = PROCESS_OUTPUT.stream().collect(Collectors.joining(System.lineSeparator()));
    private static final int EXIT_CODE = 123;
    @InjectMocks
    private CommandExecutor commandExecutor;
    @Mock
    private Shell shell;
    @Mock
    private Process process;
    private InputStream stubInputStream = IOUtils.toInputStream(PROCESS_OUTPUT_STR, StandardCharsets.UTF_8);
    private CompletableFuture<Integer> futureExitCode;

    @Before
    public void setUp() throws Exception {
        this.futureExitCode = new CompletableFuture<>();
        when(shell.getProcessId(process)).thenReturn(MONITORED_APP_PID);
        when(shell.startProcessWithCommandAndFileOutputRedirection(COMMAND, FILE_PATH_STR)).thenAnswer(new Answer<Process>() {
            @Override
            public Process answer(InvocationOnMock invocation) throws Throwable {
                Files.deleteIfExists(FILE_PATH);
                File file = Files.createFile(FILE_PATH).toFile();
                FileUtils.writeStringToFile(file, PROCESS_OUTPUT_STR, StandardCharsets.UTF_8);
                return process;
            }
        });
        when(shell.startProcessWithCommand(COMMAND)).thenReturn(process);
        when(process.waitFor()).thenAnswer(new Answer<Integer>() {
            @Override
            public Integer answer(InvocationOnMock invocation) throws Throwable {
                return futureExitCode.get();
            }
        });
        when(process.getInputStream()).thenReturn(stubInputStream);
    }

    @Test
    public void shouldReturnRunningProcessWithIdReturnedByShell() throws Exception {
        RunningProcess runningProcess = commandExecutor.runInTheBackground(COMMAND, FILE_PATH_STR);

        assertThat(runningProcess.getProcessId(), equalTo(MONITORED_APP_PID));
    }

    @Test
    public void shouldRunInTheBackgroundAndGiveNonCompleteProcessResult() throws Exception {
        RunningProcess runningProcess = commandExecutor.runInTheBackground(COMMAND, FILE_PATH_STR);

        assertFalse(runningProcess.getProcessResult().isDone());
        assertFalse(runningProcess.getTotalTimeMs().isDone());
    }

    @Test
    public void shouldRunInTheBackgroundAndCompleteWhenAsynchronouslyWaitedForTheOutput() throws Exception {
        RunningProcess runningProcess = commandExecutor.runInTheBackground(COMMAND, FILE_PATH_STR);
        futureExitCode.complete(EXIT_CODE);

        ProcessResult processResult = runningProcess.getProcessResult().get(10, TimeUnit.SECONDS);
        assertThat(processResult.getOutput(), equalTo(PROCESS_OUTPUT));
        assertThat(processResult.getExitCode(), equalTo(EXIT_CODE));
        Long totalTimeMs = runningProcess.getTotalTimeMs().get(10, TimeUnit.SECONDS);
        assertThat(totalTimeMs, greaterThanOrEqualTo(0L));
    }

    @Test
    public void shouldRemoveTheTemporaryFileCreatedByShell() throws Exception {
        RunningProcess runningProcess = commandExecutor.runInTheBackground(COMMAND, FILE_PATH_STR);
        futureExitCode.complete(EXIT_CODE);

        runningProcess.getProcessResult().get(10, TimeUnit.SECONDS);
        assertFalse(Files.exists(FILE_PATH));
    }

    @Test
    public void shouldInvokeStopActionOnReturnedProcessWhenThisProcessStopsOnDemand() throws Exception {
        RunningProcess runningProcess = commandExecutor.runInTheBackground(COMMAND, FILE_PATH_STR);
        runningProcess.stop();

        ProcessResult processResult = runningProcess.getProcessResult().getNow(null);
        assertThat(processResult.getOutput(), equalTo(PROCESS_OUTPUT));
        assertThat(processResult.getExitCode(), nullValue());
        Long totalTimeMs = runningProcess.getTotalTimeMs().get(10, TimeUnit.SECONDS);
        assertThat(totalTimeMs, greaterThanOrEqualTo(0L));
        verify(process).destroy();
    }

    @Test
    public void shouldNotDoAnythingOnStopRequestWhenAlreadyCompleted() throws Exception {
        RunningProcess runningProcess = commandExecutor.runInTheBackground(COMMAND, FILE_PATH_STR);
        futureExitCode.complete(EXIT_CODE);
        runningProcess.getProcessResult().get(10, TimeUnit.SECONDS);
        runningProcess.stop();

        ProcessResult processResult = runningProcess.getProcessResult().get(10, TimeUnit.SECONDS);
        assertThat(processResult.getOutput(), equalTo(PROCESS_OUTPUT));
        assertThat(processResult.getExitCode(), equalTo(EXIT_CODE));
        Long totalTimeMs = runningProcess.getTotalTimeMs().get(10, TimeUnit.SECONDS);
        assertThat(totalTimeMs, greaterThanOrEqualTo(0L));
        verify(process, never()).destroy();
    }

    @Test
    public void shouldRunInTheBackgroundAndCompleteExceptionallyWhenInterruptedDuringAsynchronousWait() throws Exception {
        reset(process);
        when(process.waitFor()).thenThrow(new InterruptedException());

        RunningProcess runningProcess = commandExecutor.runInTheBackground(COMMAND, FILE_PATH_STR);

        CompletableFuture<ProcessResult> futureProcessResult = runningProcess.getProcessResult();
        try {
            futureProcessResult.get(10, TimeUnit.SECONDS);
        } catch (ExecutionException e) {
            assertTrue(futureProcessResult.isCompletedExceptionally());
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    public void shouldExecuteAndGetOutputSynchronously() throws Exception {
        futureExitCode.complete(EXIT_CODE);
        List<String> output = commandExecutor.executeAndGetOutput(COMMAND);

        assertThat(output, equalTo(PROCESS_OUTPUT));
    }

    @Test(expected = IOException.class)
    public void shouldThrowIOExceptionWhenExecutingSynchronouslyAndInterrupted() throws Exception {
        reset(process);
        when(process.waitFor()).thenThrow(new InterruptedException());

        commandExecutor.executeAndGetOutput(COMMAND);
    }
}