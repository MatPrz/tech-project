package com.mateuszprzybyla.uj.masterthesis.runner.perfstat;

import com.mateuszprzybyla.uj.masterthesis.metrics.model.Metric;
import org.junit.Test;

import java.util.Collections;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;

public class PerfStatMetricsTest {
    private final Metric EXISTING_METRIC = Metric.BRANCHES;
    private final Metric NON_EXISTING_METRIC = Metric.MEMORY_USAGE;
    private final PerfStatMetrics perfStatMetrics = new PerfStatMetrics(
            Collections.singletonMap(EXISTING_METRIC, "branches"));

    @Test
    public void shouldReturnKeyForExistingMetric() {
        assertThat(perfStatMetrics.getPerfStatKey(EXISTING_METRIC), equalTo("branches"));
    }

    @Test
    public void shouldReturnNullKeyForNonExistingMetric() {
        assertThat(perfStatMetrics.getPerfStatKey(NON_EXISTING_METRIC), nullValue());
    }

    @Test
    public void shouldReturnMetricForExistingKey() {
        assertThat(perfStatMetrics.getPerfStatMetricByKey("branches"), equalTo(EXISTING_METRIC));
    }

    @Test
    public void shouldReturnNullMetricForNonExistingKey() {
        assertThat(perfStatMetrics.getPerfStatMetricByKey("doesnotexist"), nullValue());
    }
}