package com.mateuszprzybyla.uj.masterthesis.runner.command;

import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static com.mateuszprzybyla.uj.masterthesis.runner.MockData.MONITORED_APP_PID;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.Matchers.empty;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class ProcessOutputSupplierTest {
    private static final List<String> OUTPUT = Arrays.asList("mock", "output");
    private static final String OUTPUT_STRING = OUTPUT.stream().collect(Collectors.joining(System.lineSeparator()));
    private final Reader stubReader = new InputStreamReader(IOUtils.toInputStream(OUTPUT_STRING, StandardCharsets.UTF_8));
    private ProcessOutputSupplier processOutputSupplier;
    @Mock
    private ReaderSupplier readerSupplier;
    @Mock
    private Reader errorReader;

    @Before
    public void setUp() throws Exception {
        this.processOutputSupplier = new ProcessOutputSupplier(MONITORED_APP_PID, readerSupplier);
        when(readerSupplier.get()).thenReturn(stubReader);
        when(errorReader.read(any(char[].class), anyInt(), anyInt())).thenThrow(new IOException());
    }

    @Test
    public void shouldReturnOutputOfTheReaderProvidedByTheSupplier() {
        List<String> output = processOutputSupplier.get();

        assertThat(output, equalTo(OUTPUT));
    }

    @Test
    public void shouldCloseTheReaderSupplier() {
        processOutputSupplier.get();

        verify(readerSupplier).close();
    }

    @Test
    public void shouldReturnEmptyOutputIfReadingFailed() {
        reset(readerSupplier);
        when(readerSupplier.get()).thenReturn(errorReader);

        List<String> output = processOutputSupplier.get();

        assertThat(output, empty());
    }

    @Test
    public void shouldNotPerformReadingTheOutputTwice() {
        processOutputSupplier.get();
        processOutputSupplier.get();

        verify(readerSupplier, times(1)).get();

        processOutputSupplier.getOutput();
        processOutputSupplier.getOutput();

        verify(readerSupplier, times(3)).get();
    }
}