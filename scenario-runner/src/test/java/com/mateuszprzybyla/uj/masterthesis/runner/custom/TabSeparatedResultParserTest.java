package com.mateuszprzybyla.uj.masterthesis.runner.custom;

import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

public class TabSeparatedResultParserTest {
    @Test
    public void shouldReturnValidDoubleForSingleSpaceInput() {
        TabSeparatedResultParser parser = new TabSeparatedResultParser(1, 3);
        assertThat(parser.getMetricValue(Arrays.asList("1 2 3 4", "5 6 7 8")), equalTo(8.));
    }

    @Test
    public void shouldReturnValidDoubleForMultipleSpacesInput() {
        TabSeparatedResultParser parser = new TabSeparatedResultParser(1, 3);
        assertThat(parser.getMetricValue(Arrays.asList("1  2  3   4", "5   6   7   8")), equalTo(8.));
    }

    @Test
    public void shouldReturnValidDoubleForInputWithOtherWhiteSpaceCharacters() {
        TabSeparatedResultParser parser = new TabSeparatedResultParser(1, 3);
        assertThat(parser.getMetricValue(Arrays.asList("1\t2 3\r4", "5\t 6 \t7\t\t8")), equalTo(8.));
    }

    @Test
    public void shouldNotTrimInput() {
        TabSeparatedResultParser parser = new TabSeparatedResultParser(1, 3);
        assertThat(parser.getMetricValue(Arrays.asList("1 2 3 4 ", " 5 6 7 8")), equalTo(7.));
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowAnExceptionOnNullInput() {
        TabSeparatedResultParser parser = new TabSeparatedResultParser(0, 0);
        parser.getMetricValue(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowAnExceptionOnEmptyInput() {
        TabSeparatedResultParser parser = new TabSeparatedResultParser(0, 0);
        parser.getMetricValue(Collections.emptyList());
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowAnExceptionOnTooLessNumberOfRows() {
        TabSeparatedResultParser parser = new TabSeparatedResultParser(1, 0);
        parser.getMetricValue(Collections.singletonList("1 2 3 4"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowAnExceptionOnTooLessNumberOfColumns() {
        TabSeparatedResultParser parser = new TabSeparatedResultParser(0, 4);
        parser.getMetricValue(Collections.singletonList("1 2 3 4"));
    }
}