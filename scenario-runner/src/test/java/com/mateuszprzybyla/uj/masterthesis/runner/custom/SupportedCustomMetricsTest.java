package com.mateuszprzybyla.uj.masterthesis.runner.custom;

import com.mateuszprzybyla.uj.masterthesis.metrics.model.Metric;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class SupportedCustomMetricsTest {
    @Mock
    private CustomMetric<String> customMetric;

    @Test
    public void shouldReturnCustomMetricConfigForExistingMetric() {
        SupportedCustomMetrics supportedCustomMetrics = new SupportedCustomMetrics(
                Collections.singletonMap(Metric.CPU_CYCLES, customMetric));
        assertThat(supportedCustomMetrics.getCustomMetricConfig(Metric.CPU_CYCLES), equalTo(customMetric));
    }

    @Test
    public void shouldReturnNullForNonExistingMetric() {
        SupportedCustomMetrics emptySupportedCustomMetrics = new SupportedCustomMetrics(Collections.emptyMap());
        assertThat(emptySupportedCustomMetrics.getCustomMetricConfig(Metric.CPU_CYCLES), nullValue());
    }
}