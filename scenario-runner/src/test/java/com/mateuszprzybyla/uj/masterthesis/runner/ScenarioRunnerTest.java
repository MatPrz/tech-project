package com.mateuszprzybyla.uj.masterthesis.runner;

import com.mateuszprzybyla.uj.masterthesis.metrics.model.Metric;
import com.mateuszprzybyla.uj.masterthesis.metrics.model.input.MetricData;
import com.mateuszprzybyla.uj.masterthesis.metrics.model.runner.ScenarioConfig;
import com.mateuszprzybyla.uj.masterthesis.metrics.model.runner.ScenarioResult;
import com.mateuszprzybyla.uj.masterthesis.runner.command.CommandExecutor;
import com.mateuszprzybyla.uj.masterthesis.runner.command.ProcessResult;
import com.mateuszprzybyla.uj.masterthesis.runner.command.RunningProcess;
import com.mateuszprzybyla.uj.masterthesis.runner.custom.CustomMetricsCollectorScheduler;
import com.mateuszprzybyla.uj.masterthesis.runner.perfstat.PerfStatCollector;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CountDownLatch;
import java.util.stream.Collectors;

import static com.mateuszprzybyla.uj.masterthesis.runner.MockData.*;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ScenarioRunnerTest {
    @Mock
    private CustomMetricsCollectorScheduler customMetricsCollectorScheduler;
    @Mock
    private CommandExecutor commandExecutor;
    @Mock
    private PerfStatCollector perfStatCollector;
    @Mock
    private ScenarioSummaryService scenarioSummaryService;
    private ScenarioRunner scenarioRunner;
    @Mock
    private RunningProcess monitoredAppProcess;

    @Before
    public void setUpScenarioRunner() {
        this.scenarioRunner = new ScenarioRunner(perfStatCollector, scenarioSummaryService,
                customMetricsCollectorScheduler, commandExecutor, DEFAULT_TIMEOUT);
    }

    @Before
    public void setUpHappyPath() throws Exception {
        when(commandExecutor.runInTheBackground(eq(COMMAND), anyString())).thenReturn(monitoredAppProcess);
        CompletableFuture<ProcessResult> mockProcessResult = mockProcessResult(0, Collections.singletonList("app result"));
        when(monitoredAppProcess.getProcessResult()).thenReturn(mockProcessResult);
        when(monitoredAppProcess.getTotalTimeMs()).thenReturn(CompletableFuture.completedFuture(MONITORED_APP_TOTAL_TIME));
        mockCustomCollectorSchedulerWithDelay(0);
        mockPerfStatWithDelay(0);
        when(scenarioSummaryService.aggregateSum(any())).thenReturn(new HashMap<>());
    }

    @Test
    public void shouldMergeCustomMetricsWithPerfStatMetrics() {
        ScenarioConfig scenarioConfig = mockScenarioConfigForDefaultCommand();

        Optional<ScenarioResult> scenarioResult = scenarioRunner.run(scenarioConfig);

        assertTrue(scenarioResult.isPresent());
        List<MetricData> metricDataList = scenarioResult.get().getMetricDatas();
        assertThat(metricDataList, hasSize(4));
        assertThat(metricDataList.stream().map(MetricData::getKey).collect(Collectors.toSet()),
                containsInAnyOrder(Metric.VIRTUAL_MEMORY_SIZE, Metric.MEMORY_USAGE, Metric.CPU_CYCLES, Metric.INSTRUCTIONS));
    }

    @Test
    public void shouldAttachAggregatedMetricsWithTotalTime() {
        ScenarioConfig scenarioConfig = mockScenarioConfigForDefaultCommand();

        Optional<ScenarioResult> scenarioResult = scenarioRunner.run(scenarioConfig);

        assertTrue(scenarioResult.isPresent());
        Map<Metric, Double> summary = scenarioResult.get().getSummary();
        assertThat(summary.size(), equalTo(1));
        assertThat(summary.get(Metric.TOTAL_RUN_TIME), equalTo(Double.valueOf(Long.toString(MONITORED_APP_TOTAL_TIME))));
    }

    @Test
    public void shouldUseProvidedTimeoutIfPresent() {
        mockCustomCollectorSchedulerWithDelay(2000);
        ScenarioConfig scenarioConfig = mockScenarioConfigForTimeoutMs(3000);

        Optional<ScenarioResult> scenarioResult = scenarioRunner.run(scenarioConfig);

        assertTrue(scenarioResult.isPresent());
    }

    @Test
    public void shouldReturnNoResultsIfRunningCommandFailed() throws Exception {
        ScenarioConfig scenarioConfig = mockScenarioConfigForDefaultCommand();
        when(commandExecutor.runInTheBackground(eq(COMMAND), anyString())).thenThrow(new IOException());

        Optional<ScenarioResult> scenarioResult = scenarioRunner.run(scenarioConfig);

        assertFalse(scenarioResult.isPresent());
    }

    @Test
    public void shouldReturnNoResultsIfTestedAppReturnedNoResult() throws Exception {
        ScenarioConfig scenarioConfig = mockScenarioConfigForDefaultCommand();
        when(monitoredAppProcess.getProcessResult()).thenReturn(CompletableFuture.completedFuture(null));

        Optional<ScenarioResult> scenarioResult = scenarioRunner.run(scenarioConfig);

        assertFalse(scenarioResult.isPresent());
    }

    @Test
    public void shouldReturnNoResultsIfTestedAppReturnedNonZeroExitCode() {
        ScenarioConfig scenarioConfig = mockScenarioConfigForDefaultCommand();
        CompletableFuture<ProcessResult> mockProcessResult = mockProcessResult(1, Collections.singletonList("result"));
        when(monitoredAppProcess.getProcessResult()).thenReturn(mockProcessResult);

        Optional<ScenarioResult> scenarioResult = scenarioRunner.run(scenarioConfig);

        assertFalse(scenarioResult.isPresent());
    }


    @Test
    public void shouldReturnEmptyResultIfDefaultTimeoutActivated() {
        mockCustomCollectorSchedulerWithDelay(2000);
        ScenarioConfig scenarioConfig = mockScenarioConfigForDefaultCommand();

        Optional<ScenarioResult> scenarioResult = scenarioRunner.run(scenarioConfig);

        assertFalse(scenarioResult.isPresent());
    }

    @Test
    public void shouldReturnEmptyResultIfProvidedTimeoutActivated() {
        mockCustomCollectorSchedulerWithDelay(2500);
        ScenarioConfig scenarioConfig = mockScenarioConfigForTimeoutMs(2000);

        Optional<ScenarioResult> scenarioResult = scenarioRunner.run(scenarioConfig);

        assertFalse(scenarioResult.isPresent());
    }

    @Test
    public void shouldReturnEmptyResultIfTimeoutCausedByPerfStatCommand() {
        mockPerfStatWithDelay(2000);
        ScenarioConfig scenarioConfig = mockScenarioConfigForDefaultCommand();

        Optional<ScenarioResult> scenarioResult = scenarioRunner.run(scenarioConfig);

        assertFalse(scenarioResult.isPresent());
    }

    private void mockCustomCollectorSchedulerWithDelay(int delayMs) {
        when(customMetricsCollectorScheduler.startCollecting(anyLong(), eq(monitoredAppProcess), any(), any(CountDownLatch.class)))
                .thenAnswer(invocation -> {
                    CountDownLatch latch = (CountDownLatch) invocation.getArguments()[3];
                    CompletableFuture<List<MetricData>> futureMetricData = mockFutureMetricData(delayMs,
                            Metric.VIRTUAL_MEMORY_SIZE, 123, 1.6, Metric.MEMORY_USAGE, 123, 25.6);
                    futureMetricData.thenRun(latch::countDown);
                    return futureMetricData;
                });
    }

    private void mockPerfStatWithDelay(int delayMs) {
        when(perfStatCollector.collectPerfStatMetrics(any(), eq(monitoredAppProcess), any(CountDownLatch.class)))
                .thenAnswer(invocation -> {
                    CountDownLatch latch = (CountDownLatch) invocation.getArguments()[2];
                    CompletableFuture<List<MetricData>> futureMetricData = mockFutureMetricData(delayMs,
                            Metric.CPU_CYCLES, 123, 1600., Metric.INSTRUCTIONS, 123, 256.);
                    futureMetricData.thenRun(latch::countDown);
                    return futureMetricData;
                });
    }
}