package com.mateuszprzybyla.uj.masterthesis.runner;

import com.mateuszprzybyla.uj.masterthesis.metrics.model.Metric;
import com.mateuszprzybyla.uj.masterthesis.metrics.model.input.MetricData;
import com.mateuszprzybyla.uj.masterthesis.metrics.model.input.MetricSnapshot;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

public class ScenarioSummaryServiceTest {
    private ScenarioSummaryService scenarioSummaryService = new ScenarioSummaryService();

    @Test
    public void shouldReturnEmptyMapForEmptyInputList() {
        assertThat(scenarioSummaryService.aggregateSum(Collections.emptyList()), equalTo(Collections.emptyMap()));
    }

    @Test
    public void shouldReturnZeroSumForMetricWithNoValues() {
        List<MetricData> metricDataList = Collections.singletonList(new MetricData(Metric.BRANCH_MISSES));

        Map<Metric, Double> aggregatedResult = scenarioSummaryService.aggregateSum(metricDataList);

        assertThat(aggregatedResult.size(), equalTo(1));
        assertThat(aggregatedResult.get(Metric.BRANCH_MISSES), equalTo(0.));
    }

    @Test
    public void shouldFilterNonAggregableMetrics() {
        List<MetricData> metricDataList = Arrays.asList(new MetricData(Metric.MEMORY_USAGE),
                new MetricData(Metric.BRANCH_MISSES), new MetricData(Metric.TOTAL_RUN_TIME), new MetricData(Metric.BRANCHES));

        Map<Metric, Double> aggregatedResult = scenarioSummaryService.aggregateSum(metricDataList);

        assertThat(aggregatedResult.size(), equalTo(2));
    }

    @Test
    public void shouldSumAllValuesForGivenMetric() {
        MetricData metricData = new MetricData(Metric.BRANCH_MISSES);
        metricData.setData(Arrays.asList(new MetricSnapshot(0, 123.), new MetricSnapshot(1, 145.)));
        List<MetricData> metricDataList = Collections.singletonList(metricData);

        Map<Metric, Double> aggregatedResult = scenarioSummaryService.aggregateSum(metricDataList);

        assertThat(aggregatedResult.get(Metric.BRANCH_MISSES), equalTo(268.));
    }
}