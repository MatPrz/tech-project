package com.mateuszprzybyla.uj.masterthesis.runner.command;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;

public class TemporaryFileReaderSupplierTest {
    private static final Path FILE_PATH = Paths.get("/tmp", "fileName.txt");
    private static final String NOT_EXISTING_FILE_NAME = "notExisting";
    private static final List<String> CONTENT = Arrays.asList("ala", "ma", "kota");
    private static final String CONTENT_STR = CONTENT.stream().collect(Collectors.joining(System.lineSeparator()));

    @Before
    public void setUp() throws Exception {
        File file = Files.createFile(FILE_PATH).toFile();
        FileUtils.writeStringToFile(file, CONTENT_STR, StandardCharsets.UTF_8);
    }

    @After
    public void tearDown() throws Exception {
        Files.deleteIfExists(FILE_PATH);
    }

    @Test
    public void shouldReturnFileReaderIfFileExists() throws Exception {
        TemporaryFileReaderSupplier fileReaderSupplier = new TemporaryFileReaderSupplier(FILE_PATH.toString());

        Reader reader = fileReaderSupplier.get();

        assertThat(IOUtils.readLines(reader), equalTo(CONTENT));
    }

    @Test
    public void shouldReturnEmptyStringReaderIfFileDoesNotExist() throws Exception {
        TemporaryFileReaderSupplier fileReaderSupplier = new TemporaryFileReaderSupplier(NOT_EXISTING_FILE_NAME);

        Reader reader = fileReaderSupplier.get();

        assertThat(IOUtils.readLines(reader), empty());
    }

    @Test
    public void shouldRemoveTheFileIfExistsWhenClosing() {
        TemporaryFileReaderSupplier fileReaderSupplier = new TemporaryFileReaderSupplier(FILE_PATH.toString());

        fileReaderSupplier.close();

        assertFalse(Files.exists(FILE_PATH));
    }

    @Test
    public void shouldNotFailOnRemovingTheFileThatDoesNotExist() {
        TemporaryFileReaderSupplier fileReaderSupplier = new TemporaryFileReaderSupplier(NOT_EXISTING_FILE_NAME);

        fileReaderSupplier.close();

        assertFalse(Files.exists(Paths.get(NOT_EXISTING_FILE_NAME)));
    }
}