package com.mateuszprzybyla.uj.masterthesis.runner.custom;

import com.mateuszprzybyla.uj.masterthesis.metrics.model.Metric;
import com.mateuszprzybyla.uj.masterthesis.runner.MockData;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Map;

import static com.mateuszprzybyla.uj.masterthesis.runner.MockData.DEFAULT_VALUE;
import static com.mateuszprzybyla.uj.masterthesis.runner.MockData.MONITORED_APP_PID;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CustomMetricsCollectorServiceTest {
    private static final long START_TIME = System.currentTimeMillis();
    private static final String USAGE_METRICS_SOURCE = "Usage Metrics Source";
    private static final String MEMORY_METRICS_SOURCE = "Memory Metrics Source";
    private static final double MEMORY_USAGE_VALUE = 23.45;
    private static final double CPU_USAGE_VALUE = 13.45;
    private static final double VSS_VALUE = 12345.;

    @InjectMocks
    private CustomMetricsCollectorService collectorService;
    @Mock
    private SupportedCustomMetrics supportedCustomMetrics;
    @Mock
    private MetricsSourceProvider<String> usageProvider;
    @Mock
    private MetricsSourceProvider<String> memoryDataProvider;
    @Mock
    private MetricsParser<String> memoryUsageParser;
    @Mock
    private MetricsParser<String> cpuUsageParser;
    @Mock
    private MetricsParser<String> vssParser;

    @Before
    @SuppressWarnings("unchecked")
    public void setUp() throws Exception {
        when(supportedCustomMetrics.getCustomMetricConfig(Metric.MEMORY_USAGE)).thenReturn(
                new CustomMetric(Metric.MEMORY_USAGE, usageProvider, memoryUsageParser));
        when(supportedCustomMetrics.getCustomMetricConfig(Metric.CPU_USAGE)).thenReturn(
                new CustomMetric(Metric.CPU_USAGE, usageProvider, cpuUsageParser));
        when(supportedCustomMetrics.getCustomMetricConfig(Metric.VIRTUAL_MEMORY_SIZE)).thenReturn(
                new CustomMetric(Metric.VIRTUAL_MEMORY_SIZE, memoryDataProvider, vssParser));
        when(usageProvider.getMetricsSourceForProcess(MONITORED_APP_PID)).thenReturn(USAGE_METRICS_SOURCE);
        when(memoryDataProvider.getMetricsSourceForProcess(MONITORED_APP_PID)).thenReturn(MEMORY_METRICS_SOURCE);
        when(memoryUsageParser.getMetricValue(USAGE_METRICS_SOURCE)).thenReturn(MEMORY_USAGE_VALUE);
        when(cpuUsageParser.getMetricValue(USAGE_METRICS_SOURCE)).thenReturn(CPU_USAGE_VALUE);
        when(vssParser.getMetricValue(MEMORY_METRICS_SOURCE)).thenReturn(VSS_VALUE);
    }


    @Test
    public void shouldCollectOnlyCustomMetrics() {
        ScenarioMetricsSnapshot metricsSnapshot = collectorService.collectMetrics(START_TIME, MONITORED_APP_PID,
                Arrays.asList(Metric.BRANCHES, Metric.MEMORY_USAGE, Metric.CPU_USAGE, Metric.VIRTUAL_MEMORY_SIZE));

        Map<Metric, Double> values = metricsSnapshot.getMetricsValues();
        assertThat(values.size(), equalTo(3));
        assertThat(values.get(Metric.MEMORY_USAGE), equalTo(MEMORY_USAGE_VALUE));
        assertThat(values.get(Metric.CPU_USAGE), equalTo(CPU_USAGE_VALUE));
        assertThat(values.get(Metric.VIRTUAL_MEMORY_SIZE), equalTo(VSS_VALUE));
    }

    @Test
    public void shouldRunEachMetricProviderOnlyOnce() throws Exception {
        collectorService.collectMetrics(START_TIME, MONITORED_APP_PID,
                Arrays.asList(Metric.MEMORY_USAGE, Metric.CPU_USAGE, Metric.VIRTUAL_MEMORY_SIZE));

        verify(usageProvider, times(1)).getMetricsSourceForProcess(MONITORED_APP_PID);
        verify(memoryDataProvider, times(1)).getMetricsSourceForProcess(MONITORED_APP_PID);
    }

    @Test
    public void shouldAddZeroMetricsFromProviderThatThrewAnException() throws Exception {
        when(usageProvider.getMetricsSourceForProcess(MONITORED_APP_PID)).thenThrow(new NullPointerException());

        ScenarioMetricsSnapshot metricsSnapshot = collectorService.collectMetrics(START_TIME, MONITORED_APP_PID,
                Arrays.asList(Metric.MEMORY_USAGE, Metric.CPU_USAGE, Metric.VIRTUAL_MEMORY_SIZE));

        Map<Metric, Double> values = metricsSnapshot.getMetricsValues();
        assertThat(values.get(Metric.MEMORY_USAGE), equalTo(DEFAULT_VALUE));
        assertThat(values.get(Metric.CPU_USAGE), equalTo(DEFAULT_VALUE));
        assertThat(values.get(Metric.VIRTUAL_MEMORY_SIZE), equalTo(VSS_VALUE));
    }

    @Test
    public void shouldAddZeroMetricsFromProviderThatReturnedNullMetricsSource() throws Exception {
        when(usageProvider.getMetricsSourceForProcess(MONITORED_APP_PID)).thenReturn(null);

        ScenarioMetricsSnapshot metricsSnapshot = collectorService.collectMetrics(START_TIME, MONITORED_APP_PID,
                Arrays.asList(Metric.MEMORY_USAGE, Metric.CPU_USAGE, Metric.VIRTUAL_MEMORY_SIZE));

        Map<Metric, Double> values = metricsSnapshot.getMetricsValues();
        assertThat(values.get(Metric.MEMORY_USAGE), equalTo(DEFAULT_VALUE));
        assertThat(values.get(Metric.CPU_USAGE), equalTo(DEFAULT_VALUE));
        assertThat(values.get(Metric.VIRTUAL_MEMORY_SIZE), equalTo(VSS_VALUE));
    }

    @Test
    public void shouldAddZeroMetricsIfParsingThrewAnException() {
        when(vssParser.getMetricValue(MEMORY_METRICS_SOURCE)).thenThrow(new NullPointerException());

        ScenarioMetricsSnapshot metricsSnapshot = collectorService.collectMetrics(START_TIME, MONITORED_APP_PID,
                Arrays.asList(Metric.MEMORY_USAGE, Metric.CPU_USAGE, Metric.VIRTUAL_MEMORY_SIZE));

        Map<Metric, Double> values = metricsSnapshot.getMetricsValues();
        assertThat(values.get(Metric.MEMORY_USAGE), equalTo(MEMORY_USAGE_VALUE));
        assertThat(values.get(Metric.CPU_USAGE), equalTo(CPU_USAGE_VALUE));
        assertThat(values.get(Metric.VIRTUAL_MEMORY_SIZE), equalTo(DEFAULT_VALUE));
    }
}