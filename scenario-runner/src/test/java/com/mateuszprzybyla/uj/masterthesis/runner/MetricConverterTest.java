package com.mateuszprzybyla.uj.masterthesis.runner;

import com.mateuszprzybyla.uj.masterthesis.metrics.model.Metric;
import com.mateuszprzybyla.uj.masterthesis.metrics.model.input.MetricData;
import com.mateuszprzybyla.uj.masterthesis.metrics.model.input.MetricSnapshot;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import static com.mateuszprzybyla.uj.masterthesis.runner.MockData.ConverterSource;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;

public class MetricConverterTest {
    private MetricConverter<ConverterSource> converter = new MetricConverter<>(ConverterSource::getTime, ConverterSource::getValuesByMetricKey);

    @Test
    public void shouldReturnEmptyResultForEmptyInput() {
        List<MetricData> metrics = converter.createMetrics(Collections.emptyList());

        assertThat(metrics, hasSize(0));
    }

    @Test
    public void shouldReturnConvertedAndSortedData() {
        List<MetricData> metrics = converter.createMetrics(Arrays.asList(new ConverterSource(15, Metric.BRANCHES, 123.),
                new ConverterSource(1, Metric.CPU_CYCLES, 162.), new ConverterSource(14, Metric.CPU_MIGRATIONS, 132.),
                new ConverterSource(5, Metric.MEMORY_USAGE, 192.)));

        assertThat(metrics, hasSize(4));
        Map<Metric, MetricData> grouped = metrics.stream().collect(Collectors.toMap(MetricData::getKey, Function.identity()));
        verifyMetricDataItem(grouped.get(Metric.CPU_CYCLES), Metric.CPU_CYCLES, 1, 162.);
        verifyMetricDataItem(grouped.get(Metric.MEMORY_USAGE), Metric.MEMORY_USAGE, 5, 192.);
        verifyMetricDataItem(grouped.get(Metric.CPU_MIGRATIONS), Metric.CPU_MIGRATIONS, 14, 132.);
        verifyMetricDataItem(grouped.get(Metric.BRANCHES), Metric.BRANCHES, 15, 123.);

    }

    @Test
    public void shouldReturnManyDataForSameMetricIfPresent() {
        List<MetricData> metrics = converter.createMetrics(Arrays.asList(new ConverterSource(15, Metric.BRANCHES, 123.),
                new ConverterSource(1, Metric.BRANCHES, 162.), new ConverterSource(14, Metric.BRANCHES, 132.),
                new ConverterSource(5, Metric.MEMORY_USAGE, 192.)));

        assertThat(metrics, hasSize(2));
        Map<Metric, MetricData> grouped = metrics.stream().collect(Collectors.toMap(MetricData::getKey, Function.identity()));
        assertThat(grouped.get(Metric.BRANCHES).getData(), hasSize(3));
        List<Double> branchesValues = grouped.get(Metric.BRANCHES).getData().stream().map(MetricSnapshot::getValue).collect(Collectors.toList());
        assertThat(branchesValues, equalTo(Arrays.asList(162., 132., 123.)));
        assertThat(grouped.get(Metric.MEMORY_USAGE).getData(), hasSize(1));
    }

    private void verifyMetricDataItem(MetricData metricData, Metric metric, Object time, double value) {
        assertThat(metricData.getKey(), equalTo(metric));
        assertThat(metricData.getData(), hasSize(1));
        assertThat(metricData.getData().get(0).getTime(), equalTo(time));
        assertThat(metricData.getData().get(0).getValue(), equalTo(value));
    }
}