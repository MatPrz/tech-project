package com.mateuszprzybyla.uj.masterthesis.runner.custom;

import com.mateuszprzybyla.uj.masterthesis.runner.command.CommandExecutor;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static com.mateuszprzybyla.uj.masterthesis.runner.MockData.COMMAND;
import static com.mateuszprzybyla.uj.masterthesis.runner.MockData.MONITORED_APP_PID;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CommandLineMetricsSourceProviderTest {
    @Mock
    private CommandExecutor commandExecutor;
    @Mock
    private CustomMetricCommand customMetricCommand;
    @InjectMocks
    private CommandLineMetricsSourceProvider provider;

    @Before
    public void setUp() throws Exception {
        when(customMetricCommand.getForProcessId(MONITORED_APP_PID)).thenReturn(COMMAND);
        when(commandExecutor.executeAndGetOutput(COMMAND)).thenReturn(Arrays.asList("ala", "  ma", "\tkota\t \n"));
    }

    @Test
    public void shouldReturnTrimmedCommandOutput() throws Exception {
        List<String> metricsSourceForProcess = provider.getMetricsSourceForProcess(MONITORED_APP_PID);

        assertThat(metricsSourceForProcess, hasSize(3));
        assertThat(metricsSourceForProcess.stream().collect(Collectors.joining(",")), equalTo("ala,ma,kota"));
    }

    @Test(expected = IOException.class)
    public void shouldPassExceptionIfThrownDuringCommandExecution() throws Exception {
        when(commandExecutor.executeAndGetOutput(COMMAND)).thenThrow(new IOException());

        provider.getMetricsSourceForProcess(MONITORED_APP_PID);
    }
}