package com.mateuszprzybyla.uj.masterthesis.runner.perfstat;

import com.mateuszprzybyla.uj.masterthesis.metrics.model.Metric;
import com.mateuszprzybyla.uj.masterthesis.metrics.model.input.MetricData;
import com.mateuszprzybyla.uj.masterthesis.runner.MetricConverter;
import com.mateuszprzybyla.uj.masterthesis.runner.command.CommandExecutor;
import com.mateuszprzybyla.uj.masterthesis.runner.command.ProcessResult;
import com.mateuszprzybyla.uj.masterthesis.runner.command.RunningProcess;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;
import org.springframework.test.util.ReflectionTestUtils;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import static com.mateuszprzybyla.uj.masterthesis.runner.MockData.*;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class PerfStatCollectorTest {
    private static final List<String> PERF_PROCESS_OUTPUT = Arrays.asList("1.2:2.3::branches::", "1.2:6.3::cycles::");
    private static final List<MetricData> EXPECTED_METRIC_DATA = Arrays.asList(
            mockMetricData(Metric.BRANCHES, 1200, 2.3), mockMetricData(Metric.CPU_CYCLES, 1200, 6.3));
    @Mock
    private CommandExecutor commandExecutor;
    @Mock
    private PerfStatMetrics perfStatMetrics;
    @Mock
    private MetricConverter<String> metricConverter;
    @Mock
    private RunningProcess perfStatProcess;
    @Mock
    private RunningProcess mainProcess;
    @InjectMocks
    private PerfStatCollector perfStatCollector;

    @Before
    public void setSeparator() {
        ReflectionTestUtils.setField(perfStatCollector, "perfStatSeparator", ":");
    }

    @Before
    public void setUpMetrics() {
        when(perfStatMetrics.getPerfStatKey(Metric.BRANCHES)).thenReturn("branches");
        when(perfStatMetrics.getPerfStatKey(Metric.CPU_CYCLES)).thenReturn("cycles");
    }

    @Before
    public void setUpHappyPath() throws Exception {
        when(commandExecutor.executeAndGetOutput("perf -v")).thenReturn(Collections.singletonList("perf version 4.4.59"));
        CompletableFuture<ProcessResult> mockProcessResult = mockProcessResult(0, PERF_PROCESS_OUTPUT);
        when(perfStatProcess.getProcessResult()).thenReturn(mockProcessResult);
        when(mainProcess.getProcessId()).thenReturn(MONITORED_APP_PID);
        when(commandExecutor.runInTheBackground(anyString(), anyString())).thenAnswer(new Answer<RunningProcess>() {
            @Override
            public RunningProcess answer(InvocationOnMock invocation) throws Throwable {
                String command = (String) invocation.getArguments()[0];
                if (command.equals(VALID_PERF_STAT_COMMAND)) {
                    return perfStatProcess;
                }
                return null;
            }
        });
        when(metricConverter.createMetrics(eq(PERF_PROCESS_OUTPUT))).thenReturn(EXPECTED_METRIC_DATA);
    }

    @Test
    public void shouldReturnFutureMetrics() throws Exception {
        CountDownLatch latch = new CountDownLatch(1);

        CompletableFuture<List<MetricData>> completableFuture = perfStatCollector.collectPerfStatMetrics(
                mockScenarioConfigForMetrics(Metric.BRANCHES, Metric.CPU_CYCLES), mainProcess, latch);

        assertThat(latch.getCount(), equalTo(0L));
        assertThat(completableFuture.get(10, TimeUnit.SECONDS), equalTo(EXPECTED_METRIC_DATA));
    }

    @Test
    public void shouldProvidedOnlyPerfEventsToPerfStatCommand() throws Exception {
        CountDownLatch latch = new CountDownLatch(1);

        perfStatCollector.collectPerfStatMetrics(mockScenarioConfigForMetrics(
                Metric.BRANCHES, Metric.L1_DCACHE_LOADS, Metric.CPU_CYCLES, Metric.CONTEXT_SWITCHES, Metric.INSTRUCTIONS),
                mainProcess, latch);

        assertThat(latch.getCount(), equalTo(0L));
        verify(commandExecutor).runInTheBackground(eq(VALID_PERF_STAT_COMMAND), anyString());
    }

    @Test
    public void shouldStopPerfProcessWhenMainProcessStops() {
        CountDownLatch latch = new CountDownLatch(1);
        when(mainProcess.getTotalTimeMs()).thenReturn(CompletableFuture.completedFuture(123L));
        doCallRealMethod().when(mainProcess).whenDone(any(Runnable.class));

        perfStatCollector.collectPerfStatMetrics(mockScenarioConfigForMetrics(Metric.BRANCHES, Metric.CPU_CYCLES), mainProcess, latch);

        assertThat(latch.getCount(), equalTo(0L));
        verify(perfStatProcess).stop();
    }


    @Test
    public void shouldReturnEmptyFutureIfPerfCommandDoesNotExist() throws Exception {
        CountDownLatch latch = new CountDownLatch(1);
        when(commandExecutor.executeAndGetOutput("perf -v")).thenReturn(Collections.singletonList("perf: command not found"));

        CompletableFuture<List<MetricData>> completableFuture = perfStatCollector.collectPerfStatMetrics(
                mockScenarioConfigForMetrics(Metric.BRANCHES, Metric.CPU_CYCLES), mainProcess, latch);

        assertThat(latch.getCount(), equalTo(0L));
        assertThat(completableFuture.getNow(Collections.emptyList()), hasSize(0));
        verify(commandExecutor, never()).runInTheBackground(anyString(), anyString());
    }

    @Test
    public void shouldReturnEmptyFutureIfCheckingForExistenceOfPerfCommandFailed() throws Exception {
        CountDownLatch latch = new CountDownLatch(1);
        when(commandExecutor.executeAndGetOutput("perf -v")).thenThrow(new IOException());

        CompletableFuture<List<MetricData>> completableFuture = perfStatCollector.collectPerfStatMetrics(
                mockScenarioConfigForMetrics(Metric.BRANCHES, Metric.CPU_CYCLES), mainProcess, latch);

        assertThat(latch.getCount(), equalTo(0L));
        assertThat(completableFuture.getNow(Collections.emptyList()), hasSize(0));
        verify(commandExecutor, never()).runInTheBackground(anyString(), anyString());
    }

    @Test
    public void shouldNotRunPerfStatCommandIfNoPerfEventsProvided() throws Exception {
        CountDownLatch latch = new CountDownLatch(1);

        CompletableFuture<List<MetricData>> completableFuture = perfStatCollector.collectPerfStatMetrics(
                mockScenarioConfigForMetrics(Metric.INSTRUCTIONS, Metric.CONTEXT_SWITCHES), mainProcess, latch);

        assertThat(latch.getCount(), equalTo(0L));
        assertThat(completableFuture.getNow(Collections.emptyList()), hasSize(0));
        verify(commandExecutor, never()).runInTheBackground(anyString(), anyString());
    }

    @Test
    public void shouldReturnEmptyFutureIfRunningPerfStatProcessFailed() throws Exception {
        CountDownLatch latch = new CountDownLatch(1);
        reset(commandExecutor);
        when(commandExecutor.executeAndGetOutput("perf -v")).thenReturn(Collections.singletonList("perf version 4.4.59"));
        when(commandExecutor.runInTheBackground(eq(VALID_PERF_STAT_COMMAND), anyString())).thenThrow(new IOException());

        CompletableFuture<List<MetricData>> completableFuture = perfStatCollector.collectPerfStatMetrics(
                mockScenarioConfigForMetrics(Metric.BRANCHES, Metric.CPU_CYCLES), mainProcess, latch);

        assertThat(latch.getCount(), equalTo(0L));
        assertThat(completableFuture.getNow(Collections.emptyList()), hasSize(0));
    }

}