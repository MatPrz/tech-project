package com.mateuszprzybyla.uj.masterthesis.runner.custom;

import com.mateuszprzybyla.uj.masterthesis.metrics.model.Metric;
import com.mateuszprzybyla.uj.masterthesis.metrics.model.input.MetricData;
import com.mateuszprzybyla.uj.masterthesis.runner.MetricConverter;
import com.mateuszprzybyla.uj.masterthesis.runner.command.ProcessResult;
import com.mateuszprzybyla.uj.masterthesis.runner.command.RunningProcess;
import io.reactivex.schedulers.TestScheduler;
import org.apache.commons.lang3.RandomUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import static com.mateuszprzybyla.uj.masterthesis.runner.MockData.*;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class CustomMetricsCollectorSchedulerTest {
    private static final long START_TIME = System.currentTimeMillis();
    private static final List<Metric> METRICS = Arrays.asList(Metric.CPU_USAGE, Metric.CPU_MIGRATIONS, Metric.BRANCHES);
    @Mock
    private CustomMetricsCollectorService service;
    @Mock
    private MetricConverter<ScenarioMetricsSnapshot> converter;
    @Mock
    private RunningProcess appProcess;
    @Mock
    private ProcessResult processResult;
    @Captor
    private ArgumentCaptor<List<ScenarioMetricsSnapshot>> converterCaptor;
    private TestScheduler testScheduler = new TestScheduler();
    private CompletableFuture<ProcessResult> processResultFuture;
    private CompletableFuture<Long> totalTime;
    private CustomMetricsCollectorScheduler customScheduler;
    private int collectMetricsCounter;

    @Before
    public void setUp() {
        this.customScheduler = new CustomMetricsCollectorScheduler(testScheduler, service, converter);
        this.processResultFuture = new CompletableFuture<>();
        this.totalTime = new CompletableFuture<>();
        when(appProcess.getProcessId()).thenReturn(MONITORED_APP_PID);
        when(appProcess.getProcessResult()).thenReturn(this.processResultFuture);
        when(appProcess.getTotalTimeMs()).thenReturn(this.totalTime);
    }

    @Before
    public void setUpService() {
        this.collectMetricsCounter = 0;
        when(service.collectMetrics(START_TIME, MONITORED_APP_PID, METRICS)).thenAnswer(new Answer<ScenarioMetricsSnapshot>() {
            @Override
            public ScenarioMetricsSnapshot answer(InvocationOnMock invocation) throws Throwable {
                collectMetricsCounter++;
                Map<Metric, Double> randomValues = new HashMap<>();
                randomValues.put(Metric.CPU_USAGE, RandomUtils.nextDouble());
                randomValues.put(Metric.CPU_MIGRATIONS, RandomUtils.nextDouble());
                randomValues.put(Metric.BRANCHES, RandomUtils.nextDouble());
                return new ScenarioMetricsSnapshot(collectMetricsCounter, randomValues);
            }
        });
    }

    @Test
    public void shouldReturnFutureMetrics() throws Exception {
        CountDownLatch latch = new CountDownLatch(1);

        CompletableFuture<List<MetricData>> futureMetrics = customScheduler.startCollecting(START_TIME, appProcess,
                mockScenarioConfigForRefreshIntervalAndMetrics(REFRESH_INTERVAL_MS, METRICS), latch);
        testScheduler.advanceTimeTo(5 * REFRESH_INTERVAL_MS, TimeUnit.MILLISECONDS);

        assertFalse(futureMetrics.isDone());
        processResultFuture.complete(processResult);
        assertTrue(futureMetrics.isDone());
        assertThat(latch.getCount(), equalTo(0L));
        verify(converter).createMetrics(converterCaptor.capture());
        List<ScenarioMetricsSnapshot> metricDataList = converterCaptor.getValue();
        assertThat(metricDataList, hasSize(5));
    }

    @Test
    public void shouldReturnMetricsWhenMonitoredProcessStopsBeforeCollection() {
        CountDownLatch latch = new CountDownLatch(1);
        processResultFuture.complete(processResult);

        CompletableFuture<List<MetricData>> futureMetrics = customScheduler.startCollecting(START_TIME, appProcess,
                mockScenarioConfigForRefreshIntervalAndMetrics(REFRESH_INTERVAL_MS, METRICS), latch);
        testScheduler.advanceTimeTo(5 * REFRESH_INTERVAL_MS, TimeUnit.MILLISECONDS);

        assertTrue(futureMetrics.isDone());
        assertThat(latch.getCount(), equalTo(0L));
        verify(converter).createMetrics(converterCaptor.capture());
        List<ScenarioMetricsSnapshot> metricDataList = converterCaptor.getValue();
        assertThat(metricDataList, empty());
    }

    @Test
    public void shouldCollectCustomMetricsEveryInterval() {
        CountDownLatch latch = new CountDownLatch(1);

        customScheduler.startCollecting(START_TIME, appProcess,
                mockScenarioConfigForRefreshIntervalAndMetrics(REFRESH_INTERVAL_MS, METRICS), latch);
        testScheduler.advanceTimeTo(23 * REFRESH_INTERVAL_MS, TimeUnit.MILLISECONDS);

        verify(service, times(23)).collectMetrics(START_TIME, MONITORED_APP_PID, METRICS);
    }

    @Test
    public void shouldStopCollectingMetricsOnceMonitoredProcessStops() {
        CountDownLatch latch = new CountDownLatch(1);

        customScheduler.startCollecting(START_TIME, appProcess,
                mockScenarioConfigForRefreshIntervalAndMetrics(REFRESH_INTERVAL_MS, METRICS), latch);
        testScheduler.advanceTimeTo(2 * REFRESH_INTERVAL_MS, TimeUnit.MILLISECONDS);

        processResultFuture.complete(processResult);
        testScheduler.advanceTimeTo(2 * REFRESH_INTERVAL_MS, TimeUnit.MILLISECONDS);
        verify(converter).createMetrics(converterCaptor.capture());
        List<ScenarioMetricsSnapshot> metricDataList = converterCaptor.getValue();
        assertThat(metricDataList, hasSize(2));
        verify(service, times(2)).collectMetrics(START_TIME, MONITORED_APP_PID, METRICS);
    }
}