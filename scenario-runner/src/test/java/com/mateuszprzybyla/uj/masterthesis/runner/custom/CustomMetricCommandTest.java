package com.mateuszprzybyla.uj.masterthesis.runner.custom;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

public class CustomMetricCommandTest {
    @Test
    public void shouldEvaluateProvidedPid() {
        CustomMetricCommand customMetricCommand = new CustomMetricCommand("ps -p ${pid}");

        String commandForPid = customMetricCommand.getForProcessId("123");

        assertThat(commandForPid, equalTo("ps -p 123"));
    }

    @Test
    public void shouldNotEvaluateIfPidNotExistInCommand() {
        CustomMetricCommand customMetricCommand = new CustomMetricCommand("command without pid");

        String commandForPid = customMetricCommand.getForProcessId("123");

        assertThat(commandForPid, equalTo("command without pid"));
    }
}