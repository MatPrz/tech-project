package com.mateuszprzybyla.uj.masterthesis.runner.command;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.core.env.Environment;
import org.springframework.test.util.ReflectionTestUtils;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.Matchers.instanceOf;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ShellFactoryBeanTest {
    @InjectMocks
    private ShellFactoryBean factoryBean;
    @Mock
    private Environment environment;

    @Test
    public void shouldCreateLinuxShellForLinuxOperatingSystem() {
        when(environment.getProperty("os.name")).thenReturn("Linux 2.5.2");
        when(environment.getRequiredProperty("scenario.runner.bash.path")).thenReturn("/bin/bash");

        Shell shell = factoryBean.getObject();
        assertThat(shell, instanceOf(UnixBashShell.class));
        assertThat(ReflectionTestUtils.getField(shell, "bashPath"), equalTo("/bin/bash"));
        assertThat(factoryBean.getObjectType(), equalTo(UnixBashShell.class));
    }

    @Test(expected = IllegalStateException.class)
    public void shouldThrowAnExceptionWhenGettingClassForInvalidOperatingSystem() {
        factoryBean.getObjectType();
    }

    @Test(expected = IllegalStateException.class)
    public void shouldThrowAnExceptionWhenCreatingBeanForInvalidOperatingSystem() {
        factoryBean.getObject();
    }
}