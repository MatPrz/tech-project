package com.mateuszprzybyla.uj.masterthesis.runner.perfstat;

import com.mateuszprzybyla.uj.masterthesis.metrics.model.Metric;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;

import static com.mateuszprzybyla.uj.masterthesis.runner.MockData.PERF_SEPARATOR;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PerfStatOutputConverterTest {
    private static final String BRANCHES_METRIC = "branches";
    private static final String NON_PERF_STAT_METRIC = "dasdad";
    private static final String VALID_LINE_FORMAT = "\t%s" + PERF_SEPARATOR + "%s" + PERF_SEPARATOR + PERF_SEPARATOR
            + "%s" + PERF_SEPARATOR + PERF_SEPARATOR;
    private static final String INVALID_LINE = "invalid_line";
    private static final double DEFAULT_VALUE = 0.;
    @Mock
    private PerfStatMetrics perfstatMetrics;
    private PerfStatOutputConverter converter;

    @Before
    public void setUp() {
        this.converter = new PerfStatOutputConverter(PERF_SEPARATOR, perfstatMetrics);
        when(perfstatMetrics.getPerfStatMetricByKey(BRANCHES_METRIC)).thenReturn(Metric.BRANCHES);
    }

    @Test
    public void shouldRoundAndReturnTimeForValidLine() {
        assertThat(converter.getTime(getValidLine("1234.56678", "1", BRANCHES_METRIC)), equalTo(1234567));
        assertThat(converter.getTime(getValidLine("1234.56500", "1", BRANCHES_METRIC)), equalTo(1234565));
        assertThat(converter.getTime(getValidLine("1234.56499", "1", BRANCHES_METRIC)), equalTo(1234565));
        assertThat(converter.getTime(getValidLine("1234.56449", "1", BRANCHES_METRIC)), equalTo(1234564));
        assertThat(converter.getTime(getValidLine("1234.56450", "1", BRANCHES_METRIC)), equalTo(1234565));
        assertThat(converter.getTime(getValidLine("0", "1", BRANCHES_METRIC)), equalTo(0));
        assertThat(converter.getTime(getValidLine("1234", "1", BRANCHES_METRIC)), equalTo(1234000));
        assertThat(converter.getTime(getValidLine("1234.", "1", BRANCHES_METRIC)), equalTo(1234000));
    }

    @Test
    public void shouldReturnValidValueForValidLine() {
        validateCorrectValue("1", 1.);
        validateCorrectValue("0.1", 0.1);
        validateCorrectValue("1,5", 1.5);
        validateCorrectValue("1.", 1.);
        validateCorrectValue("0", 0.);
        validateCorrectValue("14.6213", 14.6213);
        validateCorrectValue(".6213", .6213);
    }

    @Test
    public void shouldReturnDefaultValueForValidLineAndInvalidValueFormat() {
        validateCorrectValue("asd", DEFAULT_VALUE);
        validateCorrectValue("", DEFAULT_VALUE);
        validateCorrectValue(".", DEFAULT_VALUE);
    }

    @Test
    public void shouldNotReturnAnyValuesForNonPerfStatMetric() {
        assertThat(converter.getKeyValueItem(getValidLine("1234.56678", "123", NON_PERF_STAT_METRIC)),
                equalTo(Collections.emptyMap()));
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowIllegalExceptionWhenGettingTimeFromInvalidLine() {
        converter.getTime(INVALID_LINE);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowIllegalExceptionWhenGettingValueFromInvalidLine() {
        converter.getKeyValueItem(INVALID_LINE);
    }

    private void validateCorrectValue(String value, Double expectedValue) {
        assertThat(converter.getKeyValueItem(getValidLine("1234.56678", value, BRANCHES_METRIC)),
                equalTo(Collections.singletonMap(Metric.BRANCHES, expectedValue)));
    }

    private String getValidLine(String timestamp, String value, String key) {
        return String.format(VALID_LINE_FORMAT, timestamp, value, key);
    }
}