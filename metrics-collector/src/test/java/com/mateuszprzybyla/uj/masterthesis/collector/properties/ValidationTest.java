package com.mateuszprzybyla.uj.masterthesis.collector.properties;

import com.mateuszprzybyla.uj.masterthesis.metrics.model.Metric;
import org.junit.Before;
import org.junit.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.hamcrest.Matchers.*;
import static org.hamcrest.Matchers.hasItems;
import static org.junit.Assert.assertThat;

public class ValidationTest {
    private CollectorProperties validProperties;
    private Validator validator;
    private Scenario scenario;

    @Before
    public void setUpValidator() {
        this.validator = Validation.buildDefaultValidatorFactory().getValidator();
    }

    @Before
    public void setUpValidProperties() {
        this.validProperties = new CollectorProperties();
        this.validProperties.setName("Name");
        this.validProperties.setMetricsViewerUrl("http://localhost:8080/");
        this.validProperties.setRunnerAgentUrl("http://localhost:8080/");
        List<Metric> metricList = Collections.singletonList(Metric.MEMORY_USAGE);
        this.validProperties.setMetricList(metricList);
        this.scenario = new Scenario();
        this.scenario.setName("Name");
        this.scenario.setCommand("command");
        this.scenario.setRefreshIntervalMs(100);
        this.scenario.setTags(Arrays.asList("tag1", "tag2"));
        this.scenario.setTimeout(100);
        this.scenario.setMetricList(metricList);
        this.validProperties.setScenariosConfig(Collections.singletonList(scenario));

    }

    @Test
    public void shouldValidateCorrectProperties() {
        Set<ConstraintViolation<CollectorProperties>> constraintViolations = validator.validate(this.validProperties);
        assertThat(constraintViolations, empty());
    }

    @Test
    public void shouldValidateCorrectPropertiesWithoutOptionalFields() {
        this.scenario.setTimeout(null);
        this.scenario.setTags(null);
        Set<ConstraintViolation<CollectorProperties>> constraintViolations = validator.validate(this.validProperties);
        assertThat(constraintViolations, empty());
    }

    @Test
    public void shouldValidateCorrectPropertiesWithGlobalMetricsAndNoScenarioMetrics() {
        this.scenario.setMetricList(null);
        Set<ConstraintViolation<CollectorProperties>> constraintViolations = validator.validate(this.validProperties);
        assertThat(constraintViolations, empty());
    }

    @Test
    public void shouldValidateCorrectPropertiesWithoutGlobalMetricsAndWithScenarioMetrics() {
        this.validProperties.setMetricList(null);
        Set<ConstraintViolation<CollectorProperties>> constraintViolations = validator.validate(this.validProperties);
        assertThat(constraintViolations, empty());
    }

    @Test
    public void shouldNotValidatePropertiesWithoutName() {
        this.validProperties.setName(null);
        assertSingleConstraintViolation("Execution name cannot be blank", "name");
        this.validProperties.setName("   ");
        assertSingleConstraintViolation("Execution name cannot be blank", "name");
    }

    @Test
    public void shouldNotValidatePropertiesWithInvalidMetricsViewerUrl() {
        this.validProperties.setMetricsViewerUrl(null);
        assertSingleConstraintViolation("metricsViewerUrl cannot be blank", "metricsViewerUrl");
        this.validProperties.setMetricsViewerUrl("invalid url");
        assertSingleConstraintViolation("metricsViewerUrl is not a valid URL", "metricsViewerUrl");
    }

    @Test
    public void shouldNotValidatePropertiesWithInvalidScenarioRunnerUrl() {
        this.validProperties.setRunnerAgentUrl(null);
        assertSingleConstraintViolation("runnerAgentUrl cannot be blank", "runnerAgentUrl");
        this.validProperties.setRunnerAgentUrl("invalid url");
        assertSingleConstraintViolation("runnerAgentUrl is not a valid URL", "runnerAgentUrl");
    }

    @Test
    public void shouldNotValidatePropertiesWithoutScenarios() {
        this.validProperties.setScenariosConfig(Collections.emptyList());
        assertSingleConstraintViolation("scenarios list cannot be empty", "scenariosConfig");
        this.validProperties.setScenariosConfig(null);
        assertSingleConstraintViolation("scenarios list cannot be empty", "scenariosConfig");
    }

    @Test
    public void shouldNotValidateScenarioWithoutName() {
        this.scenario.setName("");
        assertSingleConstraintViolation("Scenario name cannot be blank", "scenariosConfig[0].name");
        this.scenario.setName(null);
        assertSingleConstraintViolation("Scenario name cannot be blank", "scenariosConfig[0].name");
    }

    @Test
    public void shouldNotValidateScenarioWithoutCommand() {
        this.scenario.setCommand("");
        assertSingleConstraintViolation("Scenario command cannot be blank", "scenariosConfig[0].command");
        this.scenario.setCommand(null);
        assertSingleConstraintViolation("Scenario command cannot be blank", "scenariosConfig[0].command");
    }

    @Test
    public void shouldNotValidateScenarioWithInvalidRefreshInterval() {
        this.scenario.setRefreshIntervalMs(null);
        assertSingleConstraintViolation("refreshIntervalMs cannot be empty", "scenariosConfig[0].refreshIntervalMs");
        this.scenario.setRefreshIntervalMs(0);
        assertSingleConstraintViolation("refreshIntervalMs must be at least 100 ms", "scenariosConfig[0].refreshIntervalMs");
        this.scenario.setRefreshIntervalMs(99);
        assertSingleConstraintViolation("refreshIntervalMs must be at least 100 ms", "scenariosConfig[0].refreshIntervalMs");
    }


    @Test
    public void shouldNotValidateScenarioWithoutMetrics() {
        this.validProperties.setMetricList(null);
        this.scenario.setMetricList(null);
        Set<ConstraintViolation<CollectorProperties>> constraintViolations = validator.validate(this.validProperties);
        assertThat(constraintViolations, hasSize(2));
        Set<String> paths = constraintViolations.stream()
                .map(ConstraintViolation::getPropertyPath)
                .map(Object::toString)
                .collect(Collectors.toSet());
        assertThat(paths, hasItems("", "scenariosConfig[0]"));
        Set<String> messages = constraintViolations.stream()
                .map(ConstraintViolation::getMessage)
                .collect(Collectors.toSet());
        assertThat(messages, hasItems("Scenario does not have a list of metrics specified",
                "Scenario does not have a list of metrics specified"));
    }

    @Test
    public void shouldReturnMultipleConstraintViolations() {
        this.scenario.setName(null);
        this.validProperties.setName(null);
        this.scenario.setRefreshIntervalMs(5);
        Set<ConstraintViolation<CollectorProperties>> constraintViolations = validator.validate(this.validProperties);
        assertThat(constraintViolations, hasSize(3));
    }


    private void assertSingleConstraintViolation(String message, String path) {
        Set<ConstraintViolation<CollectorProperties>> constraintViolations = validator.validate(this.validProperties);
        assertThat(constraintViolations, hasSize(1));
        ConstraintViolation<CollectorProperties> constraintViolation = constraintViolations.iterator().next();
        assertThat(constraintViolation.getMessage(), equalTo(message));
        assertThat(constraintViolation.getPropertyPath().toString(), equalTo(path));
    }
}