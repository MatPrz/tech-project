package com.mateuszprzybyla.uj.masterthesis.collector;

import com.mateuszprzybyla.uj.masterthesis.collector.properties.CollectorProperties;
import com.mateuszprzybyla.uj.masterthesis.collector.properties.Scenario;
import com.mateuszprzybyla.uj.masterthesis.metrics.model.Metric;
import com.mateuszprzybyla.uj.masterthesis.metrics.model.input.Execution;
import com.mateuszprzybyla.uj.masterthesis.metrics.model.input.ExecutionScenario;
import com.mateuszprzybyla.uj.masterthesis.metrics.model.input.MetricData;
import com.mateuszprzybyla.uj.masterthesis.metrics.model.runner.ScenarioConfig;
import com.mateuszprzybyla.uj.masterthesis.metrics.model.runner.ScenarioResult;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.*;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class MetricsCollectorTest {
    private static final String EXECUTION_NAME = "ExecutionName";
    private static final String SCENARIO_RUNNER_URL = "localhost:8080";
    private static final String COMMAND = "command";
    private static final int REFRESH_INTERVAL_MS = 100;
    private static final int TIMEOUT = 100;
    private static final String SCENARIO_NAME = "Scenario name";
    private static final int EXECUTION_ID = 2;
    private static final String TAG_1 = "tag1";
    private static final String TAG_2 = "tag2";

    @Mock
    private ScenarioRunnerClient scenarioRunnerClient;
    @Mock
    private MetricsViewerClient metricsViewerClient;
    @Mock
    private ScenarioResult scenarioResult;
    @Mock
    private Map<Metric, Double> scenarioSummary;
    @Mock
    private List<MetricData> metricDatas;
    @InjectMocks
    private MetricsCollector metricsCollector;
    @Captor
    private ArgumentCaptor<ScenarioConfig> scenarioCaptor;
    @Captor
    private ArgumentCaptor<Execution> executionCaptor;
    private CollectorProperties collectorProperties;

    @Before
    public void prepareProperties() {
        this.collectorProperties = new CollectorProperties();
        collectorProperties.setName(EXECUTION_NAME);
        collectorProperties.setRunnerAgentUrl(SCENARIO_RUNNER_URL);
        when(scenarioResult.getSummary()).thenReturn(scenarioSummary);
        when(scenarioResult.getMetricDatas()).thenReturn(metricDatas);
        when(scenarioRunnerClient.runScenario(any(ScenarioConfig.class)))
                .thenReturn(Optional.ofNullable(scenarioResult));
        when(metricsViewerClient.pushExecution(any(Execution.class))).thenReturn(Optional.empty());
    }

    @Test
    public void shouldExecuteSingleScenarioAndRewriteFields() {
        Scenario scenario = prepareScenario(SCENARIO_NAME, Collections.singletonList(Metric.BRANCHES),
                REFRESH_INTERVAL_MS, Arrays.asList(TAG_1, TAG_2), COMMAND, TIMEOUT);
        collectorProperties.setScenariosConfig(Collections.singletonList(scenario));

        Optional<Integer> executionId = metricsCollector.run(collectorProperties);

        assertThat(executionId, equalTo(Optional.empty()));
        verify(scenarioRunnerClient).runScenario(scenarioCaptor.capture());
        ScenarioConfig scenarioConfig = scenarioCaptor.getValue();
        assertThat(scenarioConfig.getMetricList(), equalTo(Collections.singletonList(Metric.BRANCHES)));
        assertThat(scenarioConfig.getRefreshIntervalMs(), equalTo(REFRESH_INTERVAL_MS));
        assertThat(scenarioConfig.getStartCommand(), equalTo(COMMAND));
        assertThat(scenarioConfig.getTimeout(), equalTo(TIMEOUT));
        verify(metricsViewerClient).pushExecution(executionCaptor.capture());
        Execution execution = executionCaptor.getValue();
        assertThat(execution.getName(), equalTo(EXECUTION_NAME));
        assertThat(execution.getStartTimeUtc(), notNullValue());
        assertThat(execution.getHostname(), notNullValue());
        assertTrue(execution.getSummary().isEmpty());
        assertThat(execution.getScenarios(), hasSize(1));
        ExecutionScenario executionScenario = execution.getScenarios().get(0);
        assertThat(executionScenario.getName(), equalTo(SCENARIO_NAME));
        assertThat(executionScenario.getMetricDatas(), equalTo(metricDatas));
        assertThat(executionScenario.getSummary(), equalTo(scenarioSummary));
        assertThat(executionScenario.getHostname(), nullValue());
        assertThat(executionScenario.getTags(), contains(TAG_1, TAG_2));
        assertThat(executionScenario.getRefreshIntervalMs(), equalTo(REFRESH_INTERVAL_MS));
    }

    @Test
    public void shouldReturnExecutionId() {
        Scenario scenario = prepareScenario(SCENARIO_NAME, Collections.singletonList(Metric.BRANCHES),
                REFRESH_INTERVAL_MS, Collections.emptyList(), COMMAND, TIMEOUT);
        collectorProperties.setScenariosConfig(Collections.singletonList(scenario));
        when(metricsViewerClient.pushExecution(any(Execution.class))).thenReturn(Optional.of(EXECUTION_ID));

        assertThat(metricsCollector.run(collectorProperties), equalTo(Optional.of(EXECUTION_ID)));

    }

    @Test
    public void shouldUseGlobalMetricsListUnlessOverrided() {
        Scenario scenarioNullMetrics = prepareScenario(SCENARIO_NAME, null,
                REFRESH_INTERVAL_MS, Collections.emptyList(), COMMAND, TIMEOUT);
        Scenario scenarioEmptyMetrics = prepareScenario(SCENARIO_NAME, Collections.emptyList(),
                REFRESH_INTERVAL_MS, Collections.emptyList(), COMMAND, TIMEOUT);
        Scenario scenarioOverriding = prepareScenario(SCENARIO_NAME, Collections.singletonList(Metric.BRANCHES),
                REFRESH_INTERVAL_MS, Collections.emptyList(), COMMAND, TIMEOUT);
        collectorProperties.setMetricList(Collections.singletonList(Metric.CPU_CYCLES));
        collectorProperties.setScenariosConfig(Arrays.asList(scenarioNullMetrics, scenarioEmptyMetrics, scenarioOverriding));

        metricsCollector.run(collectorProperties);

        verify(scenarioRunnerClient, times(3)).runScenario(scenarioCaptor.capture());
        List<ScenarioConfig> scenarioConfigs = scenarioCaptor.getAllValues();
        assertThat(scenarioConfigs.get(0).getMetricList(), contains(Metric.CPU_CYCLES));
        assertThat(scenarioConfigs.get(1).getMetricList(), contains(Metric.CPU_CYCLES));
        assertThat(scenarioConfigs.get(2).getMetricList(), contains(Metric.BRANCHES));
        verify(metricsViewerClient).pushExecution(executionCaptor.capture());
        Execution execution = executionCaptor.getValue();
        assertThat(execution.getScenarios(), hasSize(3));
    }


    @Test
    public void shouldNotSentExecutionWithNoScenarios() {
        Scenario scenarioThatFails = prepareScenario(SCENARIO_NAME, null, REFRESH_INTERVAL_MS, null, COMMAND, TIMEOUT);
        collectorProperties.setScenariosConfig(Collections.singletonList(scenarioThatFails));
        when(metricsViewerClient.pushExecution(any(Execution.class))).thenReturn(Optional.of(EXECUTION_ID));
        when(scenarioRunnerClient.runScenario(any(ScenarioConfig.class))).thenReturn(Optional.empty());

        Optional<Integer> executionId = metricsCollector.run(collectorProperties);

        verify(scenarioRunnerClient).runScenario(scenarioCaptor.capture());
        assertThat(executionId, equalTo(Optional.empty()));
    }

    private Scenario prepareScenario(String name, List<Metric> metricList, Integer refreshIntervalMs,
                                     List<String> tags, String command, Integer timeout) {
        Scenario scenario = new Scenario();
        scenario.setName(name);
        scenario.setMetricList(metricList);
        scenario.setRefreshIntervalMs(refreshIntervalMs);
        scenario.setTags(tags);
        scenario.setCommand(command);
        scenario.setTimeout(timeout);
        return scenario;
    }

}