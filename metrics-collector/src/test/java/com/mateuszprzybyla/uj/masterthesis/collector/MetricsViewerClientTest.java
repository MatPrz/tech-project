package com.mateuszprzybyla.uj.masterthesis.collector;

import com.mateuszprzybyla.uj.masterthesis.metrics.model.Metric;
import com.mateuszprzybyla.uj.masterthesis.metrics.model.input.Execution;
import com.mateuszprzybyla.uj.masterthesis.metrics.model.input.ExecutionScenario;
import com.mateuszprzybyla.uj.masterthesis.metrics.model.input.MetricData;
import com.mateuszprzybyla.uj.masterthesis.metrics.model.input.MetricSnapshot;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.RestClientTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.client.MockRestServiceServer;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collections;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.*;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withServerError;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

@RunWith(SpringRunner.class)
@RestClientTest(MetricsViewerClient.class)
@TestPropertySource(properties = {
        "collector.metricsViewerUrl=http://localhost:8080"
})
public class MetricsViewerClientTest {
    private static final String EXECUTION_JSON = "{\"name\":\"execution name\",\"startTimeUtc\":[2016,2,13,21,15]" +
            ",\"hostname\":\"hostname\",\"summary\":{\"BRANCH_MISSES\":12.3},\"scenarios\":[{\"name\":\"scenario " +
            "name\",\"hostname\":\"hostname\",\"refreshIntervalMs\":100,\"tags\":[\"tag1\",\"tag2\"],\"summary\":" +
            "{\"TOTAL_RUN_TIME\":23.4},\"metricDatas\":[{\"key\":\"IDLE_CYCLES_FRONTEND\",\"data\":" +
            "[{\"time\":12,\"value\":23.5},{\"time\":25,\"value\":21.3}]}]}]}";
    private static final String EXECUTION_ID = "2";

    @Autowired
    private MetricsViewerClient client;
    @Autowired
    private MockRestServiceServer server;

    private Execution execution;

    @Before
    public void setUpExecution() {
        this.execution = new Execution();
        this.execution.setName("execution name");
        this.execution.setHostname("hostname");
        this.execution.setStartTimeUtc(LocalDateTime.of(2016, 2, 13, 21, 15));
        this.execution.setSummary(Collections.singletonMap(Metric.BRANCH_MISSES, 12.3));
        ExecutionScenario executionScenario = new ExecutionScenario();
        executionScenario.setName("scenario name");
        executionScenario.setHostname("hostname");
        executionScenario.setTags(Arrays.asList("tag1", "tag2"));
        executionScenario.setRefreshIntervalMs(100);
        executionScenario.setSummary(Collections.singletonMap(Metric.TOTAL_RUN_TIME, 23.4));
        MetricData metricData = new MetricData();
        metricData.setKey(Metric.IDLE_CYCLES_FRONTEND);
        executionScenario.setMetricDatas(Collections.singletonList(metricData));
        metricData.setData(Arrays.asList(new MetricSnapshot(12, 23.5), new MetricSnapshot(25, 21.3)));
        this.execution.setScenarios(Collections.singletonList(executionScenario));
    }

    @Test
    public void shouldSendExecutionAndReturnValidExecutionId() {
        this.server.expect(requestTo("/rest/executions/"))
                .andExpect(method(HttpMethod.POST))
                .andExpect(header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE))
                .andExpect(content().string(EXECUTION_JSON))
                .andRespond(withSuccess(EXECUTION_ID, MediaType.APPLICATION_JSON));

        Optional<Integer> actualExecutionId = this.client.pushExecution(this.execution);

        assertThat(actualExecutionId, equalTo(Optional.ofNullable(Integer.valueOf(EXECUTION_ID))));
    }

    @Test
    public void shouldReturnNoExecutionIdOnInvalidResponse() {
        this.server.expect(requestTo("/rest/executions/"))
                .andRespond(withSuccess("invalid response", MediaType.TEXT_PLAIN));

        Optional<Integer> actualExecutionId = this.client.pushExecution(this.execution);

        assertThat(actualExecutionId, equalTo(Optional.empty()));
    }

    @Test
    public void shouldReturnNoExecutionIdOnServerError() {
        this.server.expect(requestTo("/rest/executions/"))
                .andRespond(withServerError());

        Optional<Integer> actualExecutionId = this.client.pushExecution(this.execution);

        assertThat(actualExecutionId, equalTo(Optional.empty()));
    }
}