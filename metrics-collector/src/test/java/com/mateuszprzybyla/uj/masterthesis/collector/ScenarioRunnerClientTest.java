package com.mateuszprzybyla.uj.masterthesis.collector;

import com.mateuszprzybyla.uj.masterthesis.metrics.model.Metric;
import com.mateuszprzybyla.uj.masterthesis.metrics.model.input.MetricData;
import com.mateuszprzybyla.uj.masterthesis.metrics.model.input.MetricSnapshot;
import com.mateuszprzybyla.uj.masterthesis.metrics.model.runner.ScenarioConfig;
import com.mateuszprzybyla.uj.masterthesis.metrics.model.runner.ScenarioResult;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.RestClientTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.client.MockRestServiceServer;

import java.util.Arrays;
import java.util.Collections;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.*;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withServerError;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

@RunWith(SpringRunner.class)
@RestClientTest(ScenarioRunnerClient.class)
@TestPropertySource(properties = {
        "collector.runnerAgentUrl=http://localhost:8080"
})
public class ScenarioRunnerClientTest {
    private static final String SCENARIO_CONFIG_JSON = "{\"startCommand\":\"startCommand\",\"refreshIntervalMs\":100" +
            ",\"metricList\":[\"BRANCH_MISSES\",\"CPU_CYCLES\",\"CPU_MIGRATIONS\"],\"timeout\":15}";
    private static final String SCENARIO_RESULT_JSON = "{\"hostname\":\"hostname\", \"metricDatas\":[{\"key\":" +
            "\"IDLE_CYCLES_FRONTEND\", \"data\": [{\"time\": 12, \"value\": 23.5}, {\"time\": 25, \"value\": " +
            "21.3}]}], \"summary\": {\"TASK_CLOCK\": 123.54}}";

    @Autowired
    private ScenarioRunnerClient client;
    @Autowired
    private MockRestServiceServer server;

    private ScenarioConfig scenarioConfig;
    private ScenarioResult scenarioResult;

    @Before
    public void setUpScenarioConfig() {
        this.scenarioConfig = new ScenarioConfig();
        this.scenarioConfig.setStartCommand("startCommand");
        this.scenarioConfig.setRefreshIntervalMs(100);
        this.scenarioConfig.setTimeout(15);
        this.scenarioConfig.setMetricList(Arrays.asList(Metric.BRANCH_MISSES, Metric.CPU_CYCLES, Metric.CPU_MIGRATIONS));
    }

    @Before
    public void setUpScenarioResult() {
        this.scenarioResult = new ScenarioResult();
        this.scenarioResult.setHostname("hostname");
        this.scenarioResult.setSummary(Collections.singletonMap(Metric.TASK_CLOCK, 123.54));
        MetricData metricData = new MetricData();
        metricData.setKey(Metric.IDLE_CYCLES_FRONTEND);
        metricData.setData(Arrays.asList(new MetricSnapshot(12, 23.5d), new MetricSnapshot(25, 21.3d)));
        this.scenarioResult.setMetricDatas(Collections.singletonList(metricData));
    }

    @Test
    public void shouldSendScenarioConfigAndReturnValidResponse() {
        this.server.expect(requestTo("/runScenario"))
                .andExpect(method(HttpMethod.POST))
                .andExpect(header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(content().string(SCENARIO_CONFIG_JSON))
                .andRespond(withSuccess(SCENARIO_RESULT_JSON, MediaType.APPLICATION_JSON));

        Optional<ScenarioResult> actualResult = this.client.runScenario(this.scenarioConfig);

        assertTrue(actualResult.isPresent());
        ScenarioResult actual = actualResult.get();
        assertThat(actual.getHostname(), equalTo(this.scenarioResult.getHostname()));
        assertThat(actual.getSummary(), equalTo(this.scenarioResult.getSummary()));
        assertThat(actual.getMetricDatas(), hasSize(1));
        assertThat(actual.getMetricDatas().get(0).getData(), hasSize(2));
    }

    @Test
    public void shouldReturnNoScenarioResultOnInvalidResponse() {
        this.server.expect(requestTo("/runScenario"))
                .andRespond(withSuccess("invalid response", MediaType.TEXT_PLAIN));

        Optional<ScenarioResult> actualResult = this.client.runScenario(this.scenarioConfig);

        assertThat(actualResult, equalTo(Optional.empty()));
    }

    @Test
    public void shouldReturnNoScenarioResultOnServerError() {
        this.server.expect(requestTo("/runScenario"))
                .andRespond(withServerError());

        Optional<ScenarioResult> actualResult = this.client.runScenario(this.scenarioConfig);

        assertThat(actualResult, equalTo(Optional.empty()));
    }
}