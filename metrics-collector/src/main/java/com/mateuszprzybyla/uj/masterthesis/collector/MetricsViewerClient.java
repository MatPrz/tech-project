package com.mateuszprzybyla.uj.masterthesis.collector;

import com.mateuszprzybyla.uj.masterthesis.metrics.model.input.Execution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.Optional;

@Component
class MetricsViewerClient extends Java8RestClient {
    private static final Logger LOGGER = LoggerFactory.getLogger(MetricsViewerClient.class);

    private final String metricsViewerUrl;
    private final RestTemplate restTemplate;

    @Autowired
    public MetricsViewerClient(RestTemplateBuilder restTemplateBuilder,
                               @Value("${collector.metricsViewerUrl}") String metricsViewerUrl) {
        super(restTemplateBuilder);
        this.metricsViewerUrl = metricsViewerUrl;
        this.restTemplate = restTemplateBuilder.rootUri(metricsViewerUrl).build();
    }

    Optional<Integer> pushExecution(Execution execution) {
        HttpEntity<Execution> httpEntity = createHttpEntity(execution);
        try {
            return Optional.of(restTemplate.postForObject("/rest/executions/", httpEntity, Integer.class));
        } catch (RestClientException e) {
            LOGGER.error(
                    String.format("Could not push executions to %s, entity: %s", metricsViewerUrl, httpEntity), e);
            return Optional.empty();
        }
    }

    private HttpEntity<Execution> createHttpEntity(Execution execution) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        return new HttpEntity<>(execution, headers);
    }
}
