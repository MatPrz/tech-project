package com.mateuszprzybyla.uj.masterthesis.collector.validation;

import com.mateuszprzybyla.uj.masterthesis.collector.properties.CollectorProperties;
import com.mateuszprzybyla.uj.masterthesis.collector.properties.Scenario;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.ListUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.List;

public class EachScenarioHasMetricsValidator implements ConstraintValidator<EachScenarioHasMetrics, CollectorProperties> {
    private EachScenarioHasMetrics constraintAnnotation;

    @Override
    public void initialize(EachScenarioHasMetrics constraintAnnotation) {
        this.constraintAnnotation = constraintAnnotation;
    }

    @Override
    public boolean isValid(CollectorProperties properties, ConstraintValidatorContext context) {
        boolean result = true;
        List<Scenario> scenarioList = ListUtils.emptyIfNull(properties.getScenariosConfig());
        for (int i = 0; i < scenarioList.size(); i++) {
            Scenario scenario = scenarioList.get(i);
            if (CollectionUtils.isEmpty(properties.getMetricList())
                    && CollectionUtils.isEmpty(scenario.getMetricList())) {
                context.buildConstraintViolationWithTemplate(constraintAnnotation.message())
                        .addPropertyNode("scenariosConfig").addBeanNode()
                        .inIterable().atKey(i).addConstraintViolation();
                result = false;
            }
        }
        return result;
    }
}
