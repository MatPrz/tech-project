package com.mateuszprzybyla.uj.masterthesis.collector;

import com.mateuszprzybyla.uj.masterthesis.metrics.model.runner.ScenarioConfig;
import com.mateuszprzybyla.uj.masterthesis.metrics.model.runner.ScenarioResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.Optional;

@Component
class ScenarioRunnerClient extends Java8RestClient {
    private static final Logger LOGGER = LoggerFactory.getLogger(ScenarioRunnerClient.class);

    private final String runnerAgentUrl;
    private final RestTemplate restTemplate;

    @Autowired
    public ScenarioRunnerClient(RestTemplateBuilder restTemplateBuilder,
                                @Value("${collector.runnerAgentUrl}") String runnerAgentUrl) {
        super(restTemplateBuilder);
        this.runnerAgentUrl = runnerAgentUrl;
        this.restTemplate = restTemplateBuilder.rootUri(runnerAgentUrl).build();
    }

    Optional<ScenarioResult> runScenario(ScenarioConfig scenarioConfig) {
        LOGGER.info("Sending a scenario to runner " + runnerAgentUrl);
        try {
            ScenarioResult scenarioResult = restTemplate
                    .postForObject("/runScenario", scenarioConfig, ScenarioResult.class);
            LOGGER.info("Retrieved a valid scenario result");
            return Optional.of(scenarioResult);
        } catch (RestClientException e) {
            LOGGER.error("Error occured during running a scenario on runner " + runnerAgentUrl, e);
            return Optional.empty();
        }
    }
}
