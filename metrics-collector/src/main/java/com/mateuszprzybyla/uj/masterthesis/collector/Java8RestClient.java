package com.mateuszprzybyla.uj.masterthesis.collector;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;

class Java8RestClient {
    protected RestTemplateBuilder restTemplateBuilder;

    Java8RestClient(RestTemplateBuilder restTemplateBuilder) {
        ObjectMapper mapper = new ObjectMapper()
                .disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
                .registerModule(new JavaTimeModule());
        this.restTemplateBuilder = restTemplateBuilder
                .messageConverters(new MappingJackson2HttpMessageConverter(mapper));
    }
}
