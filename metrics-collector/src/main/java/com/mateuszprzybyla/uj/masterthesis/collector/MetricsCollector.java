package com.mateuszprzybyla.uj.masterthesis.collector;

import com.mateuszprzybyla.uj.masterthesis.collector.properties.CollectorProperties;
import com.mateuszprzybyla.uj.masterthesis.collector.properties.Scenario;
import com.mateuszprzybyla.uj.masterthesis.metrics.model.input.Execution;
import com.mateuszprzybyla.uj.masterthesis.metrics.model.input.ExecutionScenario;
import com.mateuszprzybyla.uj.masterthesis.metrics.model.runner.ScenarioConfig;
import com.mateuszprzybyla.uj.masterthesis.metrics.model.runner.ScenarioResult;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Optional;

@Component
class MetricsCollector {
    private static final Logger LOGGER = LoggerFactory.getLogger(MetricsCollector.class);
    private final MetricsViewerClient metricsViewerClient;
    private final ScenarioRunnerClient scenarioRunnerClient;

    @Autowired
    public MetricsCollector(MetricsViewerClient metricsViewerClient,
                            ScenarioRunnerClient scenarioRunnerClient) {
        this.metricsViewerClient = metricsViewerClient;
        this.scenarioRunnerClient = scenarioRunnerClient;
    }

    Optional<Integer> run(CollectorProperties collectorProperties) {
        Execution execution = new Execution();
        execution.setStartTimeUtc(LocalDateTime.now());
        execution.setName(collectorProperties.getName());
        try {
            execution.setHostname(InetAddress.getLocalHost().getHostName());
            LOGGER.info("Hostname resolved: " + execution.getHostname());
        } catch (UnknownHostException e) {
            execution.setHostname("unknown");
            LOGGER.warn("Could not resolve host name of the machine, setting 'unknown' as host name", e);
        }
        execution.setScenarios(new ArrayList<>());
        String runnerAgentUrl = collectorProperties.getRunnerAgentUrl();
        for (Scenario scenario : collectorProperties.getScenariosConfig()) {
            LOGGER.info("Running a scenario {} on the runner {}", scenario.getName(), runnerAgentUrl);
            Optional<ScenarioResult> scenarioResultOptional = scenarioRunnerClient.runScenario(
                    convertScenarioToScenarioConfig(collectorProperties, scenario));
            Optional<ExecutionScenario> executionScenarioOptional = scenarioResultOptional.map(
                    scenarioResult -> convertToExecutionScenario(scenarioResult, scenario));
            executionScenarioOptional.ifPresent(execution.getScenarios()::add);
        }
        if (CollectionUtils.isEmpty(execution.getScenarios())) {
            LOGGER.warn("No scenarios data in the execution, stopping the processing");
            return Optional.empty();
        }
        Optional<Integer> executionId = metricsViewerClient.pushExecution(execution);
        if (executionId.isPresent()) {
            LOGGER.info("Execution sent to the viewer! Returned id: {}", executionId.get());
            LOGGER.info("Direct execution URL: {}{}{}", collectorProperties.getMetricsViewerUrl(), "execution/summary/", executionId.get());
        } else {
            LOGGER.warn("Could not sent execution to the viewer");
        }
        return executionId;
    }

    private ExecutionScenario convertToExecutionScenario(ScenarioResult scenarioResult, Scenario scenario) {
        ExecutionScenario executionScenario = new ExecutionScenario();
        executionScenario.setName(scenario.getName());
        executionScenario.setHostname(scenarioResult.getHostname());
        executionScenario.setTags(scenario.getTags());
        executionScenario.setMetricDatas(scenarioResult.getMetricDatas());
        executionScenario.setSummary(scenarioResult.getSummary());
        executionScenario.setRefreshIntervalMs(scenario.getRefreshIntervalMs());
        return executionScenario;
    }

    private ScenarioConfig convertScenarioToScenarioConfig(CollectorProperties collectorProperties, Scenario scenario) {
        ScenarioConfig scenarioConfig = new ScenarioConfig();
        scenarioConfig.setRefreshIntervalMs(scenario.getRefreshIntervalMs());
        scenarioConfig.setStartCommand(scenario.getCommand());
        scenarioConfig.setMetricList(CollectionUtils.isEmpty(scenario.getMetricList())
                ? collectorProperties.getMetricList() : scenario.getMetricList());
        scenarioConfig.setTimeout(scenario.getTimeout());
        return scenarioConfig;
    }
}
