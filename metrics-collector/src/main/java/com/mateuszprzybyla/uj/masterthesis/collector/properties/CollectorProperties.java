package com.mateuszprzybyla.uj.masterthesis.collector.properties;

import com.mateuszprzybyla.uj.masterthesis.collector.validation.EachScenarioHasMetrics;
import com.mateuszprzybyla.uj.masterthesis.metrics.model.Metric;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.URL;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import java.util.List;

@Component
@ConfigurationProperties("collector")
@Validated
@EachScenarioHasMetrics(message = "Scenario does not have a list of metrics specified")
public class CollectorProperties {
    @NotBlank(message = "Execution name cannot be blank")
    private String name;
    @NotBlank(message = "metricsViewerUrl cannot be blank")
    @URL(message = "metricsViewerUrl is not a valid URL")
    private String metricsViewerUrl;
    @NotBlank(message = "runnerAgentUrl cannot be blank")
    @URL(message = "runnerAgentUrl is not a valid URL")
    private String runnerAgentUrl;
    @NotEmpty(message = "scenarios list cannot be empty")
    @Valid
    private List<Scenario> scenariosConfig;
    @Valid
    private List<Metric> metricList;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMetricsViewerUrl() {
        return metricsViewerUrl;
    }

    public void setMetricsViewerUrl(String metricsViewerUrl) {
        this.metricsViewerUrl = metricsViewerUrl;
    }

    public String getRunnerAgentUrl() {
        return runnerAgentUrl;
    }

    public void setRunnerAgentUrl(String runnerAgentUrl) {
        this.runnerAgentUrl = runnerAgentUrl;
    }

    public List<Scenario> getScenariosConfig() {
        return scenariosConfig;
    }

    public void setScenariosConfig(List<Scenario> scenariosConfig) {
        this.scenariosConfig = scenariosConfig;
    }

    public List<Metric> getMetricList() {
        return metricList;
    }

    public void setMetricList(List<Metric> metricList) {
        this.metricList = metricList;
    }


}
