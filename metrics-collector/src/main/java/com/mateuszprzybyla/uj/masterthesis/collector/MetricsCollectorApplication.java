package com.mateuszprzybyla.uj.masterthesis.collector;

import com.mateuszprzybyla.uj.masterthesis.collector.properties.CollectorProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class MetricsCollectorApplication {
    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(MetricsCollectorApplication.class, args);
        CollectorProperties collectorProperties = context.getBean(CollectorProperties.class);
        MetricsCollector metricsCollector = context.getBean(MetricsCollector.class);
        metricsCollector.run(collectorProperties);
        context.close();
    }
}
