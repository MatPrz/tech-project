package com.mateuszprzybyla.uj.masterthesis.collector.properties;

import com.mateuszprzybyla.uj.masterthesis.metrics.model.Metric;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.Range;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotNull;
import java.util.List;

public class Scenario {
    @NotBlank(message = "Scenario name cannot be blank")
    private String name;
    @NotNull(message = "refreshIntervalMs cannot be empty")
    @Range(min = 100, message = "refreshIntervalMs must be at least 100 ms")
    private Integer refreshIntervalMs;
    @NotBlank(message = "Scenario command cannot be blank")
    private String command;
    private List<String> tags;
    private Integer timeout;
    private List<Metric> metricList;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getRefreshIntervalMs() {
        return refreshIntervalMs;
    }

    public void setRefreshIntervalMs(Integer refreshIntervalMs) {
        this.refreshIntervalMs = refreshIntervalMs;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public List<Metric> getMetricList() {
        return metricList;
    }

    public void setMetricList(List<Metric> metricList) {
        this.metricList = metricList;
    }

    public Integer getTimeout() {
        return timeout;
    }

    public void setTimeout(Integer timeout) {
        this.timeout = timeout;
    }
}
