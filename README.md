# README #

This repository contains technical part of master thesis implemented by Mateusz Przybyla.

### Project overview ###

![metrics_master_thesis_diagram.png](https://bitbucket.org/repo/kEjqA8/images/730798886-metrics_master_thesis_diagram.png)

* **metrics-viewer-web**

This component is responsible for accepting and displaying metrics gathered for given executions.
It provides a user interface with charts and allows a user to compare selected executions and scenarios.

* **scenario-runner**

This component is an agent that can be run on any machine with UNIX OS. It's responsible for accepting a
request to run a single scenario and gathering all metrics that have been requested.
It exposes a single HTTP endpoint through which scenarios can be run. To fully use the potential of the
component, it is advised to install package containing [perf-stat tool](https://perf.wiki.kernel.org/index.php/Tutorial)

* **metrics-collector**

This component is responsible for accepting a configuration for whole execution, running all scenarios with
provided configuration and sending collected results to metrics-viewer-web component.

### Prerequisites ###

Following tools are needed to take full advantage of the project:

* UNIX operating system (to host all components and run components related script)
* [Java Runtime Environment](http://www.oracle.com/technetwork/java/javase/downloads/jre8-downloads-2133155.html)
(needed to run scenario-runner and metrics-collector components)
* [Docker](https://www.docker.com/) (needed to compile and build all components and then to run metrics-viewer-web component)
* [perf-stat tool](https://perf.wiki.kernel.org/index.php/Tutorial) - can be installed through packages `apt-get install linux-tools-common linux-tools-generic linux-tools-$(uname -r)`


### Quick start ###

**1. Building project**

Go to the directory with project sources (the one with `Dockerfile`) and run following command to build
all the components from the sources:
```
docker run -it --rm --name metrics-master-thesis -v "$PWD":/usr/src/app -v "$HOME"/.m2:/root/.m2 -w /usr/src/app maven:3.5-jdk-8 mvn package
```
In order to clean the project artifacts run:
```
docker run -it --rm --name metrics-master-thesis -v "$PWD":/usr/src/app -v "$HOME"/.m2:/root/.m2 -w /usr/src/app maven:3.5-jdk-8 mvn clean
```
*Do not run clean and package goals with single command!*

**2. Building Docker image with metrics-viewer-web component**

After building all the artifacts you can build a Docker container that will host metrics-viewer-web component.
Run following command from where `Dockerfile` is located:
```
docker build . -t metrics-viewer-web:1.0
```
After the image has been built, you're ready to start a Docker container with this image. 
```
docker run -p 8090:8080 -v "$HOME"/mgr_workspace/docker_home:/usr/metrics-app/mounted metrics-viewer-web:1.0
```
This is a sample command, but of course you're free to use any other Docker features. If you want to persist (and also access) logs and database state just make sure that you're mounting `/usr/metrics-app/mounted` directory.
If everything went right, you should see a following message in the log file (or in the console)
```
TomcatEmbeddedServletContainer - Tomcat started on port(s): 8080 (http)
```
Remember that 8080 port is a port of the Docker container. You're now ready to set up scenario runner component(s). To use it (together with metrics-collector) locally in an easy way follow the next steps.

**3. Downloading client package**

A client package can be downloaded from main page of metrics-viewer-web component to make running  everything locally easier.
To ensure that permissions of executable scripts will are preserved untar the package with `-p` flag.

**4. Using client package**

Client package consists of `bin` directory (containing scripts for end users) and `lib` directory (with artifacts that are used by those scripts). `bin/performance` is a script that should be used in order to use all the components locally. It supports following commands:

`/performance agent start` - starts **scenario-runner** agent locally listening on port `8080`

`/performance agent stop` - stops **scenario-runner** agent (if running)

`/performance runConfig config.yaml` - runs locally **metrics-collector** application with a configuration specified in `config.yaml` file. If an agent has been already started, it's used. If no agent has been started, this command will spawn a new **scenario-runner** agent, run **metrics-collector** and shut down the agent.

You can find an example YAML file with list of supported parameters below.

### Features ###

+ **YAML format supported by metrics-collector**

```
#!yaml

collector:
    name: Execution Name
    metricsViewerUrl: http://localhost:8090/
    runnerAgentUrl: http://localhost:8080/
    metricList: MEMORY_USAGE, VIRTUAL_MEMORY_SIZE, CONTEXT_SWITCHES, BRANCHES
    scenariosConfig:
        - name: Short Run
          refreshIntervalMs: 250
          tags: tag1, tag2, tag3
          command: stress --cpu 1 --timeout 3
        - name: Long Run
          refreshIntervalMs: 250
          tags: tag2, tag4, tag3
          command: stress --cpu 8 --timeout 5
          metricList: VIRTUAL_MEMORY_SIZE, CONTEXT_SWITCHES, BRANCHES, BRANCH_MISSES
```
`collector.name` - name of the execution for which the data are collected

`collector.metricsViewerUrl` - location of the **metrics-viewer-web** component that will accept the execution data (list of supported metrics is presented below)

`collector.runnerAgentUrl` - optional location of the **scenario-runner** component that will execute all the scenarios, if not provided, it will be determined by `performance` script

`collector.metricList` - list of metrics that is going to be collected for all the scenarios (unless overrided)

`collector.scenariosConfig[*].name` - name of the scenario

`collector.scenariosConfig[*].refreshIntervalMs` - how often the metrics will be collected

`collector.scenariosConfig[*].tags` - comma separated list of tags (strings) that are associated with a given scenario run

`collector.scenariosConfig[*].command` - command for running a single scenario

`collector.scenariosConfig[*].metricList` - optional configuration for metrics that should be collected for this specific scenario (if not provided `collector.metricList` value will be used)

`collector.scenariosConfig[*].timeout` - how long (in seconds) the collector should wait for the scenario to finish