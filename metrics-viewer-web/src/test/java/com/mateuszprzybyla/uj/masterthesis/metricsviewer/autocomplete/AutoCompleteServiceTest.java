package com.mateuszprzybyla.uj.masterthesis.metricsviewer.autocomplete;

import com.mateuszprzybyla.uj.masterthesis.metricsviewer.dao.ExecutionDAO;
import com.mateuszprzybyla.uj.masterthesis.metricsviewer.dao.ScenarioDAO;
import com.mateuszprzybyla.uj.masterthesis.metricsviewer.dto.ExecutionDTO;
import com.mateuszprzybyla.uj.masterthesis.metricsviewer.dto.ScenarioDTO;
import com.mateuszprzybyla.uj.masterthesis.metricsviewer.dto.ScenarioTagDTO;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AutoCompleteServiceTest {
    private static final LocalDateTime NOW = LocalDateTime.of(2017, 7, 19, 23, 23, 23);

    @InjectMocks
    private AutoCompleteService service;
    @Mock
    private ScenarioDAO scenarioDAO;
    @Mock
    private ExecutionDAO executionDAO;

    @Before
    public void setUp() {
        when(scenarioDAO.findAll()).thenReturn(Arrays.asList(
                getScenarioWithTags("scenario name 1", "tagUnique", "tagShared"),
                getScenarioWithTags("scenario name 1", "anotherUniqueTag", "tagShared"),
                getScenarioWithTags("scenario name 2")
        ));
        when(executionDAO.findAll()).thenReturn(Arrays.asList(
                getExecutionWithNameAndStartTime(2, "execution 2", NOW),
                getExecutionWithNameAndStartTime(1, "execution 1", NOW.minusDays(1)),
                getExecutionWithNameAndStartTime(3, "execution 3", NOW.plusDays(1))
        ));
    }

    @Test
    public void shouldReturnEmptyTagsIfDAODoesNotFindAnything() {
        when(scenarioDAO.findAll()).thenReturn(Collections.emptyList());

        assertThat(service.getTags("tagName"), empty());
    }

    @Test
    public void shouldReturnAllMatchingScenarioTags() {
        List<AutoCompleteItem> items = service.getTags("tagUnique");

        assertThat(items, hasSize(1));
        assertThat(items.get(0), equalTo(new AutoCompleteItem<>("tagUnique", "tagUnique")));

        assertThat(service.getTags("tag"), hasSize(3));

        assertThat(service.getTags("tagShared"), hasSize(1));
    }

    @Test
    public void shouldIgnoreCasesOnTagsSearch() {
        assertThat(service.getTags("TaG"), hasSize(3));
    }

    @Test
    public void shouldReturnEmptyExecutionsIfDAODoesNotFindAnything() {
        when(executionDAO.findAll()).thenReturn(Collections.emptyList());

        assertThat(service.getExecutions("executions"), empty());
    }

    @Test
    public void shouldReturnMatchingExecutionsWithMeaningfulNameOrderedByStartTimeFromTheNewest() {
        List<AutoCompleteItem> items = service.getExecutions("execUtion");

        assertThat(items, hasSize(3));
        assertThat(items.get(0), equalTo(new AutoCompleteItem<>(3, "execution 3 (Jul 20, 2017 11:23:23 PM)")));
        assertThat(items.stream().map(AutoCompleteItem::getId).collect(Collectors.toList()),
                equalTo(Arrays.asList(3, 2, 1)));

        assertThat(service.getExecutions("1"), hasSize(1));
    }

    @Test
    public void shouldReturnEmptyScenariosIfDAODoesNotFindAnything() {
        when(scenarioDAO.findAll()).thenReturn(Collections.emptyList());

        assertThat(service.getScenarios("scenarios"), empty());
    }

    @Test
    public void shouldReturnAllMatchingScenarios() {
        List<AutoCompleteItem> items = service.getScenarios("scenario name 2");

        assertThat(items, hasSize(1));
        assertThat(items.get(0), equalTo(new AutoCompleteItem<>("scenario name 2", "scenario name 2")));

        assertThat(service.getScenarios("scenariO"), hasSize(2));
    }

    private ScenarioDTO getScenarioWithTags(String name, String... tagNames) {
        ScenarioDTO scenarioDTO = new ScenarioDTO();
        scenarioDTO.setName(name);
        scenarioDTO.setTags(Arrays.stream(tagNames).map(ScenarioTagDTO::new).collect(Collectors.toList()));
        return scenarioDTO;
    }

    private ExecutionDTO getExecutionWithNameAndStartTime(int id, String name, LocalDateTime startTime) {
        ExecutionDTO executionDTO = new ExecutionDTO();
        executionDTO.setId(id);
        executionDTO.setName(name);
        executionDTO.setStartTimeUtc(startTime);
        return executionDTO;
    }
}