package com.mateuszprzybyla.uj.masterthesis.metricsviewer.controller;

import com.mateuszprzybyla.uj.masterthesis.metrics.model.ui.execution.ExecutionInfo;
import com.mateuszprzybyla.uj.masterthesis.metrics.model.ui.execution.ExecutionSummary;
import com.mateuszprzybyla.uj.masterthesis.metricsviewer.dto.ExecutionDTO;
import com.mateuszprzybyla.uj.masterthesis.metricsviewer.service.ExecutionService;
import com.mateuszprzybyla.uj.masterthesis.metricsviewer.service.ExecutionStorageService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.data.web.config.EnableSpringDataWebSupport;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(ExecutionController.class)
@EnableSpringDataWebSupport
public class ExecutionControllerTest {
    private static final LocalDateTime NOW = LocalDateTime.of(2017, 7, 19, 23, 23, 23);
    private static final List<ExecutionInfo> EXECUTION_INFO_LIST = Arrays.asList(
            createExecutionInfo(1, "execution 1", NOW.toEpochSecond(ZoneOffset.UTC)),
            createExecutionInfo(2, "execution 2", NOW.minusDays(2).toEpochSecond(ZoneOffset.UTC))
    );
    private static final String EXECUTION_INFO_JSON = "{\"content\":[{\"id\":1,\"name\":\"execution 1\"," +
            "\"startTimeUtc\":1500506603},{\"id\":2,\"name\":\"execution 2\",\"startTimeUtc\":1500333803}]," +
            "\"totalPages\":1,\"totalElements\":2,\"last\":true,\"sort\":null,\"numberOfElements\":2," +
            "\"first\":true,\"size\":0,\"number\":0}";
    private static final int EXECUTION_ID = 1;
    private static final ExecutionSummary EXECUTION_SUMMARY =
            createExecutionSummary(1, "summary 1", NOW.toEpochSecond(ZoneOffset.UTC));
    private static final String EXECUTION_SUMMARY_JSON = "{\"id\":1,\"name\":\"summary 1\",\"hostname\":null," +
            "\"startTimeUtc\":1500506603,\"summary\":null,\"scenarios\":null}";

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @MockBean
    private ExecutionService executionService;

    @MockBean
    private ExecutionStorageService storageService;

    @Before
    public void setUp() {
        when(executionService.getExecutionSummary(EXECUTION_ID)).thenReturn(Optional.of(EXECUTION_SUMMARY));
        when(executionService.getRecentExecutionSummary()).thenReturn(Optional.of(EXECUTION_SUMMARY));
        when(executionService.getAllExecutionInfo(any())).thenReturn(new PageImpl<>(EXECUTION_INFO_LIST));
        when(storageService.storeExecutionData(any())).thenReturn(getExecutionWithId(EXECUTION_ID));
    }

    @Test
    public void shouldReturnEmptyPageWhenNoPageInfoInTheRequestProvideed() throws Exception {
        when(executionService.getAllExecutionInfo(any())).thenReturn(new PageImpl<>(Collections.emptyList()));

        this.mockMvc.perform(get("/rest/executions/infoAll"))
                .andExpect(content().json("{\"content\":[],\"totalElements\":0,\"last\":true,\"totalPages\":1," +
                        "\"sort\":null,\"numberOfElements\":0,\"first\":true,\"size\":0,\"number\":0}"));

        ArgumentCaptor<Pageable> pageableCaptor = ArgumentCaptor.forClass(Pageable.class);
        verify(executionService).getAllExecutionInfo(pageableCaptor.capture());
        Pageable pageable = pageableCaptor.getValue();
        assertThat(pageable.getPageNumber(), equalTo(0));
        assertThat(pageable.getPageSize(), equalTo(20));
    }

    @Test
    public void shouldReturnEmptyPageWhenNoExecutionInfoFound() throws Exception {
        when(executionService.getAllExecutionInfo(any())).thenReturn(new PageImpl<>(Collections.emptyList()));

        this.mockMvc.perform(get("/rest/executions/infoAll?page=0&pageSize=20"))
                .andExpect(content().json("{\"content\":[],\"totalElements\":0,\"last\":true,\"totalPages\":1," +
                        "\"sort\":null,\"numberOfElements\":0,\"first\":true,\"size\":0,\"number\":0}"));

        ArgumentCaptor<Pageable> pageableCaptor = ArgumentCaptor.forClass(Pageable.class);
        verify(executionService).getAllExecutionInfo(pageableCaptor.capture());
        Pageable pageable = pageableCaptor.getValue();
        assertThat(pageable.getPageNumber(), equalTo(0));
        assertThat(pageable.getPageSize(), equalTo(20));
    }


    @Test
    public void shouldReturnFoundExecutionInfo() throws Exception {
        this.mockMvc.perform(get("/rest/executions/infoAll?page=0&pageSize=10"))
                .andExpect(content().json(EXECUTION_INFO_JSON));
    }

    @Test
    public void shouldStoreExecutionData() throws Exception {
        this.mockMvc.perform(
                post("/rest/executions/")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{}"))
                .andExpect(content().string(String.valueOf(EXECUTION_ID)));
    }

    @Test
    public void shouldReturnNotFoundWhenNoRecentExecutionSummary() throws Exception {
        when(executionService.getRecentExecutionSummary()).thenReturn(Optional.empty());

        this.mockMvc.perform(get("/rest/executions/summary")).andExpect(status().isNotFound());
    }

    @Test
    public void shouldReturnValidResponseWhenRecentExecutionSummaryExists() throws Exception {
        this.mockMvc.perform(get("/rest/executions/summary")).andExpect(content().json(EXECUTION_SUMMARY_JSON));
    }

    @Test
    public void shouldReturnNotFoundWhenNoGivenExecutionSummary() throws Exception {
        when(executionService.getExecutionSummary(EXECUTION_ID)).thenReturn(Optional.empty());

        this.mockMvc.perform(get("/rest/executions/summary/" + EXECUTION_ID)).andExpect(status().isNotFound());
    }

    @Test
    public void shouldReturnValidResponseWhenGivenExecutionSummaryExists() throws Exception {
        this.mockMvc.perform(get("/rest/executions/summary/" + EXECUTION_ID)).andExpect(content().json(EXECUTION_SUMMARY_JSON));
    }

    private static ExecutionInfo createExecutionInfo(int id, String name, long startTimeUtc) {
        ExecutionInfo executionInfo = new ExecutionInfo();
        executionInfo.setId(id);
        executionInfo.setName(name);
        executionInfo.setStartTimeUtc(startTimeUtc);
        return executionInfo;
    }

    private ExecutionDTO getExecutionWithId(int id) {
        ExecutionDTO executionDTO = new ExecutionDTO();
        executionDTO.setId(id);
        return executionDTO;
    }

    private static ExecutionSummary createExecutionSummary(int id, String name, long startTimeUtc) {
        ExecutionSummary executionSummary = new ExecutionSummary();
        executionSummary.setId(id);
        executionSummary.setName(name);
        executionSummary.setStartTimeUtc(startTimeUtc);
        return executionSummary;
    }
}