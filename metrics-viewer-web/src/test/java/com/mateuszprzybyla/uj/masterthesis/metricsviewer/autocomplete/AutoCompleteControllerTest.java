package com.mateuszprzybyla.uj.masterthesis.metricsviewer.autocomplete;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(AutoCompleteController.class)
public class AutoCompleteControllerTest {
    private static final List<AutoCompleteItem> ITEMS = Arrays.asList(
            new AutoCompleteItem<>("id1", "text1"), new AutoCompleteItem<>("id2", "text2"));
    private static final String JSON = "[{\"id\":\"id1\",\"text\":\"text1\"},{\"id\":\"id2\",\"text\":\"text2\"}]";

    @Autowired
    private MockMvc mockMvc;

    @MockBean(answer = Answers.RETURNS_DEFAULTS)
    private AutoCompleteService autoCompleteService;

    @Before
    public void setUp() {
        when(autoCompleteService.getTags("tagName")).thenReturn(ITEMS);
        when(autoCompleteService.getExecutions("eName")).thenReturn(ITEMS);
        when(autoCompleteService.getScenarios("sName")).thenReturn(ITEMS);
    }

    @Test
    public void shouldReturnValidTagsForValidRequest() throws Exception {
        this.mockMvc.perform(get("/rest/autocomplete/tags/tagName"))
                .andDo(print())
                .andExpect(content().json(JSON));
    }

    @Test
    public void shouldReturnValidExecutionsForValidRequest() throws Exception {
        this.mockMvc.perform(get("/rest/autocomplete/executions/eName"))
                .andDo(print())
                .andExpect(content().json(JSON));
    }

    @Test
    public void shouldReturnValidScenariosForValidRequest() throws Exception {
        this.mockMvc.perform(get("/rest/autocomplete/scenarios/sName"))
                .andDo(print())
                .andExpect(content().json(JSON));
    }

    @Test
    public void shouldReturnBadRequestForInvalidTagInput() throws Exception {
        this.mockMvc.perform(get("/rest/autocomplete/tags/ "))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    public void shouldReturnBadRequestForInvalidExecutionInput() throws Exception {
        this.mockMvc.perform(get("/rest/autocomplete/executions/ "))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    public void shouldReturnBadRequestForInvalidScenarioInput() throws Exception {
        this.mockMvc.perform(get("/rest/autocomplete/scenarios/ "))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }
}