package com.mateuszprzybyla.uj.masterthesis.metricsviewer.controller;

import com.mateuszprzybyla.uj.masterthesis.metrics.model.ui.chart.ChartsResult;
import com.mateuszprzybyla.uj.masterthesis.metricsviewer.service.ChartService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(ChartController.class)
public class ChartControllerTest {
    private static final ChartsResult EMPTY_CHART_RESULT = new ChartsResult();

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ChartService chartService;

    @Before
    public void setUp() {
        when(chartService.getExecutionTrends(15)).thenReturn(EMPTY_CHART_RESULT);
        when(chartService.getChartsForComparisonFilter(any())).thenReturn(EMPTY_CHART_RESULT);
    }

    @Test
    public void shouldReturnChartResultForFullComparisonFilter() throws Exception {
        assertComparisonTestCaseForJsonFilter("{" +
                "  \"tags\": [\"tag1\", \"tag2\"]," +
                "  \"executions\": [1, 2, 3]," +
                "  \"scenarios\": [\"scenario one\", \"scenario two\"]," +
                "  \"timeFrom\": \"2015-05-22T21:14:22\"," +
                "  \"timeTo\": \"2015-05-22T21:14:22\"" +
                "}");
    }

    @Test
    public void shouldReturnChartResultForTagOnlyComparisonFilter() throws Exception {
        assertComparisonTestCaseForJsonFilter("{" +
                "  \"tags\": [\"tag1\", \"tag2\"]" +
                "}");
    }

    @Test
    public void shouldReturnChartResultForExecutionOnlyComparisonFilter() throws Exception {
        assertComparisonTestCaseForJsonFilter("{" +
                "  \"executions\": [1, 2, 3]" +
                "}");
    }

    @Test
    public void shouldReturnChartResultForScenarioOnlyComparisonFilter() throws Exception {
        assertComparisonTestCaseForJsonFilter("{" +
                "  \"scenarios\": [\"scenario one\", \"scenario two\"]" +
                "}");
    }

    @Test
    public void shouldReturnChartResultForTimeFromOnlyComparisonFilter() throws Exception {
        assertComparisonTestCaseForJsonFilter("{" +
                "  \"timeFrom\": \"2015-05-22T21:14:22\"" +
                "}");
    }

    @Test
    public void shouldReturnChartResultForTimeToOnlyComparisonFilter() throws Exception {
        assertComparisonTestCaseForJsonFilter("{" +
                "  \"timeTo\": \"2015-05-22T21:14:22\"" +
                "}");
    }

    @Test
    public void shouldReturnBadRequestForEmptyComparisonFilter() throws Exception {
        this.mockMvc.perform(post("/rest/charts/byComparisonFilter")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content("{" +
                        "  \"tags\": []," +
                        "  \"executions\": []," +
                        "  \"scenarios\": []" +
                        "}"))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    public void shouldReturnBadRequestForInvalidDateFormatInComparisonFilter() throws Exception {
        this.mockMvc.perform(post("/rest/charts/byComparisonFilter")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content("{" +
                        "  \"timeTo\": \"ala ma kota\"" +
                        "}"))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    public void shouldReturnChartResultForExecutionTrends() throws Exception {
        this.mockMvc.perform(get("/rest/charts/executionTrends/15"))
                .andDo(print())
                .andExpect(content().json("{\"lineCharts\":[],\"columnCharts\":[]}"));
    }

    private void assertComparisonTestCaseForJsonFilter(String comparisonFilterJson) throws Exception {
        this.mockMvc.perform(post("/rest/charts/byComparisonFilter")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(comparisonFilterJson))
                .andDo(print())
                .andExpect(content().json("{\"lineCharts\":[],\"columnCharts\":[]}"));
    }

}