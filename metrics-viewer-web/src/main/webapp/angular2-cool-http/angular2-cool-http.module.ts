import { NgModule, ModuleWithProviders } from '@angular/core';
import { HttpModule } from '@angular/http';

import { CoolHttp } from './cool-http.service';

export * from './cookie-store.service';
export * from './cool-http.service';
export * from './http-header.model';
export * from './request-interceptor.interface';
export * from './response-interceptor.interface';

@NgModule({
    exports: [],
    imports: [HttpModule],
    providers: [CoolHttp]
})
export class CoolHttpModule {
    /** @deprecated */
    public static forRoot(): ModuleWithProviders {
        return {
            ngModule: CoolHttpModule,
            providers: []
        };
    }
}