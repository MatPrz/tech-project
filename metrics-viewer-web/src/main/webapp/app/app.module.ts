import "./rxjs-operators";
import {NgModule} from "@angular/core";
import {BrowserModule} from "@angular/platform-browser";
import {AppComponent} from "./components/app.component";
import {ChartModule} from "angular2-highcharts";
import {HttpModule} from "@angular/http";
import {RouterModule} from "@angular/router";
import {LineChartComponent} from "./components/lineChart.component";
import {ExecutionSummaryComponent} from "./components/executionSummary.component";
import {ExecutionService} from "./service/execution.service";
import {DashboardComponent} from "./components/dashboard.component";
import {ScenarioDetailsComponent} from "./components/scenarioDetails.component";
import {ScenarioService} from "./service/scenario.service";
import {ScenariosDetailsParentComponent} from "./components/scenariosDetailsParent.component";
import {ComparisonComponent} from "./components/comparison.component";
import {ChartService} from "./service/chart.service";
import {FormsModule} from "@angular/forms";
import {GeneralService} from "./service/general.service";
import {ModalModule} from "angular2-modal";
import {BootstrapModalModule} from "angular2-modal/plugins/bootstrap";
import {ExecutionTrendsComponent} from "./components/trendsExecution.component";
import {ColumnChartComponent} from "./components/columnChart.component";
import {SelectModule} from "ng2-select";
import {AutoCompleteComponent} from "./components/autoComplete.component";
import {TagsAutoCompleteProvider} from "./service/tags-auto-complete-provider.service";
import {AutoCompleteService} from "./service/auto-complete.service";
import {ExecutionsAutoCompleteProvider} from "./service/executions-auto-complete-provider.service";
import {ScenariosAutoCompleteProvider} from "./service/scenarios-auto-complete-provider.service";
import {CoolHttpModule} from "../angular2-cool-http/angular2-cool-http.module";
import {CoolLoadingIndicatorModule} from '../angular2-cool-loading-indicator/index';


@NgModule({
    imports: [
        BrowserModule,
        HttpModule,
        FormsModule,
        RouterModule.forRoot([
            {
                path: "execution",
                component: DashboardComponent,
                children: [
                    {path: "summary", component: ExecutionSummaryComponent},
                    {path: "summary/:executionId", component: ExecutionSummaryComponent}
                ]
            },
            {
                path: "scenariosDetails/:executionId",
                component: ScenariosDetailsParentComponent,
                children: [
                    {path: ":scenarioId", component: ScenarioDetailsComponent}
                ]
            },
            {path: "comparison", component: ComparisonComponent},
            {path: "trends/execution", component: ExecutionTrendsComponent},
            {path: "", redirectTo: "execution/summary", pathMatch: "full"},
            {path: "**", component: DashboardComponent},
        ]),
        ChartModule,
        ModalModule.forRoot(),
        BootstrapModalModule,
        SelectModule,
        CoolHttpModule,
        CoolLoadingIndicatorModule
    ],
    declarations: [
        AppComponent,
        ExecutionSummaryComponent,
        DashboardComponent,
        ScenarioDetailsComponent,
        ScenariosDetailsParentComponent,
        ComparisonComponent,
        LineChartComponent,
        ColumnChartComponent,
        ExecutionTrendsComponent,
        AutoCompleteComponent
    ],
    bootstrap: [AppComponent],
    providers: [
        ExecutionService, ScenarioService, ChartService, GeneralService,
        AutoCompleteService, TagsAutoCompleteProvider, ExecutionsAutoCompleteProvider, ScenariosAutoCompleteProvider
    ]
})
export class AppModule {
}