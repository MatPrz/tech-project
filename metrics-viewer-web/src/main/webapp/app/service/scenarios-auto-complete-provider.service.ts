import {Injectable} from "@angular/core";
import {Observable} from "rxjs/Observable";
import {AutoCompleteItem} from "../model/shared/auto-complete-item";
import {AutoCompleteProvider} from "./auto-complete-provider.service";
import {AutoCompleteService} from "./auto-complete.service";

@Injectable()
export class ScenariosAutoCompleteProvider implements AutoCompleteProvider {
    constructor(private autoCompleteService: AutoCompleteService) {
    }

    public getItems(searchString: string): Observable<AutoCompleteItem[]> {
        return this.autoCompleteService.getScenarios(searchString);
    }
}