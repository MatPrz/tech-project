import {Injectable} from "@angular/core";
import {CoolHttp} from "angular2-cool-http/cool-http.service";
import {Observable} from "rxjs/Observable";
import {ScenarioDetails} from "../model/scenario/scenario-details";
import "rxjs/Rx";
import {ScenarioInfo} from "../model/scenario/scenario-info";
import {HttpService} from "./http.service";

@Injectable()
export class ScenarioService extends HttpService {
    constructor(http: CoolHttp) {
        super(http);
    }

    public getScenarioDetails(scenarioId: number): Observable<ScenarioDetails> {
        return super.doGet<ScenarioDetails>("/rest/scenarios/details/" + scenarioId);
    }

    public getScenariosInfoForExecution(executionId: number): Observable<ScenarioInfo[]> {
        return super.doGet<ScenarioInfo[]>("/rest/scenarios/infoForExecution/" + executionId);
    }
}