import {Observable} from "rxjs/Observable";
import {AutoCompleteItem} from "../model/shared/auto-complete-item";

export interface AutoCompleteProvider {
    getItems(searchString: string): Observable<AutoCompleteItem[]>;
}