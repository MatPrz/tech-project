import {Injectable} from "@angular/core";
import {Observable} from "rxjs/Observable";
import {HttpService} from "./http.service";
import {AutoCompleteItem} from "../model/shared/auto-complete-item";
import {CoolHttp} from "angular2-cool-http/cool-http.service";

@Injectable()
export class AutoCompleteService extends HttpService {
    constructor(http: CoolHttp) {
        super(http);
    }

    public getTags(searchString: string): Observable<AutoCompleteItem[]> {
        return super.doGet<AutoCompleteItem[]>("/rest/autocomplete/tags/" + searchString);
    }

    public getExecutions(searchString: string): Observable<AutoCompleteItem[]> {
        return super.doGet<AutoCompleteItem[]>("/rest/autocomplete/executions/" + searchString);
    }

    public getScenarios(searchString: string): Observable<AutoCompleteItem[]> {
        return super.doGet<AutoCompleteItem[]>("/rest/autocomplete/scenarios/" + searchString);
    }
}