import {Response, Headers, RequestOptions} from "@angular/http";
import {Observable} from "rxjs/Observable";
import "rxjs/Rx";
import {CoolHttp} from "angular2-cool-http/cool-http.service";

export abstract class HttpService {
    constructor(private http: CoolHttp) {
    }

    protected doGet<T>(path: string): Observable<T> {
        return Observable.fromPromise(this.http.getAsync(path))
            .map(this.extractData)
            .catch(this.handleError);
    }

    protected doPost<T>(path: string, body: any): Observable<T> {
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({headers: headers});
        return Observable.fromPromise(this.http.postAsync(path, body, options))
            .map(this.extractData)
            .catch(this.handleError);
    }

    private extractData(res: Response) {
        return res || {};
    }

    private handleError(error: any) {
        return Observable.throw(error.message || "Unknown error has occured...");
    }
}