import {Injectable} from "@angular/core";
import {CoolHttp} from "angular2-cool-http/cool-http.service";
import {Observable} from "rxjs/Observable";
import "rxjs/Rx";
import {ExecutionSummary} from "../model/execution/execution-summary";
import {ExecutionInfo} from "../model/execution/execution-info";
import {HttpService} from "./http.service";
import {Page} from "../model/shared/page";

@Injectable()
export class ExecutionService extends HttpService {
    constructor(http: CoolHttp) {
        super(http);
    }

    public getRecentExecutionSummary(): Observable<ExecutionSummary> {
        return super.doGet<ExecutionSummary>("/rest/executions/summary");
    }

    public getExecutionSummary(executionId): Observable<ExecutionSummary> {
        return super.doGet<ExecutionSummary>("/rest/executions/summary/" + executionId);
    }

    public getAllExecutionsInfo(pageNumber: number, pageSize: number): Observable<Page<ExecutionInfo>> {
        return super.doGet<Page<ExecutionInfo>>("/rest/executions/infoAll?page=" + pageNumber + "&size=" + pageSize);
    }
}