import {Injectable} from "@angular/core";
import {CoolHttp} from "angular2-cool-http/cool-http.service";
import {Observable} from "rxjs/Observable";
import "rxjs/Rx";
import {HttpService} from "./http.service";
import {ComparisonFilter} from "../model/shared/comparison-filter";
import {ChartsResult} from "../model/chart/charts-result";

@Injectable()
export class ChartService extends HttpService {
    constructor(http: CoolHttp) {
        super(http);
    }

    public getByComparisonFilter(comparisonFilter: ComparisonFilter): Observable<ChartsResult> {
        return super.doPost<ChartsResult>("/rest/charts/byComparisonFilter", comparisonFilter);
    }

    public getExecutionTrends(historySize: number): Observable<ChartsResult> {
        return super.doGet<ChartsResult>("/rest/charts/executionTrends/" + historySize);
    }
}