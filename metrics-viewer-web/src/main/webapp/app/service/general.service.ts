import {Injectable} from "@angular/core";
import {Observable} from "rxjs/Observable";
import "rxjs/Rx";
import {Subject} from "rxjs/Subject";

@Injectable()
export class GeneralService {

    errorSubject: Subject<string> = new Subject<string>();

    publishError(error: string): void {
        this.errorSubject.next(error);
    }

    getErrorObservable(): Observable<string> {
        return this.errorSubject.asObservable();
    }
}