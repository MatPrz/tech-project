import {Component, Input, OnChanges} from "@angular/core";
import {ChartSeries} from "../model/chart/chart-series";
import {ColumnChart} from "../model/chart/column-chart";

@Component({
    selector: "column-chart",
    template: `<chart [options]="options"></chart>`
})
export class ColumnChartComponent implements OnChanges {
    @Input()
    private chart: ColumnChart;
    private options: Object;

    ngOnChanges(): void {
        if (this.chart) {
            this.constructChartOptions();
        }
    }

    private constructChartOptions(): void {
        var titleText;
        if (this.chart.unit) {
            titleText = this.chart.name + " (" + this.chart.unit + ")";
        } else {
            titleText = this.chart.name;
        }
        let seriesList = [];
        this.chart.seriesList.forEach(scenario => seriesList.push(this.constructSeriesObject(scenario)));
        this.options = {
            chart: {
                type: 'column'
            },
            title: {text: titleText},
            subtitle: {text: this.chart.description},
            series: seriesList,
            xAxis: {
                title: {
                    text: "Executions"
                },
                categories: this.chart.categories,
            },
            yAxis: {
                title: {
                    text: null
                }
            }
        };
    }

    private constructSeriesObject(chartSeries: ChartSeries): Object {
        let data = [];
        chartSeries.data.forEach(seriesData => {
            data.push([seriesData.label, seriesData.value]);
        });
        return {
            data: data,
            name: chartSeries.name
        };
    }
}

