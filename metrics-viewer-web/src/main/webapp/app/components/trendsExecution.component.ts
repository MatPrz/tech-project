import {Component, OnInit} from "@angular/core";
import {ChartService} from "../service/chart.service";
import {ChartsResult} from "../model/chart/charts-result";
import {GeneralService} from "../service/general.service";

@Component({
    selector: "comparison",
    templateUrl: "/app/views/trendsExecution.component.html"
})
export class ExecutionTrendsComponent implements OnInit{
    chartsResult: ChartsResult = new ChartsResult();

    constructor(private chartService: ChartService, private errorService: GeneralService) {
    }

    ngOnInit(): void {
        this.loadExecutionTrends();
    }

    // TODO extract history size to UI slider
    loadExecutionTrends(): void {
        this.chartService.getExecutionTrends(20).subscribe(chartsResult => {
            this.chartsResult = chartsResult;
        }, errorMsg => {
            this.errorService.publishError(errorMsg);
        });
    }
}

