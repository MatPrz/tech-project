import {Component, OnInit, OnDestroy} from "@angular/core";
import {ScenarioService} from "../service/scenario.service";
import {ScenarioInfo} from "../model/scenario/scenario-info";
import {ActivatedRoute} from "@angular/router";
import {AnonymousSubscription} from "rxjs/Subscription";

@Component({
    selector: "dashboard",
    templateUrl: "/app/views/scenariosDetailsParent.component.html"
})
export class ScenariosDetailsParentComponent implements OnInit, OnDestroy {
    private static EXECUTION_ID_KEY: string = "executionId";

    private scenariosInfo: ScenarioInfo[];
    private executionId: number;

    private routeParamsSubscription: AnonymousSubscription;
    private scenarioServiceSubscription: AnonymousSubscription;


    constructor(private route: ActivatedRoute, private scenarioService: ScenarioService) {
    }

    ngOnInit(): void {
        this.routeParamsSubscription = this.route.params.subscribe(params => {
            this.executionId = +params[ScenariosDetailsParentComponent.EXECUTION_ID_KEY];
            this.getScenariosInfoForExecution(this.executionId);
        });
    }

    ngOnDestroy(): void {
        this.routeParamsSubscription.unsubscribe();
        if (this.scenarioServiceSubscription) {
            this.scenarioServiceSubscription.unsubscribe();
        }
    }

    private getScenariosInfoForExecution(executionId: number): void {
        this.scenarioServiceSubscription = this.scenarioService.getScenariosInfoForExecution(executionId).subscribe(
            scenariosInfo => {
                this.scenariosInfo = scenariosInfo;
            }
        );
    }
}

