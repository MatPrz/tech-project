import {Component, Input, OnChanges} from "@angular/core";
import {ChartSeries} from "../model/chart/chart-series";
import {LineChart} from "../model/chart/line-chart";

@Component({
    selector: "line-chart",
    template: `<chart [options]="options"></chart>`
})
export class LineChartComponent implements OnChanges {
    @Input()
    private chart: LineChart;
    @Input()
    private showLegend: boolean = true;
    private options: Object;

    ngOnChanges(): void {
        if (this.chart) {
            this.constructChartOptions();
        }
    }

    private constructChartOptions(): void {
        var titleText;
        if (this.chart.unit) {
            titleText = this.chart.name + " (" + this.chart.unit + ")";
        } else {
            titleText = this.chart.name;
        }
        let seriesList = [];
        this.chart.seriesList.forEach(metric => seriesList.push(this.constructSeriesObject(metric)));
        this.options = {
            title: {text: titleText},
            subtitle: {text: this.chart.description},
            legend: {enabled: this.showLegend},
            series: seriesList,
            xAxis: {
                title: {
                    text: "Execution time (ms)"
                },
                type: this.chart.xAxisType,
                visible: this.chart.xAxisVisible
            },
            yAxis: {
                title: {
                    text: null
                }
            }
        };
    }

    private constructSeriesObject(chartSeries: ChartSeries): Object {
        let data = [];
        chartSeries.data.forEach(seriesData => {
            data.push([seriesData.label, seriesData.value]);
        });
        return {
            data: data,
            name: chartSeries.name
        };
    }
}

