import {Component, OnInit, OnDestroy} from "@angular/core";
import {ExecutionSummary} from "../model/execution/execution-summary";
import {ExecutionService} from "../service/execution.service";
import {AnonymousSubscription} from "rxjs/Subscription";
import {ActivatedRoute} from "@angular/router";

@Component({
    selector: "execution-summary",
    templateUrl: "/app/views/executionSummary.component.html"
})
export class ExecutionSummaryComponent implements OnInit, OnDestroy {
    private static EXECUTION_ID_KEY: string = "executionId";
    private executionSummary: ExecutionSummary;
    private routeParamsSubscription: AnonymousSubscription;
    private executionServiceSubscription: AnonymousSubscription;

    constructor(private route: ActivatedRoute, private executionService: ExecutionService) {
    }

    ngOnInit(): void {
        this.routeParamsSubscription = this.route.params.subscribe(params => {
            let executionId = +params[ExecutionSummaryComponent.EXECUTION_ID_KEY];
            if (!executionId) {
                this.getRecentExecutionSummary();
            } else {
                this.getExecutionSummaryForExecutionId(executionId);
            }
        });
    }

    ngOnDestroy(): void {
        this.routeParamsSubscription.unsubscribe();
        if (this.executionServiceSubscription) {
            this.executionServiceSubscription.unsubscribe();
        }
    }

    getScenarioSummaryHeadingId(index: number): string {
        return "scenarioSummaryHeading" + index.toString();
    }

    getScenarioSummaryContentId(index: number): string {
        return "scenarioSummaryCollapse" + index.toString();
    }

    private getRecentExecutionSummary(): void {
        this.executionServiceSubscription = this.executionService.getRecentExecutionSummary().subscribe(
            executionSummary => {
                this.executionSummary = executionSummary;
            }
        );
    }

    private getExecutionSummaryForExecutionId(executionId: number): void {
        this.executionServiceSubscription = this.executionService.getExecutionSummary(executionId).subscribe(
            executionSummary => {
                this.executionSummary = executionSummary;
            }
        );
    }
}

