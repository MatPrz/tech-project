import {Component, ViewChild, Input, Output, EventEmitter} from "@angular/core";
import {SelectItem, SelectComponent} from "ng2-select";
import {AutoCompleteItem} from "../model/shared/auto-complete-item";
import {AutoCompleteProvider} from "../service/auto-complete-provider.service";

@Component({
    selector: "auto-complete",
    template: `<ng-select #input 
                    [multiple]="multiple"
                    [allowClear]="true"
                    (data)="refreshValues($event)"
                    (typed)="searchItems($event)"
                    placeholder="No value selected">
    </ng-select>`
})
export class AutoCompleteComponent {
    @ViewChild('input')
    private input: SelectComponent;
    private searchString: string = null;
    @Input()
    private multiple: boolean = false;
    @Input()
    private selectedData: string[] = [];
    @Output()
    private selectedDataChange: EventEmitter<string[]> = new EventEmitter<string[]>();
    @Input()
    private autoCompleteProvider: AutoCompleteProvider;

    private refreshValues(selectedItems: SelectItem[]): void {
        this.selectedData = AutoCompleteComponent.getItemIds(selectedItems);
        this.selectedDataChange.emit(this.selectedData);
    }

    private searchItems(searchString: string): void {
        if (searchString === this.searchString) {
            this.input.items = [];
            this.input.open();
            this.searchString = '';
            return
        }
        this.searchString = searchString;
        // TODO - flatMap use
        this.autoCompleteProvider.getItems(searchString)
            .map(items => items.map(AutoCompleteComponent.mapToSelectItem))
            .subscribe((results: SelectItem[]) => {
                this.input.items = results;
                this.input.open();
            });
    }

    private static getItemIds(value: SelectItem[]): string[] {
        return value.map(item => item.id);
    }

    private static mapToSelectItem(autoCompleteItem: AutoCompleteItem): SelectItem {
        return new SelectItem(autoCompleteItem);
    }
}

