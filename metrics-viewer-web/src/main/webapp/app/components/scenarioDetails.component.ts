import {Component, OnInit, OnDestroy} from "@angular/core";
import {ActivatedRoute} from "@angular/router";
import {ScenarioService} from "../service/scenario.service";
import {AnonymousSubscription} from "rxjs/Subscription";
import {ScenarioDetails} from "../model/scenario/scenario-details";

@Component({
    selector: "dashboard",
    templateUrl: "/app/views/scenarioDetails.component.html"
})
export class ScenarioDetailsComponent implements OnInit, OnDestroy {
    private static SCENARIO_ID_KEY: string = "scenarioId";

    private scenarioDetails: ScenarioDetails;

    private routeParamsSubscription: AnonymousSubscription;
    private scenarioServiceSubscription: AnonymousSubscription;

    constructor(private route: ActivatedRoute, private scenarioService: ScenarioService) {
    }

    ngOnInit(): void {
        this.routeParamsSubscription = this.route.params.subscribe(params => {
            let scenarioId = +params[ScenarioDetailsComponent.SCENARIO_ID_KEY];
            this.getScenarioDetailsForScenarioId(scenarioId);
        });
    }

    ngOnDestroy(): void {
        this.routeParamsSubscription.unsubscribe();
        if (this.scenarioServiceSubscription) {
            this.scenarioServiceSubscription.unsubscribe();
        }
    }

    private getScenarioDetailsForScenarioId(scenarioId: number): void {
        this.scenarioServiceSubscription = this.scenarioService.getScenarioDetails(scenarioId).subscribe(
            scenarioDetails => {
                this.scenarioDetails = scenarioDetails;
            }
        );
    }
}

