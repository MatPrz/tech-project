import {Component} from "@angular/core";
import {ChartService} from "../service/chart.service";
import {ChartsResult} from "../model/chart/charts-result";
import {ComparisonFilter} from "../model/shared/comparison-filter";
import {GeneralService} from "../service/general.service";
import {TagsAutoCompleteProvider} from "../service/tags-auto-complete-provider.service";
import {ExecutionsAutoCompleteProvider} from "../service/executions-auto-complete-provider.service";
import {ScenariosAutoCompleteProvider} from "../service/scenarios-auto-complete-provider.service";

@Component({
    selector: "comparison",
    templateUrl: "/app/views/comparison.component.html"
})
export class ComparisonComponent {
    chartsResult: ChartsResult = new ChartsResult();
    comparisonFilter: ComparisonFilter;

    constructor(private chartService: ChartService,
                private tagsAutoCompleteProvider: TagsAutoCompleteProvider,
                private executionsAutoCompleteProvider: ExecutionsAutoCompleteProvider,
                private scenariosAutoCompleteProvider: ScenariosAutoCompleteProvider,
                private errorService: GeneralService) {
        this.comparisonFilter = new ComparisonFilter();
    }

    loadCharts(): void {
        this.chartService.getByComparisonFilter(this.comparisonFilter).subscribe(chartsResult => {
            this.chartsResult = chartsResult;
        }, errorMsg => {
            this.errorService.publishError(errorMsg);
        });
    }
}

