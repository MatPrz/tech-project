import {Component, OnInit, OnDestroy, ViewContainerRef} from "@angular/core";
import {GeneralService} from "../service/general.service";
import {AnonymousSubscription} from "rxjs/Subscription";
import {Overlay} from "angular2-modal";
import {Modal} from "angular2-modal/plugins/bootstrap";

@Component({
    selector: "my-app",
    templateUrl: "app/views/app.component.html",
    providers: [Modal]
})
export class AppComponent implements OnInit, OnDestroy {
    private subscription: AnonymousSubscription;

    constructor(private errorService: GeneralService, overlay: Overlay, vcRef: ViewContainerRef, public modal: Modal) {
        overlay.defaultViewContainer = vcRef;
    }

    ngOnInit(): void {
        this.subscription = this.errorService.getErrorObservable().subscribe(error => {
            this.showErrorModal(error);
        });
    }

    ngOnDestroy(): void {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }

    showErrorModal(error: string): void {
        this.modal.alert().size('lg').showClose(true)
            .dialogClass('modal-dialog modal-md').okBtnClass('btn btn-danger')
            .title('Error').body(error).open();
    }
}