import {Component, OnInit} from "@angular/core";
import {ExecutionService} from "../service/execution.service";
import {ExecutionInfo} from "../model/execution/execution-info";
import {Page} from "../model/shared/page";

@Component({
    selector: "dashboard",
    templateUrl: "/app/views/dashboard.component.html"
})
export class DashboardComponent implements OnInit {
    executionsInfo: ExecutionInfo[];
    pageNumber: number = 0;
    pageSize: number = 10;
    page: Page<ExecutionInfo>;

    constructor(private executionService: ExecutionService) {
    }

    ngOnInit(): void {
        this.loadPage(0);
    }

    loadPage(pageNumber: number) {
        this.executionService.getAllExecutionsInfo(pageNumber, this.pageSize).subscribe(page => {
            this.executionsInfo = page.content;
            this.pageNumber = page.number;
            this.page = page;
        }, errorMsg => {
            console.log("Could not retrieve executions info");
            this.executionsInfo = null;
        });
    }

    prevPage(): void {
        if (!this.page.first) {
            this.loadPage(this.pageNumber - 1);
        }
    }

    nextPage(): void {
        if (!this.page.last) {
            this.loadPage(this.pageNumber + 1);
        }
    }
}

