import {LineChart} from "../chart/line-chart";

export class ScenarioDetails {
    name: string;
    summary: Object;
    lineCharts: LineChart[];
}