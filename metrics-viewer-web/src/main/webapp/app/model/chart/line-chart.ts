import {ChartSeries} from "./chart-series";

export class LineChart {
    name: string;
    description: string;
    unit: string;
    xAxisType: string;
    xAxisVisible: boolean;
    seriesList: ChartSeries[];
}