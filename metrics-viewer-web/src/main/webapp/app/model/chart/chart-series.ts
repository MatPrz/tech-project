import {SeriesData} from "./series-data";

export class ChartSeries {
    name: string;
    data: SeriesData[];
}