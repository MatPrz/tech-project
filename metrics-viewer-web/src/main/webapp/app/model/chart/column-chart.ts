import {ChartSeries} from "./chart-series";

export class ColumnChart {
    name: string;
    description: string;
    unit: string;
    categories: string[];
    seriesList: ChartSeries[];
}