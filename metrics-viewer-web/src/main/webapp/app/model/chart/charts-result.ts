import {LineChart} from "./line-chart";
import {ColumnChart} from "./column-chart";

export class ChartsResult {
    lineCharts: LineChart[] = [];
    columnCharts: ColumnChart[] = [];
}