import {SummaryItem} from "../shared/summary-item";
import {ScenarioSummary} from "../shared/scenario-summary";

export class ExecutionSummary {
    id: number;
    name: String;
    hostname: String;
    startTimeUtc: number;
    summary: SummaryItem[];
    scenarios: ScenarioSummary[];
}