export class ExecutionInfo {
    id: number;
    name: string;
    startTimeUtc: number;
}