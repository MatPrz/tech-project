export class ComparisonFilter {
    tags: string[];
    executions: string[];
    scenarios: string[];
    timeFrom: string;
    timeTo: string;
}