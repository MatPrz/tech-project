import {SummaryItem} from "./summary-item";

export class ScenarioSummary {
    name: string;
    hostname: string;
    summary: SummaryItem[];
    tags: string[];
}