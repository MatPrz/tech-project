export class SummaryItem {
    key: string;
    value: number;
    unit: string;
}