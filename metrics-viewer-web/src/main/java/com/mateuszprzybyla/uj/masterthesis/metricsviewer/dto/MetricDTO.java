package com.mateuszprzybyla.uj.masterthesis.metricsviewer.dto;

import com.mateuszprzybyla.uj.masterthesis.metrics.model.Metric;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "METRIC")
public class MetricDTO {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private int id;

    @Column(name = "KEY")
    private Metric key;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    @OrderBy("time")
    private List<MetricDataDTO> metricData;

    public MetricDTO() {
    }

    public MetricDTO(Metric key, List<MetricDataDTO> metricData) {
        this.key = key;
        this.metricData = metricData;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Metric getKey() {
        return key;
    }

    public void setKey(Metric key) {
        this.key = key;
    }

    public List<MetricDataDTO> getMetricData() {
        return metricData;
    }

    public void setMetricData(List<MetricDataDTO> metricData) {
        this.metricData = metricData;
    }
}
