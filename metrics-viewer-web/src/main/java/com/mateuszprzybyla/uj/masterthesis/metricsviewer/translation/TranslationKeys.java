package com.mateuszprzybyla.uj.masterthesis.metricsviewer.translation;

public enum TranslationKeys {
    IDENTITY_FORMAT("%s"),
    METRIC_NAME_FORMAT("metric.%s.name"),
    METRIC_DESCRIPTION_FORMAT("metric.%s.description"),

    SCENARIO_SUMMARY_AGGREGATE_FORMAT("scenario.summary.aggregate.format"),
    SCENARIO_SUMMARY_ONLY_FORMAT("scenario.summary.only.format");

    private String keyFormat;

    TranslationKeys(String keyFormat) {
        this.keyFormat = keyFormat;
    }

    public String getKeyFormat() {
        return keyFormat;
    }
}
