package com.mateuszprzybyla.uj.masterthesis.metricsviewer.service;

import com.mateuszprzybyla.uj.masterthesis.metrics.model.ui.chart.ChartSeries;
import com.mateuszprzybyla.uj.masterthesis.metrics.model.ui.chart.ColumnChart;
import com.mateuszprzybyla.uj.masterthesis.metrics.model.ui.chart.SeriesData;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

class ColumnChartBuilder<T, U extends Comparable<? super U>> {
    private String name;
    private String description;
    private String unit;

    private Set<T> categoryObjects;
    private Function<T, U> orderKeyExtractor;
    private Function<T, String> categoryNameFunction;
    private Map<String, Map<T, Double>> valuesBySeriesNameAndCategoryObject = new HashMap<>();


    ColumnChartBuilder(Function<T, U> orderKeyExtractor, Function<T, String> categoryNameFunction) {
        this.orderKeyExtractor = orderKeyExtractor;
        this.categoryNameFunction = categoryNameFunction;
        this.categoryObjects = new TreeSet<>(Comparator.comparing(orderKeyExtractor));
    }

    ColumnChartBuilder<T, U> setName(String name) {
        this.name = name;
        return this;
    }

    ColumnChartBuilder<T, U> setDescription(String description) {
        this.description = description;
        return this;
    }

    ColumnChartBuilder<T, U> setUnit(String unit) {
        this.unit = unit;
        return this;
    }

    ColumnChartBuilder addData(double value, String seriesName, T categoryObject) {
        this.categoryObjects.add(categoryObject);
        this.valuesBySeriesNameAndCategoryObject.putIfAbsent(seriesName, new HashMap<>());
        this.valuesBySeriesNameAndCategoryObject.get(seriesName).put(categoryObject, value);
        return this;
    }

    ColumnChart build() {
        ColumnChart columnChart = new ColumnChart();
        columnChart.setName(name);
        columnChart.setDescription(description);
        columnChart.setUnit(unit);
        columnChart.setCategories(generateCategories());
        columnChart.setSeriesList(generateSeriesList());
        return columnChart;
    }

    private List<String> generateCategories() {
        return this.categoryObjects.stream().map(this.categoryNameFunction).collect(Collectors.toList());
    }

    private List<ChartSeries> generateSeriesList() {
        List<ChartSeries> seriesList = new ArrayList<>(valuesBySeriesNameAndCategoryObject.size());
        for (Map.Entry<String, Map<T, Double>> entry : valuesBySeriesNameAndCategoryObject.entrySet()) {
            ChartSeries chartSeries = new ChartSeries();
            chartSeries.setName(entry.getKey());
            chartSeries.setData(new ArrayList<>(categoryObjects.size()));
            for (T categoryObject : categoryObjects) {
                Double value = entry.getValue().getOrDefault(categoryObject, 0.);
                chartSeries.getData().add(new SeriesData(categoryNameFunction.apply(categoryObject), value));
            }
            seriesList.add(chartSeries);
        }
        return seriesList;
    }
}
