package com.mateuszprzybyla.uj.masterthesis.metricsviewer.service;

import com.mateuszprzybyla.uj.masterthesis.metricsviewer.dto.ScenarioDTO;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Join;
import java.time.LocalDateTime;
import java.util.List;

class ScenarioSpecification {
    static Specification<ScenarioDTO> isBeforeDate(LocalDateTime endDate) {
        return (root, query, cb) -> cb.lessThan(root.get("execution").get("startTimeUtc"), endDate);
    }

    static Specification<ScenarioDTO> isAfterDate(LocalDateTime startDate) {
        return (root, query, cb) -> cb.greaterThan(root.get("execution").get("startTimeUtc"), startDate);
    }

    static Specification<ScenarioDTO> isBetweenDates(LocalDateTime startDate, LocalDateTime endDate) {
        return (root, query, cb) -> cb.between(root.get("execution").get("startTimeUtc"), startDate, endDate);
    }

    static Specification<ScenarioDTO> belongsToOneOfExecutions(List<Integer> executionIds) {
        return (root, query, cb) -> root.get("execution").get("id").in(executionIds);
    }

    static Specification<ScenarioDTO> containsOneOfTags(List<String> tags) {
        return (root, query, cb) -> {
            Join<Object, Object> tagsJoin = root.join("tags");
            return tagsJoin.get("name").in(tags);
        };
    }

    static Specification<ScenarioDTO> matchesOneOfNames(List<String> names) {
        return (root, query, cb) -> root.get("name").in(names);
    }
}
