package com.mateuszprzybyla.uj.masterthesis.metricsviewer.controller;

import com.mateuszprzybyla.uj.masterthesis.metrics.model.input.Execution;
import com.mateuszprzybyla.uj.masterthesis.metrics.model.ui.execution.ExecutionInfo;
import com.mateuszprzybyla.uj.masterthesis.metricsviewer.dto.ExecutionDTO;
import com.mateuszprzybyla.uj.masterthesis.metricsviewer.service.ExecutionService;
import com.mateuszprzybyla.uj.masterthesis.metricsviewer.service.ExecutionStorageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/rest/executions")
public class ExecutionController {
    private static final Logger LOGGER = LoggerFactory.getLogger(ExecutionController.class);

    private final ExecutionService executionService;
    private final ExecutionStorageService executionStorageService;

    @Autowired
    public ExecutionController(ExecutionStorageService executionStorageService, ExecutionService executionService) {
        this.executionStorageService = executionStorageService;
        this.executionService = executionService;
    }

    @RequestMapping(value = "/summary", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getRecentExecutionSummary() {
        LOGGER.debug("Received request to /rest/executions/summary");

        return executionService.getRecentExecutionSummary()
            .map(ResponseEntity::ok)
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @RequestMapping(value = "/summary/{executionId}", method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getExecutionSummary(@PathVariable int executionId) {
        LOGGER.debug("Received request to /rest/executions/summary/{}", executionId);

        return executionService.getExecutionSummary(executionId)
            .map(ResponseEntity::ok)
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @RequestMapping(value = "/infoAll", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Page<ExecutionInfo> getAllExecutionsInfo(Pageable pageable) {
        LOGGER.debug("Received request to /rest/executions/infoAll for page number {} and page size {}",
                pageable.getPageNumber(), pageable.getPageSize());

        return executionService.getAllExecutionInfo(pageable);
    }

    @RequestMapping(value = "/", method = RequestMethod.POST,
        consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public Integer acceptExecutionData(@RequestBody Execution execution) {
        LOGGER.debug("Received POST request to /rest/executions/ with body {}", execution);

        ExecutionDTO executionDTO = executionStorageService.storeExecutionData(execution);
        return executionDTO.getId();
    }

}
