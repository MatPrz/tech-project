package com.mateuszprzybyla.uj.masterthesis.metricsviewer.service;

import com.mateuszprzybyla.uj.masterthesis.metrics.model.ui.scenario.ScenarioDetails;
import com.mateuszprzybyla.uj.masterthesis.metrics.model.ui.scenario.ScenarioInfo;
import com.mateuszprzybyla.uj.masterthesis.metricsviewer.dao.ScenarioDAO;
import com.mateuszprzybyla.uj.masterthesis.metricsviewer.dto.ScenarioDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ScenarioService {
    private static final Logger LOGGER = LoggerFactory.getLogger(ScenarioService.class);

    private final ScenarioDAO scenarioDAO;
    private final ChartService chartService;

    @Autowired
    public ScenarioService(ScenarioDAO scenarioDAO, ChartService chartService) {
        this.scenarioDAO = scenarioDAO;
        this.chartService = chartService;
    }

    public Optional<ScenarioDetails> getDetails(int scenarioId) {
        LOGGER.info("Getting scenario details for scenarioId {}", scenarioId);

        Optional<ScenarioDTO> scenarioDTO = scenarioDAO.findOne(scenarioId);
        return scenarioDTO.map(this::createScenarioDetail);
    }

    public List<ScenarioInfo> getInfoForExecution(int executionId) {
        LOGGER.info("Getting scenarioInfo for execution with id {}", executionId);

        return scenarioDAO.findByExecutionId(executionId)
            .stream()
            .map(this::convertToScenarioInfo)
            .collect(Collectors.toList());
    }

    private ScenarioDetails createScenarioDetail(ScenarioDTO scenarioDTO) {
        LOGGER.debug("Creating scenarioDetails from scenarioDTO for scenarioId {}", scenarioDTO.getId());

        ScenarioDetails scenarioDetails = new ScenarioDetails();
        scenarioDetails.setName(scenarioDTO.getName());
        scenarioDetails.setSummary(Collections.emptyMap());
        scenarioDetails.setLineCharts(chartService.getLineChartsForScenarioDTO(scenarioDTO));
        return scenarioDetails;
    }

    private ScenarioInfo convertToScenarioInfo(ScenarioDTO scenarioDTO) {
        LOGGER.debug("Converting scenarioDTO to scenarioInfo for scenarioId {}", scenarioDTO.getId());

        ScenarioInfo scenarioInfo = new ScenarioInfo();
        scenarioInfo.setId(scenarioDTO.getId());
        scenarioInfo.setName(scenarioDTO.getName());
        return scenarioInfo;
    }
}
