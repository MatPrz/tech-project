package com.mateuszprzybyla.uj.masterthesis.metricsviewer.service;

import com.mateuszprzybyla.uj.masterthesis.metricsviewer.dto.ScenarioDTO;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;

import java.time.LocalDateTime;
import java.util.List;

public class ComparisonFilter {
    private List<String> tags;
    private List<Integer> executions;
    private List<String> scenarios;
    private LocalDateTime timeFrom;
    private LocalDateTime timeTo;

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public LocalDateTime getTimeFrom() {
        return timeFrom;
    }

    public void setTimeFrom(LocalDateTime timeFrom) {
        this.timeFrom = timeFrom;
    }

    public LocalDateTime getTimeTo() {
        return timeTo;
    }

    public void setTimeTo(LocalDateTime timeTo) {
        this.timeTo = timeTo;
    }

    public List<Integer> getExecutions() {
        return executions;
    }

    public void setExecutions(List<Integer> executions) {
        this.executions = executions;
    }

    public List<String> getScenarios() {
        return scenarios;
    }

    public void setScenarios(List<String> scenarios) {
        this.scenarios = scenarios;
    }

    Specifications<ScenarioDTO> toScenarioSpecifications() {
        Specification<ScenarioDTO> executionIdSpecification =
                CollectionUtils.isNotEmpty(executions) ? ScenarioSpecification.belongsToOneOfExecutions(executions) : null;
        Specification<ScenarioDTO> tagsSpecification =
                CollectionUtils.isNotEmpty(tags) ? ScenarioSpecification.containsOneOfTags(tags) : null;
        Specification<ScenarioDTO> nameSpecification =
                CollectionUtils.isNotEmpty(scenarios) ? ScenarioSpecification.matchesOneOfNames(scenarios) : null;
        Specification<ScenarioDTO> timeSpecification = createTimeSpecifications();
        return Specifications.where(executionIdSpecification).and(tagsSpecification)
                .and(nameSpecification).and(timeSpecification);
    }

    private Specification<ScenarioDTO> createTimeSpecifications() {
        if (timeFrom == null && timeTo == null) {
            return null;
        }
        if (timeFrom != null && timeTo != null) {
            return ScenarioSpecification.isBetweenDates(timeFrom, timeTo);
        }
        if (timeFrom != null) {
            return ScenarioSpecification.isAfterDate(timeFrom);
        } else {
            return ScenarioSpecification.isBeforeDate(timeTo);
        }
    }

    @Override
    public String toString() {
        return "ComparisonFilter{" +
                "tags='" + tags + '\'' +
                ", timeFrom=" + timeFrom +
                ", timeTo=" + timeTo +
                ", executions=" + executions +
                ", scenarios='" + scenarios + '\'' +
                '}';
    }
}
