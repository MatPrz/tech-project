package com.mateuszprzybyla.uj.masterthesis.metricsviewer.dao;

import com.mateuszprzybyla.uj.masterthesis.metricsviewer.dto.ScenarioDTO;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ScenarioDAO extends OptionalCrudRepository<ScenarioDTO, Integer>,
        JpaSpecificationExecutor<ScenarioDTO> {
    List<ScenarioDTO> findByExecutionId(int executionId);
}
