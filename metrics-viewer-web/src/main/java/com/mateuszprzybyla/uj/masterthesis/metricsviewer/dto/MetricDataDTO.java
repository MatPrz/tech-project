package com.mateuszprzybyla.uj.masterthesis.metricsviewer.dto;

import javax.persistence.*;

@Entity
@Table(name = "METRIC_DATA")
public class MetricDataDTO {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private int id;

    @Column(name = "TIME")
    private int time;

    @Column(name = "VALUE")
    private double value;

    public MetricDataDTO() {
    }

    public MetricDataDTO(int time, double value) {
        this.time = time;
        this.value = value;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }
}
