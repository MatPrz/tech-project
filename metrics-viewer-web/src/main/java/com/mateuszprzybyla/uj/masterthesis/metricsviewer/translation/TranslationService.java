package com.mateuszprzybyla.uj.masterthesis.metricsviewer.translation;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceResolvable;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.stereotype.Service;

import java.util.Locale;

/*
TODO:
- translation parameters builder
 */
@Service
public class TranslationService {
    private static final Logger LOGGER = LoggerFactory.getLogger(TranslationService.class);

    private final MessageSource messageSource;

    public TranslationService(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    public String getMessage(String key) {
        return getMessageWithDefault(key, StringUtils.EMPTY);
    }

    public String getMessageWithDefault(String key, String defaultValue) {
        return getMessageForKeyFormatWithArgs(TranslationKeys.IDENTITY_FORMAT, key, defaultValue);
    }

    public String getMessageForKeyWithArgs(TranslationKeys key, Object... args) {
        return getMessageForKeyFormatWithArgs(TranslationKeys.IDENTITY_FORMAT, key.getKeyFormat(), StringUtils.EMPTY, args);
    }

    public String getMessageForKeyFormat(TranslationKeys keyFormat, Object keyParam) {
        return getMessageForKeyFormatWithArgs(keyFormat, keyParam, StringUtils.EMPTY);
    }

    public String getMessageForKeyFormatWithArgs(TranslationKeys keyFormat, Object keyParam, Object... args) {
        return getMessageForKeyFormatWithArgs(keyFormat, keyParam, StringUtils.EMPTY, args);
    }

    public String getMessageForKeyFormatWithArgs(TranslationKeys keyFormat, Object keyParam, String defaultValue, Object... valueArgs) {
        String actualKey = String.format(keyFormat.getKeyFormat(), keyParam);
        Locale locale = LocaleContextHolder.getLocale();
        MessageSourceResolvable messageSourceResolvable =
                new DefaultMessageSourceResolvable(new String[]{actualKey}, valueArgs, defaultValue);
        LOGGER.debug("Resolving translation for key {}, args: {} and default value: {}", actualKey, valueArgs, defaultValue);
        try {
            return messageSource.getMessage(messageSourceResolvable, locale);
        } catch (Exception e) {
            LOGGER.warn("Could not resolve translation with key {}, resolving to default value {}", actualKey, defaultValue);
            return defaultValue;
        }
    }
}
