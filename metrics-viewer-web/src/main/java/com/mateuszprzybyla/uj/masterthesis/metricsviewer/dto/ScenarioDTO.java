package com.mateuszprzybyla.uj.masterthesis.metricsviewer.dto;

import javax.persistence.*;
import java.util.List;

/**
 * TODO:
 * - improve persisting scenario tags, reuse names, do not create new for each scenario
 */
@Entity
@Table(name = "SCENARIO")
public class ScenarioDTO {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private int id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "HOSTNAME")
    private String hostname;

    @Column(name = "REFRESH_INTERVAL_MS")
    private int refreshIntervalMs;

    @OneToMany(orphanRemoval = true, cascade = CascadeType.ALL)
    private List<SummaryItemDTO> summary;

    @OneToMany(orphanRemoval = true, cascade = CascadeType.ALL)
    private List<ScenarioTagDTO> tags;

    @OneToMany(orphanRemoval = true, cascade = CascadeType.ALL)
    private List<MetricDTO> metrics;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "EXECUTION_ID")
    private ExecutionDTO execution;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRefreshIntervalMs() {
        return refreshIntervalMs;
    }

    public void setRefreshIntervalMs(int refreshIntervalMs) {
        this.refreshIntervalMs = refreshIntervalMs;
    }

    public List<SummaryItemDTO> getSummary() {
        return summary;
    }

    public void setSummary(List<SummaryItemDTO> summary) {
        this.summary = summary;
    }

    public List<ScenarioTagDTO> getTags() {
        return tags;
    }

    public void setTags(List<ScenarioTagDTO> tags) {
        this.tags = tags;
    }

    public List<MetricDTO> getMetrics() {
        return metrics;
    }

    public void setMetrics(List<MetricDTO> metrics) {
        this.metrics = metrics;
    }

    public ExecutionDTO getExecution() {
        return execution;
    }

    public void setExecution(ExecutionDTO execution) {
        this.execution = execution;
    }

    public String getHostname() {
        return hostname;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }
}
