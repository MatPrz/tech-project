package com.mateuszprzybyla.uj.masterthesis.metricsviewer.service;

import com.mateuszprzybyla.uj.masterthesis.metrics.model.Metric;
import com.mateuszprzybyla.uj.masterthesis.metrics.model.input.Execution;
import com.mateuszprzybyla.uj.masterthesis.metrics.model.input.ExecutionScenario;
import com.mateuszprzybyla.uj.masterthesis.metrics.model.input.MetricData;
import com.mateuszprzybyla.uj.masterthesis.metrics.model.input.MetricSnapshot;
import com.mateuszprzybyla.uj.masterthesis.metricsviewer.dao.ExecutionDAO;
import com.mateuszprzybyla.uj.masterthesis.metricsviewer.dto.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class ExecutionStorageService {
    private static final Logger LOGGER = LoggerFactory.getLogger(ExecutionStorageService.class);

    private final ExecutionDAO executionDAO;

    @Autowired
    public ExecutionStorageService(ExecutionDAO executionDAO) {
        this.executionDAO = executionDAO;
    }

    @Transactional
    public ExecutionDTO storeExecutionData(Execution execution) {
        LOGGER.info("Storing execution data for execution name {}", execution.getName());

        ExecutionDTO executionDTO = convertExecutionToDTO(execution);
        return executionDAO.save(executionDTO);
    }

    private ExecutionDTO convertExecutionToDTO(Execution execution) {
        LOGGER.debug("Converting execution to executionDTO");

        ExecutionDTO executionDTO = new ExecutionDTO();
        executionDTO.setName(execution.getName());
        executionDTO.setHostname(execution.getHostname());
        executionDTO.setStartTimeUtc(execution.getStartTimeUtc());
        executionDTO.setScenarios(convertScenariosToDTO(executionDTO, execution.getScenarios()));
        executionDTO.setSummary(convertSummaryToDTO(execution.getSummary()));
        return executionDTO;
    }

    private List<ScenarioDTO> convertScenariosToDTO(ExecutionDTO executionDTO, List<ExecutionScenario> scenarios) {
        return scenarios.stream()
            .map((scenario) -> convertScenarioToDTO(scenario, executionDTO))
            .collect(Collectors.toList());
    }

    private ScenarioDTO convertScenarioToDTO(ExecutionScenario scenario, ExecutionDTO executionDTO) {
        LOGGER.debug("Converting scenario to scenarioDTO for scenarioName {}", scenario.getName());

        ScenarioDTO scenarioDTO = new ScenarioDTO();
        scenarioDTO.setName(scenario.getName());
        scenarioDTO.setHostname(scenario.getHostname());
        scenarioDTO.setTags(
            scenario.getTags().stream().map(ScenarioTagDTO::new).collect(Collectors.toList()));
        scenarioDTO.setSummary(convertSummaryToDTO(scenario.getSummary()));
        scenarioDTO.setRefreshIntervalMs(scenario.getRefreshIntervalMs());
        scenarioDTO.setMetrics(convertMetricsToDTO(scenario.getMetricDatas()));
        scenarioDTO.setExecution(executionDTO);
        return scenarioDTO;
    }

    private List<MetricDTO> convertMetricsToDTO(List<MetricData> metricDatas) {
        return metricDatas.stream().map(this::convertMetricToDTO).collect(Collectors.toList());
    }

    private MetricDTO convertMetricToDTO(MetricData metricData) {
        LOGGER.debug("Converting metricData to metricDTO for metric key {}", metricData.getKey());

        MetricDTO metricDTO = new MetricDTO();
        metricDTO.setKey(metricData.getKey());
        metricDTO.setMetricData(convertMetricSnapshotsToDTO(metricData.getData()));
        return metricDTO;
    }

    private List<MetricDataDTO> convertMetricSnapshotsToDTO(List<MetricSnapshot> metricSnapshots) {
        return metricSnapshots.stream().map(this::convertMetricSnapshotToDTO).collect(Collectors.toList());
    }

    private MetricDataDTO convertMetricSnapshotToDTO(MetricSnapshot snapshot) {
        LOGGER.debug("Converting metricSnapshot to metricDataDTO for time {}", snapshot.getTime());

        MetricDataDTO metricDataDTO = new MetricDataDTO();
        metricDataDTO.setTime(snapshot.getTime());
        metricDataDTO.setValue(snapshot.getValue());
        return metricDataDTO;
    }

    private List<SummaryItemDTO> convertSummaryToDTO(Map<Metric, Double> summaryMap) {
        return summaryMap.entrySet().stream().map(entry -> createSummaryItemDTO(entry.getKey(), entry.getValue()))
            .collect(Collectors.toList());
    }

    private SummaryItemDTO createSummaryItemDTO(Metric key, Double value) {
        LOGGER.debug("Creating summaryItemDTO from key {}", key);

        SummaryItemDTO summaryItemDTO = new SummaryItemDTO();
        summaryItemDTO.setKey(key);
        summaryItemDTO.setValue(value);
        return summaryItemDTO;
    }
}
