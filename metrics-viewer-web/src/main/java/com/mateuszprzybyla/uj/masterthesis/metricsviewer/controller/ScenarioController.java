package com.mateuszprzybyla.uj.masterthesis.metricsviewer.controller;

import com.mateuszprzybyla.uj.masterthesis.metrics.model.ui.scenario.ScenarioDetails;
import com.mateuszprzybyla.uj.masterthesis.metrics.model.ui.scenario.ScenarioInfo;
import com.mateuszprzybyla.uj.masterthesis.metricsviewer.service.ScenarioService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/rest/scenarios/")
public class ScenarioController {
    private static final Logger LOGGER = LoggerFactory.getLogger(ScenarioController.class);

    private final ScenarioService scenarioService;

    @Autowired
    public ScenarioController(ScenarioService scenarioService) {
        this.scenarioService = scenarioService;
    }

    @RequestMapping(value = "/details/{scenarioId}",
        method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ScenarioDetails> getDetails(@PathVariable int scenarioId) {
        LOGGER.debug("Received request to /rest/scenarios/details/{}", scenarioId);

        return scenarioService.getDetails(scenarioId)
            .map(ResponseEntity::ok)
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @RequestMapping(value = "/infoForExecution/{executionId}",
        method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ScenarioInfo> getInfoForExecution(@PathVariable String executionId) {
        LOGGER.debug("Received request to /rest/scenarios/infoForExecution/{}", executionId);

        return scenarioService.getInfoForExecution(Integer.valueOf(executionId));
    }
}
