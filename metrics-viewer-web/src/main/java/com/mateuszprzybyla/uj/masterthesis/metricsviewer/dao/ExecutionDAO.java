package com.mateuszprzybyla.uj.masterthesis.metricsviewer.dao;

import com.mateuszprzybyla.uj.masterthesis.metricsviewer.dto.ExecutionDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface ExecutionDAO extends OptionalCrudRepository<ExecutionDTO, Integer> {
    List<ExecutionDTO> findFirstByOrderByStartTimeUtcDesc();

    Page<ExecutionDTO> findByOrderByStartTimeUtcDesc(Pageable pageable);

    Page<ExecutionDTO> findAllByOrderByStartTimeUtcDesc(Pageable pageable);

    default Optional<ExecutionDTO> findOneMostRecent() {
        List<ExecutionDTO> mostRecentList = findFirstByOrderByStartTimeUtcDesc();
        if (mostRecentList.isEmpty()) {
            return Optional.empty();
        } else {
            return Optional.of(mostRecentList.get(0));
        }
    }
}
