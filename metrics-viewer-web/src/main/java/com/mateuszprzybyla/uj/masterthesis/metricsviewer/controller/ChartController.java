package com.mateuszprzybyla.uj.masterthesis.metricsviewer.controller;

import com.mateuszprzybyla.uj.masterthesis.metrics.model.ui.chart.ChartsResult;
import com.mateuszprzybyla.uj.masterthesis.metricsviewer.service.ChartService;
import com.mateuszprzybyla.uj.masterthesis.metricsviewer.service.ComparisonFilter;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/rest/charts")
public class ChartController {
    private static final Logger LOGGER = LoggerFactory.getLogger(ChartController.class);

    private final ChartService chartService;

    @Autowired
    public ChartController(ChartService chartService) {
        this.chartService = chartService;
    }

    @RequestMapping(value = "/byComparisonFilter", method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getByFilter(@RequestBody ComparisonFilter comparisonFilter) {
        LOGGER.debug("Received POST request to /rest/charts/byComparisonFilter with body {}", comparisonFilter);

        if (!isComparisonFilterValid(comparisonFilter)) {
            return ResponseEntity.badRequest().body("Please provide at least one filter parameter");
        }

        return ResponseEntity.ok(chartService.getChartsForComparisonFilter(comparisonFilter));
    }

    @RequestMapping(value = "/executionTrends/{historySize}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ChartsResult> getExecutionTrends(@PathVariable int historySize) {
        LOGGER.debug("Received POST request to /rest/charts/executionTrends with historySize {}", historySize);

        return ResponseEntity.ok(chartService.getExecutionTrends(historySize));
    }

    private boolean isComparisonFilterValid(ComparisonFilter comparisonFilter) {
        return CollectionUtils.isNotEmpty(comparisonFilter.getTags())
                || CollectionUtils.isNotEmpty(comparisonFilter.getScenarios())
                || CollectionUtils.isNotEmpty(comparisonFilter.getExecutions())
                || comparisonFilter.getTimeFrom() != null
                || comparisonFilter.getTimeTo() != null;
    }
}
