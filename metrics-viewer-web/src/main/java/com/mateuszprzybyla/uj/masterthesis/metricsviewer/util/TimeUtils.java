package com.mateuszprzybyla.uj.masterthesis.metricsviewer.util;

import java.time.LocalDateTime;
import java.time.ZoneOffset;

public class TimeUtils {
    public static long toEpochMillis(LocalDateTime dateTimeInUtc) {
        return dateTimeInUtc.toInstant(ZoneOffset.UTC).toEpochMilli();
    }
}
