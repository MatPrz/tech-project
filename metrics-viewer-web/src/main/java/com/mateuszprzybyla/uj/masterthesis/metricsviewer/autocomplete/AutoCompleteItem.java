package com.mateuszprzybyla.uj.masterthesis.metricsviewer.autocomplete;

class AutoCompleteItem<T> {
    private T id;
    private String text;

    AutoCompleteItem(T id) {
        this(id, id.toString());
    }

    AutoCompleteItem(T id, String text) {
        this.id = id;
        this.text = text;
    }

    public T getId() {
        return id;
    }

    public String getText() {
        return text;
    }

    public void setId(T id) {
        this.id = id;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AutoCompleteItem<?> that = (AutoCompleteItem<?>) o;

        if (!id.equals(that.id)) return false;
        return text.equals(that.text);

    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + text.hashCode();
        return result;
    }
}
