package com.mateuszprzybyla.uj.masterthesis.metricsviewer.controller;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Controller
public class HomeController {
    private static final Logger LOGGER = LoggerFactory.getLogger(HomeController.class);

    @RequestMapping(value = {"/", "execution/summary", "/execution/summary/*",
            "/scenariosDetails/*/*", "/comparison", "/trends/*"}, method = RequestMethod.GET)
    public String angularRoot(HttpServletRequest httpServletRequest) {
        LOGGER.debug("Received angularRoot request to path {}", httpServletRequest.getContextPath());
        return "index";
    }

    @RequestMapping(path = "/download/{resourceName:.+}", method = RequestMethod.GET)
    public ResponseEntity<Resource> getDownloadableResource(@PathVariable String resourceName) {
        LOGGER.debug("Received /download/{} request to get downloadable request", resourceName);
        try {
            byte[] byteArray = IOUtils.toByteArray(getClass().getResourceAsStream("/downloadable/" + resourceName));
            ByteArrayResource resource = new ByteArrayResource(byteArray);
            return ResponseEntity.ok()
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + resourceName)
                    .contentLength(resource.contentLength())
                    .contentType(MediaType.APPLICATION_OCTET_STREAM)
                    .body(resource);
        } catch (Exception e) {
            LOGGER.warn("Could not provide client package", e);
            return ResponseEntity.notFound().build();
        }
    }
}
