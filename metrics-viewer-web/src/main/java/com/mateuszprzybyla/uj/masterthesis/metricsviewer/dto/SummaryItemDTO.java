package com.mateuszprzybyla.uj.masterthesis.metricsviewer.dto;

import com.mateuszprzybyla.uj.masterthesis.metrics.model.Metric;

import javax.persistence.*;

@Entity
@Table(name = "SUMMARY_ITEM")
public class SummaryItemDTO {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private int id;

    @Column(name = "KEY")
    private Metric key;

    @Column(name = "VALUE")
    private double value;

    public SummaryItemDTO() {
    }

    public SummaryItemDTO(Metric key, double value) {
        this.key = key;
        this.value = value;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Metric getKey() {
        return key;
    }

    public void setKey(Metric key) {
        this.key = key;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }
}
