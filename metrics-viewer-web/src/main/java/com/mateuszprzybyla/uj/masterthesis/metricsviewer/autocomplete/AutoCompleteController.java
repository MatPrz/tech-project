package com.mateuszprzybyla.uj.masterthesis.metricsviewer.autocomplete;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/rest/autocomplete")
class AutoCompleteController {
    private static final Logger LOGGER = LoggerFactory.getLogger(AutoCompleteController.class);

    private final AutoCompleteService autoCompleteService;

    @Autowired
    AutoCompleteController(AutoCompleteService autoCompleteService) {
        this.autoCompleteService = autoCompleteService;
    }

    @RequestMapping(value = "/tags/{searchString}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<List<AutoCompleteItem>> getTags(@PathVariable String searchString) {
        LOGGER.debug("Received request to /rest/autocomplete/tags/{}", searchString);

        if (StringUtils.isBlank(searchString)) {
            LOGGER.warn("Received request with invalid search string parameter {}", searchString);
            return ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(autoCompleteService.getTags(searchString));
    }

    @RequestMapping(value = "/executions/{searchString}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<List<AutoCompleteItem>> getExecutions(@PathVariable String searchString) {
        LOGGER.debug("Received request to /rest/autocomplete/executions/{}", searchString);

        if (StringUtils.isBlank(searchString)) {
            LOGGER.warn("Received request with invalid search string parameter {}", searchString);
            return ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(autoCompleteService.getExecutions(searchString));
    }

    @RequestMapping(value = "/scenarios/{searchString}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<List<AutoCompleteItem>> getScenarios(@PathVariable String searchString) {
        LOGGER.debug("Received request to /rest/autocomplete/scenarios/{}", searchString);

        if (StringUtils.isBlank(searchString)) {
            LOGGER.warn("Received request with invalid search string parameter {}", searchString);
            return ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(autoCompleteService.getScenarios(searchString));
    }
}
