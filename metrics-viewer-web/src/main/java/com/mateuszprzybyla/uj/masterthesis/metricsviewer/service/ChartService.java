package com.mateuszprzybyla.uj.masterthesis.metricsviewer.service;

import com.mateuszprzybyla.uj.masterthesis.metrics.model.Metric;
import com.mateuszprzybyla.uj.masterthesis.metrics.model.ui.chart.*;
import com.mateuszprzybyla.uj.masterthesis.metricsviewer.dao.ExecutionDAO;
import com.mateuszprzybyla.uj.masterthesis.metricsviewer.dao.ScenarioDAO;
import com.mateuszprzybyla.uj.masterthesis.metricsviewer.dto.*;
import com.mateuszprzybyla.uj.masterthesis.metricsviewer.translation.TranslationKeys;
import com.mateuszprzybyla.uj.masterthesis.metricsviewer.translation.TranslationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class ChartService {
    private static final Logger LOGGER = LoggerFactory.getLogger(ChartService.class);

    private final ScenarioDAO scenarioDAO;
    private final ExecutionDAO executionDAO;
    private final TranslationService translations;

    @Autowired
    public ChartService(ScenarioDAO scenarioDAO, ExecutionDAO executionDAO, TranslationService translations) {
        this.scenarioDAO = scenarioDAO;
        this.executionDAO = executionDAO;
        this.translations = translations;
    }

    @Transactional
    public ChartsResult getChartsForComparisonFilter(ComparisonFilter comparisonFilter) {
        LOGGER.info("Getting charts for comparison filter {}", comparisonFilter);

        List<ScenarioDTO> scenarioDTOs = scenarioDAO.findAll(comparisonFilter.toScenarioSpecifications());
        LOGGER.debug("Retrieved {} scenarios matching filter", scenarioDTOs.size());

        Map<Metric, LineChart> lineChartsByMetricName = new EnumMap<>(Metric.class);
        scenarioDTOs.forEach(scenario -> addScenarioSeriesToLineCharts(scenario, lineChartsByMetricName));

        Map<Metric, ColumnChartBuilder<ExecutionDTO, LocalDateTime>> columnChartBuildersByMetricName = new EnumMap<>(Metric.class);
        scenarioDTOs.forEach(scenario -> addScenarioSummariesToColumnCharts(scenario, columnChartBuildersByMetricName));

        ChartsResult chartsResult = new ChartsResult();
        chartsResult.setLineCharts(new LinkedList<>(lineChartsByMetricName.values()));
        List<ColumnChart> columnCharts = columnChartBuildersByMetricName.values().stream()
                .map(ColumnChartBuilder::build)
                .collect(Collectors.toList());
        chartsResult.setColumnCharts(columnCharts);
        return chartsResult;
    }

    @Transactional
    public ChartsResult getExecutionTrends(int historySize) {
        LOGGER.info("Getting execution trends for historySize {}", historySize);

        Page<ExecutionDTO> page = executionDAO.findAllByOrderByStartTimeUtcDesc(new PageRequest(0, historySize));
        List<ExecutionDTO> lastExecutions = new LinkedList<>(page.getContent());
        Collections.reverse(lastExecutions);
        LOGGER.debug("Retrieved {} last executions", lastExecutions.size());

        Map<Metric, LineChart> chartsBySummaryItemName = new EnumMap<>(Metric.class);
        lastExecutions.forEach(execution -> addExecutionSummaryToLineCharts(execution, chartsBySummaryItemName));
        ChartsResult chartsResult = new ChartsResult();
        chartsResult.setLineCharts(new LinkedList<>(chartsBySummaryItemName.values()));
        return chartsResult;
    }

    List<LineChart> getLineChartsForScenarioDTO(ScenarioDTO scenarioDTO) {
        LOGGER.info("Creating charts for scenarioDTO with id {}", scenarioDTO.getId());

        return scenarioDTO.getMetrics()
                .stream()
                .map(this::createLineChartForMetricDTO)
                .collect(Collectors.toList());
    }

    private LineChart createLineChartForMetricDTO(MetricDTO metricDTO) {
        LOGGER.info("Creating chart from metricDTO for metricId {}", metricDTO.getId());

        String seriesName = translations.getMessageForKeyFormatWithArgs(TranslationKeys.METRIC_NAME_FORMAT, metricDTO.getKey());
        ChartSeries metricSeries = getSingleMetricSeries(metricDTO.getMetricData(), seriesName);
        return createNewLineChart(metricDTO.getKey(), "linear", true, Collections.singletonList(metricSeries));
    }

    private void addScenarioSeriesToLineCharts(ScenarioDTO scenario, Map<Metric, LineChart> chartsByMetricName) {
        String scenarioSeriesName = scenario.getExecution().getName() + " - " + scenario.getName();
        scenario.getMetrics().forEach(metricDTO -> {
            Metric metric = metricDTO.getKey();
            if (!chartsByMetricName.containsKey(metric)) {
                LineChart lineChart = createNewLineChart(metric, "linear", true, new ArrayList<>());
                chartsByMetricName.put(metric, lineChart);
            }
            addScenarioMetricToLineChart(metricDTO, chartsByMetricName.get(metric), scenarioSeriesName);
        });
    }

    private void addScenarioMetricToLineChart(MetricDTO metricDTO, LineChart lineChart, String scenarioSeriesName) {
        lineChart.getSeriesList().add(getSingleMetricSeries(metricDTO.getMetricData(), scenarioSeriesName));
    }

    private ChartSeries getSingleMetricSeries(List<MetricDataDTO> metricData, String seriesName) {
        LOGGER.debug("Creating single metric series for metricDataDTO with metricName {}", seriesName);

        ChartSeries chartSeries = new ChartSeries();
        chartSeries.setName(seriesName);
        chartSeries.setData(
                metricData.stream().map(m -> new SeriesData(m.getTime(), m.getValue())).collect(Collectors.toList()));
        return chartSeries;
    }

    private void addScenarioSummariesToColumnCharts(ScenarioDTO scenario, Map<Metric, ColumnChartBuilder<ExecutionDTO, LocalDateTime>> columnChartBuilderMap) {
        scenario.getSummary().forEach(summaryItemDTO -> {
            Metric metric = summaryItemDTO.getKey();
            if (!columnChartBuilderMap.containsKey(metric)) {
                ColumnChartBuilder<ExecutionDTO, LocalDateTime> columnChart = createNewColumnChartBuilder(metric);
                columnChartBuilderMap.put(metric, columnChart);
            }
            columnChartBuilderMap.get(metric).addData(summaryItemDTO.getValue(), scenario.getName(), scenario.getExecution());
        });
    }

    private void addExecutionSummaryToLineCharts(ExecutionDTO executionDTO, Map<Metric, LineChart> chartsBySummaryItemName) {
        executionDTO.getSummary().forEach(summaryItemDTO -> {
            Metric itemName = summaryItemDTO.getKey();
            if (!chartsBySummaryItemName.containsKey(itemName)) {
                LineChart lineChart = createNewExecutionTrendChart(itemName);
                chartsBySummaryItemName.put(itemName, lineChart);
            }
            ChartSeries chartSeries = chartsBySummaryItemName.get(itemName).getSeriesList().iterator().next();
            chartSeries.getData().add(new SeriesData(executionDTO.getStartTimeUtc().toString(), summaryItemDTO.getValue()));
        });
    }


    // TODO - set xAxisType to datetime (http://jsfiddle.net/gh/get/library/pure/highcharts/highcharts/tree/master/samples/highcharts/xaxis/type-datetime-irregular/)
    // TODO - set xAxisName to sth different than Execution time (ms)
    private LineChart createNewExecutionTrendChart(Metric metric) {
        ChartSeries chartSeries = new ChartSeries();
        chartSeries.setName(translations.getMessageForKeyFormat(TranslationKeys.METRIC_NAME_FORMAT, metric));
        chartSeries.setData(new LinkedList<>());
        return createNewLineChart(metric, "category", false, Collections.singletonList(chartSeries));
    }

    // TODO - try creating LineChartBuilder as for ColumnChart usecase
    private LineChart createNewLineChart(Metric metric, String xAxisType, boolean xAxisVisible, List<ChartSeries> seriesList) {
        LineChart lineChart = new LineChart();
        lineChart.setName(translations.getMessageForKeyFormat(TranslationKeys.METRIC_NAME_FORMAT, metric));
        lineChart.setDescription(translations.getMessageForKeyFormat(TranslationKeys.METRIC_DESCRIPTION_FORMAT, metric));
        lineChart.setUnit(metric.getUnit().getText());
        lineChart.setxAxisType(xAxisType);
        lineChart.setxAxisVisible(xAxisVisible);
        lineChart.setSeriesList(seriesList);
        return lineChart;
    }

    private ColumnChartBuilder<ExecutionDTO, LocalDateTime> createNewColumnChartBuilder(Metric metric) {
        return new ColumnChartBuilder<>(ExecutionDTO::getStartTimeUtc, ExecutionDTO::getName)
                .setName(translations.getMessageForKeyFormat(TranslationKeys.METRIC_NAME_FORMAT, metric))
                .setDescription(translations.getMessageForKeyFormat(TranslationKeys.METRIC_DESCRIPTION_FORMAT, metric))
                .setUnit(metric.getUnit().getText());
    }
}
