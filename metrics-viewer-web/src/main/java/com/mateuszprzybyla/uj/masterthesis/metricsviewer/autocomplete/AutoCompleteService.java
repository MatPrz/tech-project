package com.mateuszprzybyla.uj.masterthesis.metricsviewer.autocomplete;

import com.mateuszprzybyla.uj.masterthesis.metricsviewer.dao.ExecutionDAO;
import com.mateuszprzybyla.uj.masterthesis.metricsviewer.dao.ScenarioDAO;
import com.mateuszprzybyla.uj.masterthesis.metricsviewer.dto.ExecutionDTO;
import com.mateuszprzybyla.uj.masterthesis.metricsviewer.dto.ScenarioDTO;
import com.mateuszprzybyla.uj.masterthesis.metricsviewer.dto.ScenarioTagDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.format.DateTimeFormatter;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
class AutoCompleteService {
    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("MMM d, y h:mm:ss a")
            .withLocale(Locale.ENGLISH);
    private final ScenarioDAO scenarioDAO;
    private final ExecutionDAO executionDAO;

    @Autowired
    AutoCompleteService(ScenarioDAO scenarioDAO, ExecutionDAO executionDAO) {
        this.scenarioDAO = scenarioDAO;
        this.executionDAO = executionDAO;
    }

    /*
    TODO - this is really the most lazy way to do it, find a better and more optimal way (caching tag in tag table?)
     */
    List<AutoCompleteItem> getTags(String searchString) {
        return StreamSupport.stream(scenarioDAO.findAll().spliterator(), false)
                .map(ScenarioDTO::getTags)
                .flatMap(List::stream)
                .map(ScenarioTagDTO::getName)
                .distinct()
                .filter(ContainsPredicate.forString(searchString))
                .map(AutoCompleteItem::new)
                .collect(Collectors.toList());
    }

    /*
    TODO - this is really the most lazy way to do it, find a better and more optimal way
     */
    List<AutoCompleteItem> getExecutions(String searchString) {
        return StreamSupport.stream(executionDAO.findAll().spliterator(), false)
                .filter(new ContainsPredicate<>(ExecutionDTO::getName, searchString))
                .sorted(Comparator.comparing(ExecutionDTO::getStartTimeUtc).reversed())
                .map(this::toAutoCompleteItem)
                .collect(Collectors.toList());
    }

    /*
    TODO - this is really the most lazy way to do it, find a better and more optimal way (caching scenario names?)
     */
    List<AutoCompleteItem> getScenarios(String searchString) {
        return StreamSupport.stream(scenarioDAO.findAll().spliterator(), false)
                .map(ScenarioDTO::getName)
                .distinct()
                .filter(ContainsPredicate.forString(searchString))
                .map(AutoCompleteItem::new)
                .collect(Collectors.toList());
    }

    private AutoCompleteItem<Integer> toAutoCompleteItem(ExecutionDTO executionDTO) {
        String startTimeFormatted = executionDTO.getStartTimeUtc().format(DATE_TIME_FORMATTER);
        String itemName = String.format("%s (%s)", executionDTO.getName(), startTimeFormatted);
        return new AutoCompleteItem<>(executionDTO.getId(), itemName);
    }

    private static class ContainsPredicate<T> implements Predicate<T> {
        private final String searchString;
        private Function<T, String> nameExtractor;

        ContainsPredicate(Function<T, String> nameExtractor, String searchString) {
            this.searchString = searchString.toLowerCase();
            this.nameExtractor = nameExtractor;
        }

        @Override
        public boolean test(T object) {
            return nameExtractor.apply(object).toLowerCase().contains(searchString);
        }

        private static ContainsPredicate<String> forString(String searchString) {
            return new ContainsPredicate<>(Function.identity(), searchString);
        }
    }
}
