package com.mateuszprzybyla.uj.masterthesis.metricsviewer.dto;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name = "EXECUTION")
public class ExecutionDTO {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private int id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "START_TIME_UTC")
    private LocalDateTime startTimeUtc;

    @OneToMany(orphanRemoval = true, cascade = CascadeType.ALL)
    private List<SummaryItemDTO> summary;

    @Column(name = "HOSTNAME")
    private String hostname;

    @OneToMany(mappedBy = "execution", orphanRemoval = true, cascade = CascadeType.ALL)
    private List<ScenarioDTO> scenarios;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDateTime getStartTimeUtc() {
        return startTimeUtc;
    }

    public void setStartTimeUtc(LocalDateTime startTimeUtc) {
        this.startTimeUtc = startTimeUtc;
    }

    public List<SummaryItemDTO> getSummary() {
        return summary;
    }

    public void setSummary(List<SummaryItemDTO> summary) {
        this.summary = summary;
    }

    public String getHostname() {
        return hostname;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    public List<ScenarioDTO> getScenarios() {
        return scenarios;
    }

    public void setScenarios(List<ScenarioDTO> scenarios) {
        this.scenarios = scenarios;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ExecutionDTO that = (ExecutionDTO) o;

        if (!name.equals(that.name)) return false;
        return startTimeUtc.equals(that.startTimeUtc);

    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + startTimeUtc.hashCode();
        return result;
    }
}
