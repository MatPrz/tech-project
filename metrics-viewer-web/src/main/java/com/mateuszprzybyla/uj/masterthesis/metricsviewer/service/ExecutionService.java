package com.mateuszprzybyla.uj.masterthesis.metricsviewer.service;

import com.mateuszprzybyla.uj.masterthesis.metrics.model.Metric;
import com.mateuszprzybyla.uj.masterthesis.metrics.model.ui.execution.ExecutionInfo;
import com.mateuszprzybyla.uj.masterthesis.metrics.model.ui.execution.ExecutionSummary;
import com.mateuszprzybyla.uj.masterthesis.metrics.model.ui.shared.ScenarioSummary;
import com.mateuszprzybyla.uj.masterthesis.metrics.model.ui.shared.SummaryItem;
import com.mateuszprzybyla.uj.masterthesis.metricsviewer.dao.ExecutionDAO;
import com.mateuszprzybyla.uj.masterthesis.metricsviewer.dto.ExecutionDTO;
import com.mateuszprzybyla.uj.masterthesis.metricsviewer.dto.ScenarioDTO;
import com.mateuszprzybyla.uj.masterthesis.metricsviewer.dto.ScenarioTagDTO;
import com.mateuszprzybyla.uj.masterthesis.metricsviewer.dto.SummaryItemDTO;
import com.mateuszprzybyla.uj.masterthesis.metricsviewer.translation.TranslationKeys;
import com.mateuszprzybyla.uj.masterthesis.metricsviewer.translation.TranslationService;
import com.mateuszprzybyla.uj.masterthesis.metricsviewer.util.TimeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ExecutionService {
    private static final Logger LOGGER = LoggerFactory.getLogger(ExecutionService.class);

    private final ExecutionDAO executionDAO;
    private final TranslationService translationService;

    @Autowired
    public ExecutionService(ExecutionDAO executionDAO, TranslationService translationService) {
        this.executionDAO = executionDAO;
        this.translationService = translationService;
    }

    public Optional<ExecutionSummary> getRecentExecutionSummary() {
        LOGGER.info("Getting recent execution summary");

        Optional<ExecutionDTO> executionDTO = executionDAO.findOneMostRecent();
        return executionDTO.map(this::convertToExecutionSummary);
    }

    public Optional<ExecutionSummary> getExecutionSummary(int executionId) {
        LOGGER.info("Getting execution summary for id {}", executionId);
        Optional<ExecutionDTO> executionDTO = executionDAO.findOne(executionId);
        return executionDTO.map(this::convertToExecutionSummary);
    }

    public Page<ExecutionInfo> getAllExecutionInfo(Pageable pageable) {
        LOGGER.info("Getting all execution info");

        return executionDAO.findByOrderByStartTimeUtcDesc(pageable)
                .map(this::createExecutionInfo);
    }

    private ExecutionSummary convertToExecutionSummary(ExecutionDTO executionDTO) {
        LOGGER.debug("Converting executionDTO to executionSummary for id {}", executionDTO.getId());

        ExecutionSummary executionSummary = new ExecutionSummary();
        executionSummary.setId(executionDTO.getId());
        executionSummary.setName(executionDTO.getName());
        executionSummary.setHostname(executionDTO.getHostname());
        executionSummary.setStartTimeUtc(TimeUtils.toEpochMillis(executionDTO.getStartTimeUtc()));
        executionSummary.setSummary(convertSummaryItems(executionDTO.getSummary()));
        executionSummary.setScenarios(convertScenarioSummaries(executionDTO.getScenarios()));
        return executionSummary;
    }

    private List<SummaryItem> convertSummaryItems(List<SummaryItemDTO> summaryItemDTOs) {
        return summaryItemDTOs.stream().map(this::convertSummaryItem).collect(Collectors.toList());
    }

    private List<ScenarioSummary> convertScenarioSummaries(List<ScenarioDTO> scenarioDTOs) {
        return scenarioDTOs.stream().map(this::convertScenarioSummary).collect(Collectors.toList());
    }

    private ScenarioSummary convertScenarioSummary(ScenarioDTO scenarioDTO) {
        LOGGER.debug("Converting scenarionDTO to scenarioSummary for scenarioId {}", scenarioDTO.getId());

        ScenarioSummary scenarioSummary = new ScenarioSummary();
        scenarioSummary.setId(scenarioDTO.getId());
        scenarioSummary.setName(scenarioDTO.getName());
        scenarioSummary.setHostname(scenarioDTO.getHostname());
        scenarioSummary.setTags(
                scenarioDTO.getTags().stream().map(ScenarioTagDTO::getName).collect(Collectors.toList()));
        scenarioSummary.setSummary(convertSummaryItems(scenarioDTO.getSummary()));
        return scenarioSummary;
    }

    private SummaryItem convertSummaryItem(SummaryItemDTO summaryItemDTO) {
        Metric metric = summaryItemDTO.getKey();
        LOGGER.debug("Converting summaryItemDTO to summaryItem for summaryKey {}", metric);

        SummaryItem summaryItem = new SummaryItem();
        String metricTranslation = translationService.getMessageForKeyFormat(TranslationKeys.METRIC_NAME_FORMAT, metric);
        summaryItem.setKey(translationService.getMessageForKeyWithArgs(
                metric.isSummaryOnly() ?
                        TranslationKeys.SCENARIO_SUMMARY_ONLY_FORMAT :
                        TranslationKeys.SCENARIO_SUMMARY_AGGREGATE_FORMAT,
                metricTranslation));
        summaryItem.setValue(summaryItemDTO.getValue());
        summaryItem.setUnit(metric.getUnit().getText());
        return summaryItem;
    }

    private ExecutionInfo createExecutionInfo(ExecutionDTO executionDTO) {
        LOGGER.debug("Converting executionDTO to executionInfo for executionId {}", executionDTO.getId());

        ExecutionInfo executionInfo = new ExecutionInfo();
        executionInfo.setId(executionDTO.getId());
        executionInfo.setName(executionDTO.getName());
        executionInfo.setStartTimeUtc(TimeUtils.toEpochMillis(executionDTO.getStartTimeUtc()));
        return executionInfo;
    }
}
