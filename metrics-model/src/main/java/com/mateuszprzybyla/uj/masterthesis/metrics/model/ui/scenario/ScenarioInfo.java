package com.mateuszprzybyla.uj.masterthesis.metrics.model.ui.scenario;

public class ScenarioInfo {
    private int id;
    private String name;

    public ScenarioInfo() {
    }

    public ScenarioInfo(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
