package com.mateuszprzybyla.uj.masterthesis.metrics.model.ui.chart;

import java.util.List;

/*
TODO - add default lists and maps (empty) and remove null-checks on UI side
 */
public class LineChart {
    private String name;
    private String description;
    private String unit;
    private String xAxisType;
    private boolean xAxisVisible;
    private List<ChartSeries> seriesList;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getxAxisType() {
        return xAxisType;
    }

    public void setxAxisType(String xAxisType) {
        this.xAxisType = xAxisType;
    }

    public boolean isxAxisVisible() {
        return xAxisVisible;
    }

    public void setxAxisVisible(boolean xAxisVisible) {
        this.xAxisVisible = xAxisVisible;
    }

    public List<ChartSeries> getSeriesList() {
        return seriesList;
    }

    public void setSeriesList(List<ChartSeries> seriesList) {
        this.seriesList = seriesList;
    }
}
