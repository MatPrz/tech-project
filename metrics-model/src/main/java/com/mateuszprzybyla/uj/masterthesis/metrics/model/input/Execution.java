package com.mateuszprzybyla.uj.masterthesis.metrics.model.input;

import com.mateuszprzybyla.uj.masterthesis.metrics.model.Metric;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Execution {
    private String name;
    private LocalDateTime startTimeUtc;
    private String hostname;
    private Map<Metric, Double> summary = new HashMap<>();
    private List<ExecutionScenario> scenarios = new ArrayList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDateTime getStartTimeUtc() {
        return startTimeUtc;
    }

    public void setStartTimeUtc(LocalDateTime startTimeUtc) {
        this.startTimeUtc = startTimeUtc;
    }

    public Map<Metric, Double> getSummary() {
        return summary;
    }

    public void setSummary(Map<Metric, Double> summary) {
        this.summary = summary;
    }

    public String getHostname() {
        return hostname;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    public List<ExecutionScenario> getScenarios() {
        return scenarios;
    }

    public void setScenarios(List<ExecutionScenario> scenarios) {
        this.scenarios = scenarios;
    }

    @Override
    public String toString() {
        return "Execution{" +
                "name='" + name + '\'' +
                ", startTimeUtc=" + startTimeUtc +
                ", hostname='" + hostname + '\'' +
                ", summary=" + summary +
                ", scenarios=" + scenarios +
                '}';
    }
}
