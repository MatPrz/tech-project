package com.mateuszprzybyla.uj.masterthesis.metrics.model.input;

import com.mateuszprzybyla.uj.masterthesis.metrics.model.Metric;

import java.util.ArrayList;
import java.util.List;

public class MetricData {
    private Metric key;
    private List<MetricSnapshot> data = new ArrayList<>();

    public MetricData() {
    }

    public MetricData(Metric key) {
        this.key = key;
    }

    public Metric getKey() {
        return key;
    }

    public void setKey(Metric key) {
        this.key = key;
    }

    public List<MetricSnapshot> getData() {
        return data;
    }

    public void setData(List<MetricSnapshot> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "MetricData{" +
                "key='" + key + '\'' +
                ", data=" + data +
                '}';
    }
}
