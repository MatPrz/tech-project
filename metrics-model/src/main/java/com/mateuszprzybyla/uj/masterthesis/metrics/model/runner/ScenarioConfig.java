package com.mateuszprzybyla.uj.masterthesis.metrics.model.runner;

import com.mateuszprzybyla.uj.masterthesis.metrics.model.Metric;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotNull;
import java.util.List;

public class ScenarioConfig {
    @NotBlank(message = "Scenario command cannot be blank")
    private String startCommand;
    @NotNull(message = "refreshIntervalMs cannot be empty")
    @Range(min = 100, message = "refreshIntervalMs must be at least 100 ms")
    private Integer refreshIntervalMs;
    @NotEmpty(message = "metric list cannot be empty")
    private List<Metric> metricList;
    private Integer timeout;

    public String getStartCommand() {
        return startCommand;
    }

    public void setStartCommand(String startCommand) {
        this.startCommand = startCommand;
    }

    public Integer getRefreshIntervalMs() {
        return refreshIntervalMs;
    }

    public void setRefreshIntervalMs(Integer refreshIntervalMs) {
        this.refreshIntervalMs = refreshIntervalMs;
    }

    public List<Metric> getMetricList() {
        return metricList;
    }

    public void setMetricList(List<Metric> metricList) {
        this.metricList = metricList;
    }

    public Integer getTimeout() {
        return timeout;
    }

    public void setTimeout(Integer timeout) {
        this.timeout = timeout;
    }

    @Override
    public String toString() {
        return "ScenarioConfig{" +
                "startCommand='" + startCommand + '\'' +
                ", refreshIntervalMs=" + refreshIntervalMs +
                ", metricList=" + metricList +
                ", timeout=" + timeout +
                '}';
    }
}
