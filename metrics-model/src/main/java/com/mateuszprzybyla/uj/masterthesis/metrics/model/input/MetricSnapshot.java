package com.mateuszprzybyla.uj.masterthesis.metrics.model.input;

public class MetricSnapshot {
    private int time;
    private double value;

    public MetricSnapshot() {
    }

    public MetricSnapshot(int time, double value) {
        this.time = time;
        this.value = value;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "MetricSnapshot<" + time + ": " + value + '>';
    }
}
