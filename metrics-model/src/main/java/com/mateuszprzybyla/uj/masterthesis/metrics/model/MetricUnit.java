package com.mateuszprzybyla.uj.masterthesis.metrics.model;

public enum MetricUnit {
    MILLISECONDS("ms"), AMOUNT(""), KILOBYTES("KB"), MEGABYTES("MB"), PERCENT("%");

    private String text;

    MetricUnit(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }
}
