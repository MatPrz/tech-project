package com.mateuszprzybyla.uj.masterthesis.metrics.model.ui.shared;

public class SummaryItem {
    private String key;
    private double value;
    private String unit;

    public SummaryItem() {
    }

    public SummaryItem(String key, double value, String unit) {
        this.key = key;
        this.value = value;
        this.unit = unit;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }
}
