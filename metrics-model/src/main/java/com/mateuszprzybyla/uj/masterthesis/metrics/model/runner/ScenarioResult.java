package com.mateuszprzybyla.uj.masterthesis.metrics.model.runner;

import com.mateuszprzybyla.uj.masterthesis.metrics.model.Metric;
import com.mateuszprzybyla.uj.masterthesis.metrics.model.input.MetricData;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ScenarioResult {
    private String hostname;
    private Map<Metric, Double> summary = new HashMap<>();
    private List<MetricData> metricDatas = new ArrayList<>();

    public ScenarioResult() {
    }

    public ScenarioResult(Map<Metric, Double> summary, List<MetricData> metricDatas) {
        this.summary = summary;
        this.metricDatas = metricDatas;
    }

    public String getHostname() {
        return hostname;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    public Map<Metric, Double> getSummary() {
        return summary;
    }

    public List<MetricData> getMetricDatas() {
        return metricDatas;
    }

    public void setSummary(Map<Metric, Double> summary) {
        this.summary = summary;
    }

    public void setMetricDatas(List<MetricData> metricDatas) {
        this.metricDatas = metricDatas;
    }
}
