package com.mateuszprzybyla.uj.masterthesis.metrics.model.ui.scenario;

import com.mateuszprzybyla.uj.masterthesis.metrics.model.ui.chart.LineChart;
import java.util.List;
import java.util.Map;

public class ScenarioDetails {
    private String name;
    private Map<String, Double> summary;
    private List<LineChart> lineCharts;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Map<String, Double> getSummary() {
        return summary;
    }

    public void setSummary(Map<String, Double> summary) {
        this.summary = summary;
    }

    public List<LineChart> getLineCharts() {
        return lineCharts;
    }

    public void setLineCharts(List<LineChart> lineCharts) {
        this.lineCharts = lineCharts;
    }
}
