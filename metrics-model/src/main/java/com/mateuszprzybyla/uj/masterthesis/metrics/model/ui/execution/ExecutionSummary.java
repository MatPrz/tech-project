package com.mateuszprzybyla.uj.masterthesis.metrics.model.ui.execution;

import com.mateuszprzybyla.uj.masterthesis.metrics.model.ui.shared.ScenarioSummary;
import com.mateuszprzybyla.uj.masterthesis.metrics.model.ui.shared.SummaryItem;
import java.util.List;

public class ExecutionSummary {
    private int id;
    private String name;
    private String hostname;
    private long startTimeUtc;
    private List<SummaryItem> summary;
    private List<ScenarioSummary> scenarios;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHostname() {
        return hostname;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    public long getStartTimeUtc() {
        return startTimeUtc;
    }

    public void setStartTimeUtc(long startTimeUtc) {
        this.startTimeUtc = startTimeUtc;
    }

    public List<SummaryItem> getSummary() {
        return summary;
    }

    public void setSummary(List<SummaryItem> summary) {
        this.summary = summary;
    }

    public List<ScenarioSummary> getScenarios() {
        return scenarios;
    }

    public void setScenarios(List<ScenarioSummary> scenarios) {
        this.scenarios = scenarios;
    }
}
