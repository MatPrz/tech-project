package com.mateuszprzybyla.uj.masterthesis.metrics.model;

public enum Metric {
    //    CACHE_MISSES(MetricUnit.AMOUNT, true, false),
//    CACHE_REFERENCES
    BRANCHES(MetricUnit.AMOUNT, true, false),
    BRANCH_MISSES(MetricUnit.AMOUNT, true, false),
    BUS_CYCLES(MetricUnit.AMOUNT, true, false),
    CPU_CYCLES(MetricUnit.AMOUNT, true, false),
    INSTRUCTIONS(MetricUnit.AMOUNT, true, false),
    REF_CYCLES(MetricUnit.AMOUNT, true, false),
    IDLE_CYCLES_BACKEND(MetricUnit.AMOUNT, true, false),
    IDLE_CYCLES_FRONTEND(MetricUnit.AMOUNT, true, false),

    ALIGNMENT_FAULTS(MetricUnit.AMOUNT, true, false),
    CONTEXT_SWITCHES(MetricUnit.AMOUNT, true, false),
    CPU_CLOCK(MetricUnit.MILLISECONDS, true, false),
    CPU_MIGRATIONS(MetricUnit.AMOUNT, true, false),
    EMULATION_FAULTS(MetricUnit.AMOUNT, true, false),
    MAJOR_FAULTS(MetricUnit.AMOUNT, true, false),
    MINOR_FAULTS(MetricUnit.AMOUNT, true, false),
    PAGE_FAULTS(MetricUnit.AMOUNT, true, false),
    TASK_CLOCK(MetricUnit.MILLISECONDS, true, false),

    L1_DCACHE_LOAD_MISSES(MetricUnit.AMOUNT, true, false),
    L1_DCACHE_LOADS(MetricUnit.AMOUNT, true, false),
    L1_DCACHE_PREFETCH_MISSES(MetricUnit.AMOUNT, true, false),
    L1_DCACHE_PREFETCHES(MetricUnit.AMOUNT, true, false),
    L1_DCACHE_STORE_MISSES(MetricUnit.AMOUNT, true, false),
    L1_DCACHE_STORES(MetricUnit.AMOUNT, true, false),
    L1_ICACHE_LOAD_MISSES(MetricUnit.AMOUNT, true, false),
    L1_ICACHE_LOADS(MetricUnit.AMOUNT, true, false),
    L1_ICACHE_PREFETCH_MISSES(MetricUnit.AMOUNT, true, false),
    L1_ICACHE_PREFETCHES(MetricUnit.AMOUNT, true, false),
    LLC_LOAD_MISSES(MetricUnit.AMOUNT, true, false),
    LLC_LOADS(MetricUnit.AMOUNT, true, false),
    LLC_PREFETCH_MISSES(MetricUnit.AMOUNT, true, false),
    LLC_PREFETCHES(MetricUnit.AMOUNT, true, false),
    LLC_STORE_MISSES(MetricUnit.AMOUNT, true, false),
    LLC_STORES(MetricUnit.AMOUNT, true, false),
    DTLB_LOAD_MISSES(MetricUnit.AMOUNT, true, false),
    DTLB_LOADS(MetricUnit.AMOUNT, true, false),
    DTLB_PREFETCH_MISSES(MetricUnit.AMOUNT, true, false),
    DTLB_PREFETCHES(MetricUnit.AMOUNT, true, false),
    DTLB_STORE_MISSES(MetricUnit.AMOUNT, true, false),
    DTLB_STORES(MetricUnit.AMOUNT, true, false),
    ITLB_LOAD_MISSES(MetricUnit.AMOUNT, true, false),
    ITLB_LOADS(MetricUnit.AMOUNT, true, false),

    CPU_USAGE(MetricUnit.PERCENT, false, false),
    MEMORY_USAGE(MetricUnit.PERCENT, false, false),
    VIRTUAL_MEMORY_SIZE(MetricUnit.KILOBYTES, false, false),
    RESIDENT_SET_SIZE(MetricUnit.KILOBYTES, false, false),
    TEXT_RESIDENT_SET_SIZE(MetricUnit.KILOBYTES, false, false),
    DATA_RESIDENT_SET_SIZE(MetricUnit.KILOBYTES, false, false),
    TOTAL_RUN_TIME(MetricUnit.MILLISECONDS, false, true);

    private MetricUnit unit;
    private boolean aggregable;
    private boolean summaryOnly;

    Metric(MetricUnit unit, boolean aggregable, boolean summaryOnly) {
        this.unit = unit;
        this.aggregable = aggregable;
        this.summaryOnly = summaryOnly;
    }

    public MetricUnit getUnit() {
        return unit;
    }

    public boolean isAggregable() {
        return aggregable;
    }

    public boolean isSummaryOnly() {
        return summaryOnly;
    }
}
