package com.mateuszprzybyla.uj.masterthesis.metrics.model.input;

import com.mateuszprzybyla.uj.masterthesis.metrics.model.Metric;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ExecutionScenario {
    private String name;
    private String hostname;
    private int refreshIntervalMs;
    private List<String> tags = new ArrayList<>();
    private Map<Metric, Double> summary = new HashMap<>();
    private List<MetricData> metricDatas = new ArrayList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHostname() {
        return hostname;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    public int getRefreshIntervalMs() {
        return refreshIntervalMs;
    }

    public void setRefreshIntervalMs(int refreshIntervalMs) {
        this.refreshIntervalMs = refreshIntervalMs;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public Map<Metric, Double> getSummary() {
        return summary;
    }

    public void setSummary(Map<Metric, Double> summary) {
        this.summary = summary;
    }

    public List<MetricData> getMetricDatas() {
        return metricDatas;
    }

    public void setMetricDatas(List<MetricData> metricDatas) {
        this.metricDatas = metricDatas;
    }

    @Override
    public String toString() {
        return "ExecutionScenario{" +
                "name='" + name + '\'' +
                ", refreshIntervalMs=" + refreshIntervalMs +
                ", tags=" + tags +
                ", summary=" + summary +
                ", metricDatas=" + metricDatas +
                '}';
    }
}
