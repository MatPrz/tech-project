package com.mateuszprzybyla.uj.masterthesis.metrics.model.ui.chart;

import java.util.List;

public class ColumnChart {
    private String name;
    private String description;
    private String unit;
    private List<String> categories;
    private List<ChartSeries> seriesList;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public List<String> getCategories() {
        return categories;
    }

    public void setCategories(List<String> categories) {
        this.categories = categories;
    }

    public List<ChartSeries> getSeriesList() {
        return seriesList;
    }

    public void setSeriesList(List<ChartSeries> seriesList) {
        this.seriesList = seriesList;
    }
}
