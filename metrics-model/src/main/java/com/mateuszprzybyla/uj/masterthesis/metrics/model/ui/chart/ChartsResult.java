package com.mateuszprzybyla.uj.masterthesis.metrics.model.ui.chart;

import java.util.LinkedList;
import java.util.List;

public class ChartsResult {
    private List<LineChart> lineCharts = new LinkedList<>();
    private List<ColumnChart> columnCharts = new LinkedList<>();

    public List<LineChart> getLineCharts() {
        return lineCharts;
    }

    public void setLineCharts(List<LineChart> lineCharts) {
        this.lineCharts = lineCharts;
    }

    public List<ColumnChart> getColumnCharts() {
        return columnCharts;
    }

    public void setColumnCharts(List<ColumnChart> columnCharts) {
        this.columnCharts = columnCharts;
    }
}
