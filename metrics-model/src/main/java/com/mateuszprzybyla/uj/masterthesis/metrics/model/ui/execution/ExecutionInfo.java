package com.mateuszprzybyla.uj.masterthesis.metrics.model.ui.execution;

public class ExecutionInfo {
    private int id;
    private String name;
    private long startTimeUtc;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getStartTimeUtc() {
        return startTimeUtc;
    }

    public void setStartTimeUtc(long startTimeUtc) {
        this.startTimeUtc = startTimeUtc;
    }
}
