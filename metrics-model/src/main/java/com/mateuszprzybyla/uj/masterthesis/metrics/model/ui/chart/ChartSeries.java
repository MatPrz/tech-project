package com.mateuszprzybyla.uj.masterthesis.metrics.model.ui.chart;

import java.util.List;

public class ChartSeries {
    private String name;
    private List<SeriesData> data;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<SeriesData> getData() {
        return data;
    }

    public void setData(List<SeriesData> data) {
        this.data = data;
    }
}
