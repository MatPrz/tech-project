package com.mateuszprzybyla.uj.masterthesis.metrics.model.ui.shared;

import java.util.List;

public class ScenarioSummary {
    private int id;
    private String name;
    private String hostname;
    private List<String> tags;
    private List<SummaryItem> summary;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHostname() {
        return hostname;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public List<SummaryItem> getSummary() {
        return summary;
    }

    public void setSummary(List<SummaryItem> summary) {
        this.summary = summary;
    }
}
