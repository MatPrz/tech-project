package com.mateuszprzybyla.uj.masterthesis.metrics.model.ui.chart;

public class SeriesData {
    private Object label;
    private double value;

    public SeriesData() {
    }

    public SeriesData(Object label, double value) {
        this.label = label;
        this.value = value;
    }

    public Object getLabel() {
        return label;
    }

    public void setLabel(Object label) {
        this.label = label;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "MetricSnapshot<" + label + ": " + value + '>';
    }
}
